#!/bin/sh

PUBLISH_REPO=olanilsson.bitbucket.org

usage() {
	cat <<EOF
$0 [OPTIONS] [-e user.email] [-u user.name] [-b FROMBRANCH] [-B TOBRANCH] [-d GITDIR [-n]] PACKAGEFILE [PACKAGEFILES...]
  -v : Be more verbose
  -b : clone FROMBRANCH (before creating any TOBRANCH) instead of master
  -B : Create and push TOBRANCH rather than master
  -c : Continue even if a package update fails
  -e : Set git config option user.email
  -u : Set git config option user.name
  -d : Use GITDIR for the clone of $PUBLISH_REPO instead of a tempdir
  -n : Use an existing clone of $PUBLISH_REPO at GITDIR instead of cloning
  -P : Do not push to $PUBLISH_REPO
  -h : Print this help message
EOF
	[ "$1" ] && exit "$1"
}

while getopts "u:e:b:B:d:nhvcP" opt ; do
	case $opt in
		b) frombranch=$OPTARG ;;
		B) tobranch=$OPTARG   ;;
		c) continue=1         ;;
		e) usermail=$OPTARG   ;;
		d) clonedir=$OPTARG   ;;
		n) noclone=1          ;;
		u) username=$OPTARG   ;;
		v) verbose=1          ;;
		P) nopush=1           ;;
		h) usage 0 ;;
		*) usage 1 ;;
	esac
done
shift $((OPTIND-1))

cleanup() {
	rm -f deployout commitmsg
	[ "$noclone" ] || rm -rf "$clonedir"
}
trap cleanup EXIT

: "${EMACS:=emacs}"

set -e
[ ! "$verbose" ] || set -x

[ ! "$noclone" ] || [ "$clonedir" ] || {
	echo "-n requires -d"
	exit 1
}

# Read the contents before cloning a foreign directory into topdir
contents="$(make contents)"

: "${clonedir:=$(mktemp -d -p .)}"
[ "$noclone" ] ||
	git clone git@bitbucket.org:olanilsson/$PUBLISH_REPO.git ${frombranch:+-b $frombranch} "$clonedir"
dest=${clonedir}/packages
# Make sure the package target ignores PUBLISH_REPO
echo "${clonedir}" >> .elpaignore
echo "*" >> "${clonedir}/.elpaignore"
echo ".elpaignore" >> "${clonedir}/.git/info/exclude"
# Set up some git configs
[ ! "$username" ] || git -C "$dest" config user.name "$username"
[ ! "$usermail" ] || git -C "$dest" config user.email "$usermail"
[ ! "$tobranch" ] || git -C "$dest" checkout -b "$tobranch"
git -C "$dest" config push.default simple

# Remove any existing dist folder
rm -rf dist

# update the package version headers
$EMACS -Q --batch --script "$dest/deploy.el" $contents
# Build the package tar file
make package

for pack in dist/*.tar; do
	case $pack in
		*.el)
			if $EMACS -Q --batch --script "$dest/deploy.el" "$dest" "$pack" 2>deployout; then
				>&2 cat deployout
				# This find line is untested
				find "$dest" -type f -and -name "${pack%.*}-${pack##*.}" |
					sed 's!.*/\(.*\)-\(.*\)\.el!Update \1 to \2!' > commitmsg
				git -C "$dest" add --all
				git -C commit -F - <"$(pwd)/commitmsg"
				#cat "$(pwd)/commitmsg" | (cd "$dest" && git commit -F - )
			else
				errcode=$?
				>&2 cat deployout
				grep -q "New package has smaller version:" deployout && {
					[ "$continue" ] || exit $errcode
				}
			fi
			;;
		*.tar)
			echo "Deploy $pack to $dest"
			if $EMACS -Q --batch --script $dest/deploy.el $dest/ $pack 2>deployout; then
				>&2 cat deployout
				echo "${pack##*/}" |
					sed 's!.*/\(.*\)-\(.*\)\.tar!Update \1 to \2!' |
					tee commitmsg
				git -C $dest add --all
				git -C $dest commit -F $PWD/commitmsg
			else
				errcode=$?
				>&2 cat deployout
				grep -q "New package has smaller version:" deployout && {
					[ "$continue" ] || exit $errcode
				}
			fi
			;;
	esac
done
[ "$nopush" ] || git -C "$dest" push origin HEAD
