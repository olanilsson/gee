;;; gee-reply.el --- Write replies to gerrit changes  -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Ola Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Maintainer: Ola Nilsson <ola.nilsson@gmail.com>
;; URL: https://bitbucket.org/olanilsson/gee
;; Keywords: gerrit

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; The Gerrit reply dialog implemented using widgets.

;;; Code:

(require 'cl-lib)
(require 'wid-edit)
(require 'subr-x)
(require 'gee-gerrit-rest)
(require 'gee-review)

(defun gee-user-vote (user label change-info)
  "Find USERs vote (as a number) for LABEL in CHANGE-INFO.
CHANGE-INFO should have been created with the ALL_LABELS option.
USER can be user id (number), email address, username, or even
full name."
  (let* ((labels (cdr (assoc 'labels change-info)))
         (label-info (cdr (assoc label labels)))
         (all-reviews (cdr (assoc 'all label-info)))
         (approval-info (cl-find user all-reviews
                                 :test (lambda (u ai) (cl-find u ai :key #'cdr)))))
    (cdr (assoc 'value approval-info))))
    
(defun gee-format-label (val)
  "Format number VAL as Gerrit label vote string.
Negative numbers are prefixed with `-', positive with `+', and 0
with nothing."
  (if (zerop val) "0" (format "%+d" val)))

;; radio button vote widget

(define-widget 'gerrit-vote 'radio-button-choice
  "Select vote for a Gerrit label.
One-line variant of the `radio-button-choice' widget that uses a
`:tag' at the beginning of line and a `:label-text' at the end of
line.  The `:label-text' is taken from the currently selected
radio-button `item' and is updated when the selection changes."
  :action #'gerrit-vote-action
  :convert-widget #'gerrit-vote-convert-widget
  :format "%t %l%v%r%m\n"
  :format-handler #'gerrit-vote-format-handler
  :label-text ""
  :label-text-start nil
  :label-text-end nil
  :value-changed #'gerrit-vote-value-changed)

(defun gerrit-vote-value-changed (widget)
  "Return WIDGET value, if different from widget creation.
Otherwise return nil.  The value is compared to the
`:start-value' property."
  (let ((value (widget-value widget)))
    (and (not (equal value (widget-get widget :start-value)))
         value)))

(defun gerrit-vote-action (widget child event)
  "Update the descriptive text for WIDGET when CHILD is selected.
Calls `widget-radio-action' with WIDGET, CHILD, and EVENT as
arguments at the end."
  (let ((item-child (cl-find child (widget-get widget :children)
                             :key (lambda (w) (widget-get w :button)))))
    (when item-child
      (let ((description (widget-get item-child :label-text))
            (begin (widget-get widget :label-text-start))
            (end (widget-get widget :label-text-end)))
        (when (and description begin end)
          (save-excursion
            (goto-char begin)
            (set-marker-insertion-type begin nil)
            (widget-insert description)
            (set-marker-insertion-type begin t)
            (let ((inhibit-read-only t))
              (delete-region (point) end)))))))
  (widget-radio-action widget child event))

(defun gerrit-vote-convert-widget (widget)
  "Update the `:format' property of any argument `item' widgets.
The gerrit-vote WIDGET expects a list of `item' widgets as
arguments.  Each of them have to have their `:format' property
changed so the radio-buttons can be on the same line."
  (let ((args (widget-get widget :args)))
    (when args
      ;; set the :format tag of item widgets to "%t "
      (setq args (mapcar (lambda (e)
                           (when (and (widgetp e) (eq (widget-type e) 'item)
                                      (string= (widget-get e :format) "%t\n"))
                             (widget-put e :format "%t "))
                           e)
                         args))
      (widget-put widget :args args)))
  widget)

(defun gerrit-vote-format-handler (widget escape)
  "Handle extra WIDGET format ESCAPE codes for `gerrit-vote' widgets.
%l (left padding), %r (right padding, and %m for value associated string."
  (cond ((eq escape ?l)
         (insert-char ?\s (or (widget-get widget :value-padding-left) 0)))
        ((eq escape ?r)
         (insert-char ?\s (or (widget-get widget :value-padding-right) 0)))
        ((eq escape ?m)
         (let ((start-mark (point-marker))
               (value-item (cl-find (widget-get widget :value)
                                    (widget-get widget :args)
                                    :key (lambda (w) (car (last w)))
                                    :test #'string=)))
           (widget-insert (widget-get value-item :label-text))
           (set-marker-insertion-type start-mark t)
           (widget-put widget :label-text-start start-mark)
           (widget-put widget :label-text-end (point-marker))))
        (t (widget-default-format-handler widget escape))))

;; gee link widget

(define-widget 'gerrit-file-link 'link
  "Link to file and possibly line."
  :button-face 'gee-review-button-face
  :button-prefix ""
  :button-suffix ""
  :convert-widget #'gerrit-file-link-convert-widget)

(defun gerrit-file-link-convert-widget (widget)
  "Convert ansy WIDGET symbol arguments to strings."
  (let ((args (widget-get widget :args)))
    (setq args (mapcar (lambda (arg)
                         (if (symbolp arg)
                             (symbol-name arg) arg)) args))
    (widget-put widget :args args))
  widget)

(defsubst afirst (array)
  "Return the first element of ARRAY."
  (aref array 0))

(defsubst alast (array &optional n)
  "Return the last element of ARRAY.
If N is non-nil, return the Nth-to-last element of ARRAY."
  (aref array (- (length array) (or n 1))))

(defun gee-labels-start-values (change-info &optional account-id)
  "Return current votes on CHANGE-INFO REVISION-ID for ACCOUNT-ID."
  (let ((permitted-labels (cdr (assoc 'permitted_labels change-info))))
    (cl-loop for (label . label-info) in permitted-labels
             for value = (or (gee-user-vote account-id label change-info)
                             (cdr (assoc 'default_value label-info))
                             )
             collect (cons label value))))

(defun gee-reply-form-insert-vote-widgets (change-info revision-id)
  "Insert the proper label voting widgets for CHANGE-INFO.
Do nothing if REVISION-ID is not the current revision, as votings
is onlyallowed for the current (last) revision."
  (when (string= (cdr (assoc 'current_revision change-info)) revision-id)
    (let* ((permitted-labels (cdr (assoc 'permitted_labels change-info)))
           (labels (cdr (assoc 'labels change-info)))
           (self-account-info (gee-gerrit-rest-account "self")))
      (cl-loop
       for (label . values) in permitted-labels
       maximize (length (symbol-name label)) into max-label-length
       maximize (string-to-number (alast values)) into max-value
       minimize (string-to-number (afirst values)) into min-value
       finally return
       (cl-loop
        for (label . values) in permitted-labels
        for label-info = (cdr (assoc label labels))
        for value = (or (gee-user-vote (cdr (assoc '_account_id self-account-info)) label change-info)
                        (cdr (assoc 'default_value label-info)))
        for skip-value-padding-left = (* 7 (abs (- min-value (string-to-number (afirst values)))))
        for skip-value-padding-right = (* 7 (abs (- max-value (string-to-number (alast values)))))
        collect (apply #'widget-create 'gerrit-vote
                       :value (gee-format-label value)
                       :start-value (gee-format-label value)
                       :tag (symbol-name label)
                       :value-padding-left (+ (- max-label-length (length (symbol-name label)))
                                              skip-value-padding-left)
                       :value-padding-right skip-value-padding-right
                       (mapcar (lambda (val)
                                 (list 'item
                                       :label-text (or (cdr (assoc (intern val) (cdr (assoc 'values label-info))))
                                                       "NOT FOUND")
                                       (string-trim val))) values))
                                       )))))

(defun gee-reply-form-insert-draft-comments (change-info revision-id)
  "Insert draft comments from CHANGE-INFO with associated file and line links.
Only include comments for REVISION-ID.  Return the map of file
path to lists of CommentInfo entries returned by
`gee-gerrit-rest-list-drafts'."
  (let* ((revisions (cdr (assoc 'revisions change-info)))
         (revision-info (cdr (assoc (intern revision-id) revisions)))
         (drafts (and (cdr (assoc 'has_draft_comments revision-info))
                      (gee-gerrit-rest-list-drafts (cdr (assoc 'id change-info))
                                                   revision-id))))
    (cl-loop
     for (file . comments) in drafts
     do (progn
          (widget-create 'gerrit-file-link file)
          (widget-insert "\n")
          (cl-loop
           for comment across (cl-stable-sort (copy-sequence comments) #'<
                                              :key (lambda (e) (or (cdr (assoc 'line e)) -1)))
           do (progn
                (let ((comment-line (cdr (assoc 'line comment))))
                  (if (not comment-line)
                      (widget-insert "  File Comment ")
                    (widget-insert "  Line ")
                    (widget-create 'gerrit-file-link comment-line)
                    (widget-insert ":")
                    (widget-insert (make-string (- (length "  File Comment ")
                                                   (current-column)) ?\s))))
                (let ((padding (concat "\n" (make-string (current-column) ?\s)))
                      (message (cdr (assoc 'message comment))))
                  (widget-insert (replace-regexp-in-string "\n" padding message)
                                 "\n"))))))
    drafts))

(defun gee-reply-form (change-info &optional revision-id)
  "Create a change review form for CHANGE-INFO revision REVISION-ID.
Always resolves REVISION-ID using `gee-canonical-revision-id',
which means nil and \"current\" are valid values."
  (setq revision-id (gee-canonical-revision-id change-info revision-id))
  (switch-to-buffer "*gee reply form*")
  (kill-all-local-variables)
  (let ((inhibit-read-only t))
    (erase-buffer))
  (remove-overlays)
  (let ((message (widget-create 'text :format "%v\n" (make-string 6 ?\n)))
        (vote-widgets (gee-reply-form-insert-vote-widgets change-info revision-id)))
    (widget-insert "\n")
    (gee-reply-form-insert-draft-comments change-info revision-id)
    (widget-insert "\n")
    (widget-create 'link
                   :button-face 'gee-review-bordered-button-face
                   :button-prefix "" :button-suffix ""
                   :action #'gee-reply-set-review
                   :change-id (cdr (assoc 'id change-info))
                   :change-info change-info
                   :revision-id revision-id
                   :message-widget message
                   :vote-widgets vote-widgets
                   "Post")
    (widget-insert "\t")
    (widget-create 'link :button-face 'gee-review-bordered-button-face
                   :button-prefix "" :button-suffix ""
                   :action (lambda (&rest _ignore) (quit-window))
                   "Cancel")
    (use-local-map widget-keymap)
    (widget-setup)))

(defun gee-reply-set-review (widget &optional event)
  "WIDGET button EVENT action to set the review."
  ;; Will set labels to nil if this is not the current revision as :vote-widgets is nil
  (let ((labels (cl-loop for widget in (widget-get widget :vote-widgets)
                         for vote = (widget-apply widget :value-changed)
                         if vote collect (cons (widget-get widget :tag) vote))))
    (gee-gerrit-rest-change-revision-set-review
     (widget-get widget :change-id) (widget-get widget :revision-id)
     (string-trim (widget-value (widget-get widget :message-widget)))
     labels nil "PUBLISH")
    event) ;mention event to quiet warnings, checkdoc complains about _event
  (quit-window))

(provide 'gee-reply)
;;; gee-reply.el ends here
