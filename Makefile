# Copyright (C) 2016, 2017, 2019 by Ola Nilsson <ola.nilsson@gmail.com>

EMACS ?= emacs
ifeq ($(EMACS),t)
EMACS = emacs
endif

BATCHOPTS = --batch -q -l .emacs/lisp/init.el
EMACS_BATCH = $(EMACS) $(BATCHOPTS)
EMACS_PACKAGING = $(EMACS) --batch -q -l .emacs/lisp/packaging.el
TESTFILEFLAGS = -L . -L tests -l buttercup-junit
EMACS_TEST = $(EMACS) $(BATCHOPTS) $(TESTFILEFLAGS)

TESTOPTS = -L . -L tests -batch -l buttercup-junit -f package-initialize

JUNIT_circleci = $(if $(CIRCLECI),$(CIRCLE_WORKING_DIRECTORY)/test_results/ert/test.xml)
JUNIT ?= $(or $(JUNIT_circleci),gee.xml)

all: build

setup:
	$(EMACS_BATCH)

build:
	$(EMACS_BATCH) --eval '(byte-recompile-directory "'$(PWD)'" 0)'

report: $(JUNIT)

.PHONY: $(JUNIT)
$(JUNIT):
	mkdir -p $(@D)
	$(EMACS_TEST) -f buttercup-junit-run-discover --xmlfile $@ $(if $(OUTER),--outer-suite "$(OUTER)") tests

stdout:
	mkdir -p $(@D)
	$(EMACS_TEST) -f buttercup-junit-run-discover --xmlfile $(JUNIT) --junit-stdout --outer-suite tests

check tests:
	$(EMACS_TEST) -f buttercup-run-discover $(BUTTERCUP_FLAGS) tests

## Packaging

gee-pkg.el: gee.el
	$(EMACS_PACKAGING) gee.el -f generate-description-file

dist/gee-readme.txt: gee.el
	mkdir -p $(@D)
	$(EMACS_PACKAGING) gee.el -f generate-readme > $@

package_name := $(shell $(EMACS_PACKAGING) gee.el -f package-archive-name 2>/dev/null)
package_dir = $(basename $(package_name))

tar_has_exclude_ignore := $(shell tar --help 2>&1| sed -n '/exclude-ignore=/s/.*/yes/p')
ifeq ($(tar_has_exclude_ignore),yes)

package_contents = $(filter-out %/,$(shell tar -cvf /dev/null --exclude dist --exclude-ignore=.elpaignore --exclude-vcs --exclude-backup . 2>&1))

contents:
	@echo $(package_contents) | tr ' ' '\n'

package: dist/$(package_name) dist/gee-readme.txt

dist/$(package_name): gee-pkg.el $(package_contents)
	mkdir -p $(@D)
	tar -cvf $@ --transform='s/\./'$(package_dir)'/' --exclude dist --exclude-ignore=.elpaignore --exclude-vcs --exclude-backup .
else
package contents dist/$(package_name):
	@echo "The $@ target requires tar to support the --exclude-ignore option."
	@echo "This version of tar does not"
	@tar --version
	@false
endif

lisp-clean:
	-rm -rf *.elc test/*.elc

clean: lisp-clean
	rm -rf dist

reallyclean: clean
	rm -rf .emacs/elpa .emacs/.emacs-custom.el

.PHONY: all test clean lisp-clean reallyclean
