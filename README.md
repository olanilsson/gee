[![CircleCI](https://circleci.com/bb/olanilsson/gee/tree/master.svg?style=shield&circle-token=36b86f95b46bb166abbd4ad37a7aecc5cd03314e)](https://circleci.com/bb/olanilsson/gee/tree/master) [![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

# Gee - The Gerrit Emacs Experience

> **Work in progress!** Gee is very much a work in progress and any part
> may change without warning.
>
> * So far the change table, change view, and reply dialog are mostly
>   done but still require a lot of polish.
>
> * Parts (most) of the code is atrocius, brittle, inefficient and needs
>   at least one overhaul.
>
> * There is practically no error handling.
>
> * All REST API calls use the authenticated version, but authentication
>   is handled completely by the Emacs url framework and is only tested
>   on a Gerrit server using Basic Auth.
>
> * The file and function names have to be updated as names are
>   inconsistent and really should map better to the Gerrit UI.
>
> * The README.md file should be improved with screenshots and basic
>   documentation :)

### Gee aims to be a complete Gerrit UI in Emacs.

Each Gerrit view should be provided, but replace some parts with more
appropriate Emacs constructs.

### Gee lets you customize the UI.

The Gerrit Web UI provides very few customization options.  Gee wants
you to add and remove parts of the UI, add new actions, replace entire
views and so on.

### Gee lets you create your own workflows.

By providing customization points in as many places as possible, gee
lets you hook actions together to create new, more efficient ways of
working for.

## Installation

Gee is meant to be installed as an Emacs ELPA package.

While gee is not yet distributed through any ELPA archive, it should
be possible to install it by

* downloading the source
* adding it to the emacs load path
* `(require gee)`
* Set `gee-gerrit-rest-base` either through customize or using `setq`.
* `M-x gee-gerrit-dashboard-self RET`
