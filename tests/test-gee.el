;;; test-gee.el --- tests for gee-gee.el  -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Ola Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
(when load-file-name
  (add-to-list 'load-path (file-name-directory load-file-name))
  (add-to-list 'load-path (file-name-directory (file-name-directory load-file-name))))

(require 'gee)
(require 'buttercup)

(describe "Verify that gee-tablist-add-star-printer"
  (it "calls inner-printer with unmodified args"
	(spy-on 'gee-change-info-from-col-entry :and-return-value nil)
	(gee-tablist-add-star-printer (lambda (i c)
									(expect i :to-be 12)
									(expect c :to-equal '[1 2 3]))
								  12 '[1 2 3])
	(expect 'gee-change-info-from-col-entry :to-have-been-called)
	(spy-calls-reset 'gee-change-info-from-col-entry))
  (it "does nothing without a `change-info' property"
	(spy-on 'gee-change-info-from-col-entry :and-return-value nil)
	(spy-on 'add-text-properties)
	(gee-tablist-add-star-printer (lambda (i c) (ignore i c)) nil '[1])
	(expect 'add-text-properties :not :to-have-been-called)
	(spy-calls-reset 'gee-change-info-from-col-entry)
	(spy-calls-reset 'add-text-properties))
  (it "does nothing without a `change-info'->`starred' property"
	(spy-on 'gee-change-info-from-col-entry :and-return-value '((not-starred . t)))
	(spy-on 'add-text-properties)
	(gee-tablist-add-star-printer (lambda (i c) (ignore i c)) nil '[1])
	(expect 'add-text-properties :not :to-have-been-called)
	(spy-calls-reset 'gee-change-info-from-col-entry)
	(spy-calls-reset 'add-text-properties))
  (it "applies text properties to the region printed by inner-printer"
	(spy-on 'gee-change-info-from-col-entry :and-return-value '((starred . t)))
	(spy-on 'add-text-properties)
	(with-temp-buffer
	  (let ((gee-changelist-starred-properties 'geeprops))
		(gee-tablist-add-star-printer (lambda (i c) (insert "text")) "" '[""])))
	(expect 'add-text-properties :to-have-been-called-with 1 5 'geeprops))
  )

(describe "Verify that gee-tablist-add-changeinfo-printer"
  (it "calls inner-printer with unmodified args"
	(spy-on 'gee-change-info-from-col-entry :and-return-value nil)
	(gee-tablist-add-changeinfo-printer (lambda (i c)
									(expect i :to-be 12)
									(expect c :to-equal '[1 2 3]))
								  12 '[1 2 3])
	(expect 'gee-change-info-from-col-entry :to-have-been-called)
	(spy-calls-reset 'gee-change-info-from-col-entry))
  (it "does nothing without a `change-info' property"
	(spy-on 'gee-change-info-from-col-entry :and-return-value nil)
	(spy-on 'add-text-properties)
	(gee-tablist-add-changeinfo-printer (lambda (i c) (ignore i c)) nil '[1])
	(expect 'add-text-properties :not :to-have-been-called)
	(spy-calls-reset 'gee-change-info-from-col-entry)
	(spy-calls-reset 'add-text-properties))
  (it "applies text properties to the region printed by inner-printer"
	(spy-on 'gee-change-info-from-col-entry :and-return-value '((starred . t)))
	(spy-on 'add-text-properties)
	(with-temp-buffer
	  (let ((gee-changelist-starred-properties 'geeprops))
		(gee-tablist-add-changeinfo-printer (lambda (i c) (insert "text")) "" '[""])))
	(expect 'add-text-properties :to-have-been-called-with 1 5 '(change-info ((starred . t))))))

(describe "Verify that gee-tablist-section-header-printer"
  (it "calls inner-printer with unmodified args if id is not a section-string"
	(let* ((expected-tabulated-list-format '[("1" "2" nil) ("2" "2" nil)])
		   (tabulated-list-format '[("1" "2" nil) ("2" "2" nil)]))
	  (gee-tablist-section-header-printer (lambda (i c)
											(expect i :to-equal "nonsection:foo")
											(expect c :to-equal '["" "" ""])
											(expect tabulated-list-format
													:to-equal expected-tabulated-list-format))
										  "nonsection:foo" '["" "" ""])))
  (it "calls inner-printer with unmodified args if id is not a string"
	(let* ((expected-tabulated-list-format '[("1" "2" nil) ("2" "2" nil)])
		   (tabulated-list-format '[("1" "2" nil) ("2" "2" nil)]))
	  (gee-tablist-section-header-printer (lambda (i c)
											(expect i :to-equal 'section:foo)
											(expect c :to-equal '["" "" ""])
											(expect tabulated-list-format
													:to-equal expected-tabulated-list-format))
										  'section:foo '["" "" ""])))
	(describe "replaces the `tabulated-list-format' with"
	  (it "the same number of colums"
		(let* ((tabulated-list-format '[("One" 3 nil) ("Two" 4 nil)
										("Three" 5 nil) ("Four" 6 nil)]))
		  (with-temp-buffer
			(gee-tablist-section-header-printer
			 (lambda (id cols)
			   (expect (length tabulated-list-format) :to-equal 4))
			 "section:Name" ["Abel" "Baker" "Charlie" "Delta"]))))
	  (it "the sum of the column widths equal to original"
		(let* ((tabulated-list-format '[("One" 3 nil) ("Two" 4 nil)
										("Three" 5 nil) ("Four" 6 nil)])
			   (width-sum (cl-loop for c across tabulated-list-format
								   sum (nth 1 c))))
		  (with-temp-buffer
			(gee-tablist-section-header-printer
			 (lambda (id cols)
			   (expect (cl-loop for c across tabulated-list-format sum (nth 1 c))
					   :to-equal width-sum))
			 "section:Name" ["Abel" "Baker" "Charlie" "Delta"])))))
	(it "sets the face property of the printed line"
	  (let* ((tabulated-list-format '[("One" 3 nil) ("Two" 4 nil)
									  ("Three" 5 nil) ("Four" 6 nil)]))
		(spy-on 'put-text-property)
		(with-temp-buffer
		  (gee-tablist-section-header-printer
		   (lambda (i c) (insert "text")) "section:name" ["" "" "" ""])
		  (expect 'put-text-property :to-have-been-called-with 1 5 'face 'gee-dashboard-section)
		(spy-calls-reset 'put-text-property)
		))))

(describe "Verify that gee-account-property"
  :var ((test-accounts '((1 . ((_account_id . 1)
							   (name . "Daffy Duck")
							   (email . "daffy@looney.tunes")))))
		(input-account '((_account_id . 2)
						 (name . "Elmer Fudd")))
		(returned-account '((_account_id . 0)
							(name . "Diffy Cuckoo")
							(email . "diffy@gerrit")
							(username . "diffy")
							(avatars . [(url . "/gerrit/static/avatars/avatar_diffy.jpg")
										(height . 26)]))))
  (before-each
	(spy-on 'gee-gerrit-rest-account :and-return-value returned-account))
  (after-each (spy-calls-reset 'gee-gerrit-rest-account))
  (it "returns properties from the passed account-info"
	(let ((gee-gerrit-accounts test-accounts))
	  (expect (gee-account-property 'name input-account) :to-equal "Elmer Fudd")
	  (expect 'gee-gerrit-rest-account :not :to-have-been-called)))
  (it "returns the cached value if available"
	(let ((gee-gerrit-accounts test-accounts))
	  (expect (gee-account-property 'name '((_account_id . 1))) :to-equal "Daffy Duck")
	  (expect 'gee-gerrit-rest-account :not :to-have-been-called)))
  (it "fetches and caches data from server if account info is not cached"
	(let ((gee-gerrit-accounts test-accounts))
	  (expect (gee-account-property 'name '((_account_id . 0))) :to-equal "Diffy Cuckoo")
	  (expect 'gee-gerrit-rest-account :to-have-been-called)
	  (expect (cons 0 returned-account) :to-be-in gee-gerrit-accounts)))
  (it "signals an error if there is no account id in account-info"
	(expect (gee-account-property 'name '()) :to-throw))
  (it "does not store empty account-info in gee-gerrit-accounts"
	(spy-on 'gee-gerrit-rest-account :and-return-value nil)
	(let ((gee-gerrit-accounts test-accounts))
	  (expect (gee-account-property 'name '((_account_id . 100))) :to-equal nil)
	  (expect 'gee-gerrit-rest-account :to-have-been-called-with 100)
	  (expect 100 :not :to-be-a-key-in gee-gerrit-accounts))))

(describe "gee-changeinfo-entries should convert a vector of"
  :var ((simple '[((_number . 1)) ((_number . 2)) ((_number . 3))])
		(nested '[[((_number . 1)) ((_number . 2)) ((_number . 3))]
				  [((_number . 10)) ((_number . 23))]
				  [((_number . 10)) ((_number . 23))
				   ((_number . 12)) ((_number . 27))]])
		(sectioned '[("single digit" . [((_number . 1))
										((_number . 2)) ((_number . 3))])
					 ("two hits" . [((_number . 10)) ((_number . 23))])
					 ("more hits" . [((_number . 10)) ((_number . 23))
									 ((_number . 12)) ((_number . 27))])]))
  (before-each
	(spy-on 'gee--changeinfo-to-tablistentry
			:and-call-fake (lambda (ci-vector &optional wvector)
							 (cl-loop for ci across ci-vector
									  collect (list (cdr (assoc '_number ci))
													(make-vector 5 ".")))))
	(spy-on 'gee--tablist-header-entry
			:and-call-fake (lambda (title)
							 (list (format "section:%s" title)
								   `[,title "" "" "" ""])
							 )))
  (after-each (spy-calls-reset 'gee--changeinfo-to-tablistentry)
			  (spy-calls-reset 'gee-tablist-header-entry))
  (it "ChangeInfo to a list of tablist-entries"
	(let ((res (gee-changeinfo-entries simple)))
	  (expect (length res) :to-equal (length simple))
	  (dolist (entry res)
		(expect (type-of entry) :to-be 'cons)
		(expect (type-of (cadr entry)) :to-be 'vector))))
  (it "vectors of ChangeInfo to a list of tablist-entries"
	(let ((res (gee-changeinfo-entries nested)))
	  (expect (length res) :to-equal (cl-loop for v across nested
											  sum (length v)))
	  (dolist (entry res)
		(expect (type-of entry) :to-be 'cons)
		(expect (type-of (cadr entry)) :to-be 'vector))))
  (it "section-changeinfovector pairs to a list of tablist-entries"
	(let ((res (gee-changeinfo-entries sectioned)))
	  (expect (length res) :to-equal (cl-loop for (h . v) across sectioned
											  sum (1+ (length v))))
	  (dolist (entry res)
	  	(expect (type-of entry) :to-be 'cons)
	  	(expect (type-of (cadr entry)) :to-be 'vector))
	  (cl-loop for (h . v) across sectioned
	    	   do (let ((r (nth resindex res)))
	    			(expect (car r) :to-equal (format "section:%s" h))
	    			(expect (aref (cadr r) 0) :to-equal h))
	    	   sum (1+ (length v)) into resindex))))

(describe "gee-vector-collect-max"
  (describe "should"
    :var ((acc10 (make-vector 10 0))
          (plain10 (make-vector 10 "test")))
    (it "return COLUMNS onmodified"
      (expect (gee--vector-collect-max acc10 plain10) :to-be plain10)
      (expect plain10 :to-equal (make-vector 10 "test")))
    (it "correctly find the length of the strings"
      (expect acc10 :to-equal (make-vector 10 4))))
  (it "handles column descriptors"
    (let ((acc10 (make-vector 10 0))
          (col10 '["1" ("22" 1 2 3) "333" ("4444" . "dummy") "55555" "666666"
                   ("7777777" [1 2 3 4]) ("88888888") "999999999"
                   "AAAAAAAAAA"]))
      (dotimes (i 10)
        (gee--vector-collect-max acc10 col10))
      (expect acc10 :to-equal '[1 2 3 4 5 6 7 8 9 10])
      (gee--vector-collect-max acc10
                               ;; reverse does not work on vectors in emacs 24
                               (cl-loop for i from (1- (length col10)) downto 0
                                        vconcat `[,(aref col10 i)]))
      (expect acc10 :to-equal '[10 9 8 7 6 6 7 8 9 10])))
  (it "works when the accumulating vector is shorter"
    (let ((acc5 (make-vector 5 0))
          (plain10 '["1" "22" "333" "4444" "55555" "666666" "7777777"
                     "88888888" "999999999" "aaaaaaaaaa"]))
      (expect (gee--vector-collect-max acc5 plain10) :not :to-throw)
      (expect acc5 :to-equal '[1 2 3 4 5])))
  (it "fails when the accumulating vector is longer"
    (let ((acc12 (make-vector 12 0))
          (plain5 (make-vector 5 "test")))
      (expect (gee--vector acc12 plain5) :to-throw))))

(describe "gee--changelist-format"
  (it "should return an empty vector for empty input"
    (expect [] :to-equal (gee--changelist-format '())))
  (it "should handle pure-string specifications"
    (let ((gee-changelist-column-definitions '("String")))
      (expect [("String" 6 nil :right-align nil)] :to-equal (gee--changelist-format '("String")))))
  (it "should handle string-in-list specifications"
    (let ((gee-changelist-column-definitions '(("String"))))
      (expect [("String" 6 nil :right-align nil)] :to-equal (gee--changelist-format '("String")))))
  (it "should handle min width"
    (let ((gee-changelist-column-definitions '(("String" :width 12))))
      (expect [("String" 12 nil :right-align nil)] :to-equal (gee--changelist-format '("String")))))
  (it "should handle colwidth"
    (let ((gee-changelist-column-definitions '(("String" :width 12) ("String2" :width 56))))
      (expect [("String" 20 nil :right-align nil)] :to-equal (gee--changelist-format '("String") [20]))))
  (it "handle :label"
    (let ((gee-changelist-column-definitions '(("String" :label "Another")
                                               ("String2" :width 56))))
      (expect [("Another" 7 nil :right-align nil)] :to-equal (gee--changelist-format '("String")))))
  (it "handle :right-align"
    (let ((gee-changelist-column-definitions '(("String" :right-align t)
                                               ("String2" :width 56))))
      (expect [("String" 6 nil :right-align t)] :to-equal (gee--changelist-format '("String"))))))

(describe "gee--changeinfo-to-tablistentry"
  (it "handles :getter and :formatter"
    (let ((gee-changelist-columns '("ID"))
          (gee-changelist-column-definitions '(("ID" :getter "_number" :formatter number-to-string)))
          (changeinfo '[((id . "project~changeid")
                         (_number . 22))]))
      (expect (gee--changeinfo-to-tablistentry changeinfo)
              :to-equal `(("project~changeid" ["22"])))))
  (describe "looks up the lowercase name if no :getter is set"
    (it "when spec is a string"
      (let ((gee-changelist-columns '("Subject"))
            (gee-changelist-column-definitions '("Subject"))
            (changeinfo '[((id . "projext~changeid")
                           (subject . "subject string"))]))
        (expect (gee--changeinfo-to-tablistentry changeinfo)
                :to-equal `(("projext~changeid" ["subject string"])))))
    (it "when spec is a list holding a string"
      (let ((gee-changelist-columns '("Subject"))
            (gee-changelist-column-definitions '(("Subject")))
            (changeinfo '[((id . "projext~changeid")
                           (subject . "subject string"))]))
        (expect (gee--changeinfo-to-tablistentry changeinfo)
                :to-equal `(("projext~changeid" ["subject string"])))))
    (it "or when there is no matching spec"
      (let ((gee-changelist-columns '("Subject"))
            (gee-changelist-column-definitions '(("Status")))
            (changeinfo '[((id . "projext~changeid")
                           (subject . "subject string"))]))
        (expect (gee--changeinfo-to-tablistentry changeinfo)
                :to-equal `(("projext~changeid" ["subject string"]))))))
  (it "adds change-info property"
      (let ((gee-changelist-columns '("Subject"))
            (gee-changelist-column-definitions '(("Status")))
            (changeinfo '[((id . "projext~changeid")
                           (subject . "subject string"))]))
        (let ((entry (gee--changeinfo-to-tablistentry changeinfo) ))
          (expect entry :to-equal `(("projext~changeid" ["subject string"])))
          (expect (text-properties-at 1 (aref (cadr (car entry)) 0)) :to-equal
                  `(change-info ,(aref changeinfo 0))))))
  (it "adds button properties"
    (let ((gee-changelist-columns '("ID"))
          (gee-changelist-column-definitions '(("ID" :getter "_number"
                                                :button (action ignore))))
          (changeinfo '[((id . "project~changeid")
                         (_number . "22"))]))
      (expect (gee--changeinfo-to-tablistentry changeinfo)
              :to-equal `(("project~changeid" [("22" action ignore)])))))
  (it "collects max widths for each column"
    (let ((gee-changelist-columns '("a" "b" "c" "d" "e"))
          (gee-changelist-column-definitions
           '(("a" :width 2)
             ("b" :width 3)
             ("c" :label "123")
             ("d")
             ("e")))
          (changeinfo '[((id . "1") (a . "123") (b . "12") (c . "12") (d . "1"  ) (e . ""))
                        ((id . "2") (a . "123") (b . "12") (c . "12") (d . "12" ) (e . ""))
                        ((id . "3") (a . "123") (b . "12") (c . "12") (d . "123") (e . ""))])
          (maxv (make-vector 5 0))
          entries)
      (aset maxv 4 10)
      (setq entries (gee--changeinfo-to-tablistentry changeinfo maxv))
      (expect (length entries) :to-equal 3)
      (expect maxv :to-equal [3 2 2 3 10]))))

(describe "gee--make-colvalue-function"
  (it "handles string specs"
    (let ((defs '("Status"))
          (ci '((id . "idstring") (status . "OK"))))
      (expect (funcall (gee--make-colvalue-function "Status" defs) ci)
              :to-equal "OK")))
  (describe "getters"
    (it "can be plain strings"
      (let ((defs '(("ID" :getter "_number")))
            (ci '((id . "idstring") (_number . "72"))))
        (expect (funcall (gee--make-colvalue-function "ID" defs) ci)
                :to-equal "72")))
    (it "can be function symbols"
      (let ((defs '(("Status" :getter gee-changelist-status)))
            (ci '((id . "idstring") (status . "DRAFT"))))
        (expect (funcall (gee--make-colvalue-function "Status" defs) ci)
                :to-equal "Draft")))
    (it "can be lambda functions"
      (let ((defs '(("Status" :getter (lambda (ci) (cdr (assoc 'status ci))))))
            (ci '((id . "idstring") (status . "DRAFT"))))
        (expect (funcall (gee--make-colvalue-function "Status" defs) ci)
                :to-equal "DRAFT"))))
  (describe "formatters"
    (it "can be function symbols"
      (let ((defs '(("ID" :getter "_number" :formatter number-to-string)))
            (ci '((id . "idstring") (_number . 72))))
        (expect (funcall (gee--make-colvalue-function "ID" defs) ci)
                :to-equal "72")))
    (it "can be lambda functions"
      (let ((defs '(("Status" :getter (lambda (ci) (cdr (assoc 'status ci)))
                     :formatter (lambda (str) (capitalize str)))))
            (ci '((id . "idstring") (status . "DRAFT"))))
        (expect (funcall (gee--make-colvalue-function "Status" defs) ci)
                :to-equal "Draft")))
    )
  (describe "buttons"
    (it "should be added"
      (let ((defs '(("ID" :getter "_number" :formatter number-to-string
                     :button (action gee-visit-change-at-pos))))
            (ci '((id . "idstring") (_number . 72))))
        (expect (funcall (gee--make-colvalue-function "ID" defs) ci)
                :to-equal '("72" action gee-visit-change-at-pos)))
    )
  ))


;; Local Variables:
;; flycheck-emacs-lisp-load-path: ("." "..")
;; tab-width: 4
;; indent-tabs-mode: nil
;; End:
(provide 'test-gee)
;;; test-gee.el ends here
