;;; gee-gerrit-rest.el --- Gerrit REST API access -*- lexical-binding: t; -*-

;; Copyright (C) 2015, 2016  Ola Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Maintainer: Ola Nilsson <ola.nilsson@gmail.com>
;; URL: https://bitbucket.org/olanilsson/gee
;; Keywords: gerrit

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(eval-and-compile (require 'cl-lib))
(require 'url)
(defvar url-http-response-status)
(require 'json)

(defvar url-http-end-of-headers) ;; defined in url-http

(defcustom gee-gerrit-rest-base nil
  "The base URL of the Gerrit server REST API.
Adding a REST API endpoint as specified by
https://gerrit-review.googlesource.com/Documentation/rest-api.html
should form a warking URL to access the API endpoint."
  :group 'gee
  :type '(string))

(defun gee--gerrit-rest-build-url (endpoint &optional query)
  "Build a Gerrit REST API URL based on `gee-gerrit-rest-base'.
ENDPOINT is the API endpoint, QUERY is a key value list as used
by `url-build-query-string'."
  (cl-letf (((symbol-function 'url-hexify-string) #'identity))
	(let ((query-string (url-build-query-string query)))
	  (concat gee-gerrit-rest-base endpoint
			  (if (string= "" query-string) "" "?")  query-string))))

(defmacro with-gee-json-options (&rest body)
 "Execute BODY with let-bound json-read variables.
Uses `read-json' with `json-object-type', `json-key-type',
`json-array-type', `json-false', and `json-null' set to default
values."
 (declare (debug body) (indent defun))
 `(let ((json-object-type 'alist)
        (json-key-type 'symbol)
        (json-array-type 'vector)
        (json-false :json-false)
        (json-null nil))
    ,@body))

(defun gee-json-read ()
  "Parse and return the JSON object following point.
Advances point just past JSON object.  Uses `read-json' with
`json-object-type', `json-key-type', `json-array-type',
`json-false', and `json-null' set to default values."
  (with-gee-json-options (json-read)))

(defun gee-json-read-string ()
  "Parse and return the JSON object following point.
Advances point just past JSON object.  Uses `read-json' with
`json-object-type', `json-key-type', `json-array-type',
`json-false', and `json-null' set to default values."
  (with-gee-json-options (json-read-string)))

(defun gee--rest-request (url &optional body-if-200)
  "Request URL and return the resulting json-object.
Some requests will return a string instead.  JSON-objects are
coded with the standard options for `json-read' set explicitly by
this function.  Return the HTTP status code if the response body
does not start with \")]}'\".

If BODY-IF-200 is non-nil, return the response body even if it
doesn't start with \")]}'\" as long as the status code is 200."
  (with-current-buffer
    ;; TODO handle username and password
    (url-retrieve-synchronously url)
    ;; TODO check url-http-response-status
    (goto-char url-http-end-of-headers)
    (if (not (search-forward ")]}'\n" nil t))
        (if (not body-if-200)
            url-http-response-status
          (skip-syntax-forward "\s")
          (buffer-substring (point) (point-max)))
      (if (cl-find (char-after) "[{")
          (gee-json-read)
        (buffer-substring (point) (line-end-position))))))

;;; Account endpoints

;;;###autoload
(defun gee-gerrit-rest-account (&optional account-id)
  "Fetch the AccountInfo object for ACCOUNT-ID.
If ACCOUNT-ID is nil, fetch the AccountInfo object for `self'."
  (gee--rest-request
   (gee--gerrit-rest-build-url
    (format "/a/accounts/%s" (or account-id "self")))))

(defun gee-gerrit-rest-account-star (change-id &optional account-id)
  "Star change CHANGE-ID.
The change is starred for the current user, except if ACCOUNT-ID
is set.  Starred changes are returned for the search query
is:starred or starredby:USER and automatically notify the user
whenever updates are made to the change."
  (with-current-buffer
	  (let ((url-request-method "PUT"))
		(url-retrieve-synchronously
		 (gee--gerrit-rest-build-url
		  (format "/a/accounts/%s/starred.changes/%s"
				  (or account-id "self") change-id))))
	;; A 204 status is expected
	))

(defun gee-gerrit-rest-account-unstar (change-id &optional account-id)
  "Unstar a change CHANGE-ID.
Removes the starred flag, stopping notifications.
The change is unstarred for the current user, except if ACCOUNT-ID
is set."
  (with-current-buffer
	  (let ((url-request-method "DELETE"))
		(url-retrieve-synchronously
		 (gee--gerrit-rest-build-url
		  (format "/a/accounts/%s/starred.changes/%s"
				  (or account-id "self") change-id))))
	;; A 404 status is expected
	))

;;; Changes endpoints

;; create change

;;;###autoload
(defun gee-gerrit-rest-changes-list (query &optional limit start &rest options)
  "Call the query-changes API.
QUERY is a Gerrit query string or list of Gerrit query strings.
LIMIT specifies the maximum number of returned changes, nil means
no limit.  The START query parameter can be supplied to skip a
number of changes from the list.  OPTIONS can be any of the words
supported by the Gerrit query API: LABELS, DETAILED_LABELS,
CURRENT_REVISION, ALL_REVISIONS, DOWNLOAD_COMMANDS,
DRAFT_COMMENTS, CURRENT_COMMIT, ALL_COMMITS, CURRENT_FILES,
ALL_FILES, DETAILED_ACCOUNTS, MESSAGES, CURRENT_ACTIONS,
CHANGE_ACTIONS, REVIEWED, WEB_LINKS, CHECK.  All OPTIONS are
passed verbatim and without checks to the REST API."
  (let ((qlist (list (cons 'q (if (stringp query) (list query) query)))))
	(when limit (push (list 'n limit) qlist))
	(when start (push (list 'S start) qlist))
	(dolist (option options)
	  (push (list 'o option) qlist))
    (gee--rest-request
     (gee--gerrit-rest-build-url "/a/changes/" qlist))))

(defun gee-gerrit-rest-get-change (change-id &rest options)
  "Retrieves the ChangeInfo object representing CHANGE-ID.
Additional fields can be obtained by adding words as OPTIONS,
each option requires more database lookups and slows down the
query response time to the client.  Supported options are:
LABELS, DETAILED_LABELS, CURRENT_REVISION, ALL_REVISIONS,
DOWNLOAD_COMMANDS, DRAFT_COMMENTS, CURRENT_COMMIT, ALL_COMMITS,
CURRENT_FILES, ALL_FILES, DETAILED_ACCOUNTS, MESSAGES,
CURRENT_ACTIONS, CHANGE_ACTIONS, REVIEWED, WEB_LINKS, CHECK."
  (gee--rest-request
   (gee--gerrit-rest-build-url
    (format "/a/changes/%s" change-id)
    (mapcar (apply-partially 'list 'o) options))))

(defun gee-gerrit-rest-get-change-detail (change-id &rest options)
  "Retrieves ChangeInfo for CHANGE-ID with more details.
Labels, detailed labels, detailed accounts, and messages are
included by default.

Additional fields can be obtained by adding words as OPTIONS, each
option requires more database lookups and slows down the query
response time to the client so they are generally disabled by
default.  Supported options are: LABELS, DETAILED_LABELS,
CURRENT_REVISION, ALL_REVISIONS, DOWNLOAD_COMMANDS,
DRAFT_COMMENTS, CURRENT_COMMIT, ALL_COMMITS, CURRENT_FILES,
ALL_FILES, DETAILED_ACCOUNTS, MESSAGES, CURRENT_ACTIONS,
CHANGE_ACTIONS, REVIEWED, WEB_LINKS, CHECK.

Return a ChangeInfo entity that describes the change.  This
response will contain all votes for each label and include one
combined vote.  The combined label vote is calculated in the
following order (from highest to lowest): REJECTED > APPROVED >
DISLIKED > RECOMMENDED."
  (gee--rest-request
   (gee--gerrit-rest-build-url
    (format "/a/changes/%s/detail" change-id)
    (mapcar (apply-partially 'list 'o) options))))

;;* Get Topic
;;* Set Topic
;;* Delete Topic
;;* Abandon Change
;;* Restore Change
(defun gee-gerrit-rest-restore-change (change-id)
  "Restore a change with CHANGE-ID.
The request body does not need to include a RestoreInput entity
if no review comment is added.
Return a ChangeInfo entity that describes the restored change."
  (let ((url-request-method "POST"))
    (gee--rest-request
     (gee--gerrit-rest-build-url
      (format "/a/changes/%s/restore" change-id)))))

;;* Rebase Change
;;* Revert Change
;;* Submit Change
;;* Publish Draft Change
;;* Delete Draft Change
;;* Get Included In
;;* Index Change
;;* Check change
;;* Fix change

;; change edit endpoints

;;* Get Change Edit Details
;;* Change file content in Change Edit
;;* Restore file content or rename files in Change Edit
;;* Change commit message in Change Edit
;;* Delete file in Change Edit
;;* Retrieve file content from Change Edit
;;* Retrieve meta data of a file from Change Edit
;;* Retrieve commit message from Change Edit or current patch set of the change
;;* Publish Change Edit
;;* Rebase Change Edit
;;* Delete Change Edit

;; reviewer endpoints

;;* List Reviewers
;;* Suggest Reviewers
;;* Get Reviewer

(defun gee-gerrit-rest-change-add-reviewer (change-id reviewer &optional confirm)
  "Add one user or all members of one group as REVIEWER to the CHANGE-ID.
For groups, optionally CONFIRM the change.  As response an
AddReviewerResult entity is returned that describes the newly
added reviewers."
  (let ((reviewer-input (list (cons 'reviewer reviewer))))
	(when confirm
	  (push (cons 'confirmed t) reviewer-input))
	(with-current-buffer
		(let ((url-request-method "POST")
              (url-request-extra-headers '(("Content-Type" . "application/json")))
              (url-request-data (encode-coding-string (json-encode reviewer-input) 'utf-8 t)))
          (url-retrieve-synchronously
           (gee--gerrit-rest-build-url (format "/a/changes/%s/reviewers"
                                               change-id))))
	  (if (= url-http-response-status 200)
		  (progn (goto-char url-http-end-of-headers)
				 (search-forward ")]}'\n")
				 (gee-json-read))))))

;;* Delete Reviewer

;; revision endpoints

;;* Get Commit
;;* Get Revision Actions
;;* Get Review
;;* Get Related Changes

(defun gee-gerrit-rest-change-revision-set-review
    (change-id revision-id &optional message
               labels comments drafts not-strict-labels notify on-behalf-of
               )
  "Set a review on change CHANGE-ID, revision REVISION-ID.
Optional argument MESSAGE is the message to be added as review
comment.
Optional argument LABELS can be an alist that maps the
label names to the voting values.
Optional argument COMMENTS can be an alist that maps a file path
to a list of `CommentInput' entities.
Optional argument DRAFTS defines how draft comments are handled
that are already in the database but that were not also described
in this input.  Allowed values are \"DELETE\", \"PUBLISH\" and
\"KEEP\".  If not set, the default is \"KEEP\".
Optional argument NOT-STRICT-LABELS defines what to do whether
all labels are required to be within the user’s permitted ranges
based on access controls.  If nil, attempting to use a label not
granted to the user will fail the entire modify operation early.
If non-nil, the operation will execute anyway, but the proposed
labels will be modified to be the \"best\" value allowed by the
access controls.
Optional argument NOTIFY defines to whom email notifications
should be sent after the review is stored.  Allowed values are
\"NONE\", \"OWNER\", \"OWNER_REVIEWERS\" and \"ALL\".  If not
set, the default is \"ALL\".
Optional argument ON-BEHALF-OF can be an account-id which the
review should be posted on behalf of.  To use this option the
caller must have been granted labelAs-NAME permission for all
keys of labels."
  (let ((review-input `((drafts . ,(or drafts "KEEP")))))
    (when (and message (not (string= "" message)))
      (push (cons 'message message) review-input))
    ;; TODO: Sanity check labels
    (when labels (push (cons 'labels labels) review-input))
    ;; TODO: Sanity check comments
    (when comments (push (cons 'comments comments) review-input))
    (when not-strict-labels (push (cons 'strict_labels 'false) review-input))
    ;; TODO: Sanity check notify
    (when notify (push (cons 'notify notify) review-input))
    (when on-behalf-of (push (cons 'on_behalf_of on-behalf-of) review-input))
    (with-current-buffer
        (let ((url-request-method "POST")
              (url-request-extra-headers '(("Content-Type" . "application/json")))
              (url-request-data (encode-coding-string (json-encode review-input) 'utf-8 t)))
          (url-retrieve-synchronously
           (gee--gerrit-rest-build-url (format "/a/changes/%s/revisions/%s/review"
                                               change-id revision-id))))
      (goto-char url-http-end-of-headers)
      (search-forward ")]}'\n")
      (gee-json-read))))

;;* Rebase Revision
;;* Submit Revision
;;* Publish Draft Revision
;;* Delete Draft Revision
;;* Get Patch

(defun gee-gerrit-rest-change-revision-get-mergeable (change-id &optional revision-id
                                                                other-branches)
  "Check submit method and mergability of CHANGE-ID revision REVISION-ID.
Gets the method the server will use to submit (merge) the change
and an indicator if the change is currently mergeable.  Return a
MergeableInfo entity.  REVISION-ID defaults to \"current\".  If
OTHER-BRANCHES is non-nil, the mergeability will also be checked
for all other branches.  The returned MergableInfo object will
then contain a list of all other branches where this changes
could merge cleanly."
  (gee--rest-request
   (gee--gerrit-rest-build-url
    (format "/a/changes/%s/revisions/%s/mergeable"
            change-id revision-id)
    (when other-branches '(other-branches)))))

(defun gee-gerrit-change-revision-get-submit-type (change-id &optional revision-id)
  "Gets the method the server will use to submit CHANGE-ID revision REVISION-ID.
REVISION-ID defaults to \"current\"."
  (gee--rest-request
   (gee--gerrit-rest-build-url
    (format "/a/changes/%s/revisions/%s/submit_type"
            change-id (or revision-id "current")))))

;;* Get Submit Type
;;* Test Submit Type
;;* Test Submit Rule

(defun gee-gerrit-rest-list-drafts (change-id &optional revision-id)
  "List the draft comments of CHANGE-ID REVISION-ID.
Only includes draft comments that belong to the calling user.
Return an alist that maps the file path to a list of CommentInfo
entries.  The entries in the map are sorted by file path."
  (gee--rest-request
   (gee--gerrit-rest-build-url
    (format "/a/changes/%s/revisions/%s/drafts/"
            change-id (or revision-id "current")))))

(defun gee-gerrit-rest-create-draft (change-id comment-input &optional revision-id)
  "Create a draft comment on the specified CHANGE-ID.
Comment options should be set in a CommentInput JSON object in
argument COMMENT-INPUT.  A CommentInfo object is returned if the
draft comment is successfully created.  The comment is added to
the \"current\" revision unless another REVISION-ID is given."
  (with-current-buffer
	  (let ((url-request-method "PUT")
            (url-request-extra-headers '(("Content-Type" . "application/json")))
			(url-request-data (encode-coding-string (json-encode comment-input) 'utf-8 t)))
		(url-retrieve-synchronously
		 (gee--gerrit-rest-build-url
 		  (format "/a/changes/%s/revisions/%s/drafts"
				  change-id (or revision-id "current")))))
	(goto-char url-http-end-of-headers)
	(search-forward ")]}'\n")
	(gee-json-read)))

(defun gee-gerrit-rest-get-draft (change-id draft-id &optional revision-id)
  "Retrieves a draft comment for the specified CHANGE-ID and DRAFT-ID.
REVISION-ID specifies the revision, nil means \"current\".  As
response a CommentInfo entity is returned that describes the
draft comment."
  (gee--rest-request
   (gee--gerrit-rest-build-url
    (format "/a/changes/%s/revisions/%s/drafts/%s"
            change-id (or revision-id "current") draft-id))))

(defun gee-gerrit-rest-update-draft (change-id draft-id comment-input &optional revision-id)
  "Update a draft comment on the specified CHANGE-ID.
DRAFT-ID specifies the draft comment to update, and must match
the `id' attribute of COMMENT-INPUT.  Comment options should be
set in a CommentInput JSON object in argument COMMENT-INPUT.  A
CommentInfo object is returned if the draft comment is
successfully created.  The comment is added to the \"current\"
revision unless another REVISION-ID is given."
  (with-current-buffer
	  (let ((url-request-method "PUT")
            (url-request-extra-headers '(("Content-Type" . "application/json")))
			(url-request-data (encode-coding-string (json-encode comment-input) 'utf-8 t)))
		(url-retrieve-synchronously
		 (gee--gerrit-rest-build-url
 		  (format "/a/changes/%s/revisions/%s/drafts/%s"
				  change-id (or revision-id "current") draft-id))))
	(goto-char url-http-end-of-headers)
	(search-forward ")]}'\n")
	(gee-json-read)))

(defun gee-gerrit-rest-delete-draft (change-id draft-id &optional revision-id)
  "Deletes a draft comment from specified CHANGE-ID.
DRAFT-ID specifies the draft to delete.  REVISION-ID is the
targetted revision, nil is interpreted as \"current\"."
  (with-current-buffer
	  (let ((url-request-method "DELETE"))
		(url-retrieve-synchronously
		 (gee--gerrit-rest-build-url
		  (format "/a/changes/%s/revisions/%s/drafts/%s"
				  change-id (or revision-id "current") draft-id))))
	;; A 204 status is expected
	))

(defun gee-gerrit-rest-list-comments (change-id &optional revision-id)
  "List the published comments of CHANGE-ID REVISION-ID.
Return an alist that maps the file path to a list of CommentInfo
entries.  The entries in the map are sorted by file path."
  (gee--rest-request
   (gee--gerrit-rest-build-url
    (format "/a/changes/%s/revisions/%s/comments/"
            change-id (or revision-id "current")))))

(defun gee-gerrit-rest-list-change-comments (change-id &optional enable-context context-padding)
  "Lists the published comments of all revisions of the change CHANGE-ID.
Returns a map of file paths to lists of CommentInfo entries.  The
entries in the map are sorted by file path, and the comments for
each path are sorted by patch set number.  Each comment has the
patch_set and author fields set.
If the ENABLE-CONTEXT request parameter is set to true, the
comment entries will contain a list of ContextLine containing the
lines of the source file where the comment was written.
The CONTEXT-PADDING request parameter can be used to specify an
extra number of context lines to be added before and after the
comment range.  This parameter only works if enable-context is set
to true."
  (let (params)
	(when enable-context
	  (when context-padding
		(push (list 'context-padding context-padding) params))
	  (push (list 'enable-context "t") params))
	(gee--rest-request
	 (gee--gerrit-rest-build-url
	  (format "/a/changes/%s/comments" change-id) params))))

;;* Get Comment

(defun gee-gerrit-rest-list-files (change-id &optional revision-id reviewed query)
  "List the files that were modified, added or deleted in CHANGE-ID REVISION-ID.
Return an alist that maps the file path to a list of FileInfo
entries.  The entries in the alist are sorted by file path.
REVIEWED and QUERY may not be used together.
If REVIEWED is non-nil, return a list of the paths the caller has
marked as reviewed.  Clients that also need the FileInfo should
make two requests.
If QUERY is a string, return a list of all files (modified or
unmodified) that contain that substring in the path name.  This
is useful to implement suggestion services finding a file by
partial name.  Only files in the project (and branch?) identified
by CHANGE-ID may be listed."
  (gee--rest-request
   (gee--gerrit-rest-build-url
    (format "/a/changes/%s/revisions/%s/files/" change-id (or revision-id "current"))
    (cond ((and reviewed query) (error "Cannot combine reviewed and query"))
          (reviewed (list (cons 'reviewed nil)))
          (query (list (cons 'query query)))))))

(defun gee-gerrit-rest-change-get-content (change-id revision-id file-id)
  "Gets the content of a file from a certain revision.
CHANGE-ID identifies the changeset, REVISION-ID the revision of
the changeset and FILE-ID the file."
  (unless (stringp file-id)
    (setq file-id (format "%s" file-id)))
  (and (numberp revision-id)
       (= 0 revision-id)
       (setq revision-id "base"))
  (base64-decode-string
   (gee--rest-request
    (gee--gerrit-rest-build-url
     (format "/a/changes/%s/revisions/%s/files/%s/content"
             change-id (or revision-id "current")
             (if (cl-find ?/ file-id) (url-hexify-string file-id) file-id)))
    :body-if-200)))

(defun gee-gerrit-rest-get-diff (change-id revision-id file-id &optional base intraline weblinks-only)
  "Get the diff of a file from a certain CHANGE-ID REVISION-ID.
The FILE-ID is the path of the file.  Return a DiffInfo alist
that describes the diff.  BASE can be set to control the base
patch set from which the diff should be generated.  If INTRALINE
is non-nil, intraline differences are included in the diff.  If
WEBLINKS-ONLY is non-nil, return only the diff web links."
  (gee--rest-request
   (gee--gerrit-rest-build-url
    (format "/a/changes/%s/revisions/%s/files/%s/diff"
            change-id revision-id (url-hexify-string
                                   (if (symbolp file-id)
                                       (symbol-name file-id) file-id)))
    (append (when intraline '((intraline)))
            (when base `((base ,base)))
            (when weblinks-only '((weblinks)))))))

;;* Set Reviewed
;;* Delete Reviewed
;;* Cherry Pick Revision

;; Documentation endpoints

;;;###autoload
(defun gee-browse-gerrit-documentation (&optional doc)
  "Browse the Gerrit docs using `eww'.
The docs are downloaded from the server specified by
`gee-gerrit-rest-base'.  Will open index.html unless DOC specifes
some other file."
  (interactive)
  (eww (gee--gerrit-rest-build-url
		(concat "/Documentation/" (or doc "index.html")))))

;;; Projects endpoints

(cl-defun gee-gerrit-rest-projects-list
	(&key branch description limit prefix
		  regex skips substring tree type)
  "Call the list projects API.
BRANCH: Limit the results to projects having the specified
branch, and include the sha1 of the branch in the results.
DESCRIPTION: Include the project description if non-nil.
LIMIT: Limit the number of returned projects.
PREFIX: Limit the results to projects starting with the specified
string.
REGEX: Limit the results to projects matching the specified
regex. The regex must match the whole project name.
SKIP: Skip this number of matching projects from the beginning of the list.
SUBSTRING: Limit the results to projects matching the speficied string.
TREE: Get project inheritance in a tree format.  Conflicts with BRANCH.
TYPE: `ALL', `CODE' or `PERMISSIONS'. Limit results to projects of that type."
  (and branch tree
	   (user-error "BRANCH and TREE arguments can not be used together"))
  (and type (not (member type '("ALL" "CODE" "PERMISSIONS")))
	   (user-error "TYPE (%s) must be one of `ALL', `CODE', or `PERMISSIONS'" type))
  (gee--rest-request
   (gee--gerrit-rest-build-url "/a/projects/"
                               (cl-loop for (key val type) in
                                        `(("b" ,branch)
                                          ("d" ,description bool)
                                          ("n" ,limit)
                                          ("p" ,prefix) ("r" ,regex)
                                          ("S" ,skips) ("m" ,substring)
                                          ("t" ,tree bool) ("type" ,type))
                                        if val
                                        collect (if (eq type 'bool) (list key)
                                                  (list key val))))))

;;;###autoload
(defun gee-gerrit-rest-projects-get (project-name)
  "Retrieves a ProjectInfo entity that describes PROJECT-NAME."
  (gee--rest-request
   (gee--gerrit-rest-build-url
    (concat "/a/projects/" (url-hexify-string project-name)))))

;;;###autoload
(defun gee-gerrit-rest-projects-create (project-name &optional project-input)
  "Create project PROJECT-NAME.
Further project options can be set in a ProjectInput JSON object
in argument PROJECT-INPUT.  A ProjectInfo object is returned if
the project is successfully created."
  (when project-input
	;; make sure 'name' in project-input matches the project-name.
	(let ((name (or (assoc 'name project-input) (assoc "name" project-input))))
	  (if name
		  (setcdr name project-name)
		(push (cons 'name project-name) project-input))))
  (with-current-buffer
	  (let ((url-request-method "PUT")
			(url-request-data (encode-coding-string (json-encode project-input) 'utf-8 t)))
		(url-retrieve-synchronously
		 (gee--gerrit-rest-build-url (concat "/a/projects/" (url-hexify-string project-name))))
	(goto-char url-http-end-of-headers)
	(search-forward ")]}'\n")
	(gee-json-read))))

;;;###autoload
(defun gee-gerrit-rest-project-get-description (project-name)
  "Get the description of project PROJECT-NAME."
  (gee--rest-request
   (gee--gerrit-rest-build-url
    (format "/a/projects/%s/description" (url-hexify-string project-name)))))

;;;###autoload
(defun gee-gerrit-rest-project-set-description (project-name &optional description commit-message)
  "Set the PROJECT-NAME DESCRIPTION.
An optional COMMIT-MESSAGE may be provided.  If DESCRIPTION is
nil (or not provided) the project description will be deleted."
  (with-current-buffer
	  (let ((url-request-method "PUT")
			(url-request-data
			 (encode-coding-string (json-encode `((description . ,description)
												  (commit_message . ,commit-message))) 'utf-8 t)))
		(url-retrieve-synchronously
		 (gee--gerrit-rest-build-url (format "/a/projects/%s/description" (url-hexify-string project-name)))))
	(goto-char url-http-end-of-headers)
	(search-forward ")]}'\n")
	(buffer-substring (point) (point-max))))

;;;###autoload
(defun gee-gerrit-rest-project-get-parent (project-name)
  "Get the parent of project PROJECT-NAME."
  (gee--rest-request
   (gee--gerrit-rest-build-url
    (format "/a/projects/%s/parent" (url-hexify-string project-name)))))

;;;###autoload
(defun gee-gerrit-rest-project-set-parent (project-name parent &optional commit-message)
  "Set the PROJECT-NAME PARENT.
An optional COMMIT-MESSAGE may be provided."
  (with-current-buffer
	  (let ((url-request-method "PUT")
			(url-request-data
			 (encode-coding-string (json-encode `((parent . ,parent)
												  (commit_message . ,commit-message))) 'utf-8 t)))
		(url-retrieve-synchronously
		 (gee--gerrit-rest-build-url (format "/a/projects/%s/parent" (url-hexify-string project-name)))))
	(goto-char url-http-end-of-headers)
	(search-forward ")]}'\n")
	(gee-json-read)))

;;;###autoload
(defun gee-gerrit-rest-project-get-head (project-name)
  "Retrieves for project PROJECT-NAME the name of the branch to which HEAD points."
  (gee--rest-request
   (gee--gerrit-rest-build-url
    (format "/a/projects/%s/HEAD" (url-hexify-string project-name)))))

;;;###autoload
(defun gee-gerrit-rest-project-set-head (project-name ref)
  "Set the PROJECT-NAME HEAD to REF."
  (with-current-buffer
	  (let ((url-request-method "PUT")
			(url-request-data
			 (encode-coding-string (json-encode `((ref . ,ref))) 'utf-8 t)))
		(url-retrieve-synchronously
		 (gee--gerrit-rest-build-url (format "/a/projects/%s/HEAD" (url-hexify-string project-name)))))
	(goto-char url-http-end-of-headers)
	(search-forward ")]}'\n")
	(gee-json-read)))

;;;###autoload
(defun gee-gerrit-rest-project-statistics-git (project-name)
  "Return statistics for the repository of project PROJECT-NAME."
  (gee--rest-request
   (gee--gerrit-rest-build-url
    (format "/a/projects/%s/statistics.git"
            (url-hexify-string project-name)))))

;;;###autoload
(defun gee-gerrit-rest-project-get-config (project-name)
  "Gets some configuration information about project PROJECT-NAME.
Note that this config info is not simply the contents of
project.config; it generally contains fields that may have been
inherited from parent projects."
  (gee--rest-request
   (gee--gerrit-rest-build-url
    (format "/a/projects/%s/config"
            (url-hexify-string project-name)))))

;; TODO add set-config function here when a good signature has been found
;; TODO add run-gc function here when a good construct has been found
;; TODO add ban-commit function here

;; project-branch endpoints

;;;###autoload
(defun gee-gerrit-rest-project-branches (project-name &optional limit skip substring regex)
  "List the branches of project PROJECT-NAME.
When set, LIMIT the number of branches to be included in the
results.  When set, SKIP the given number of branches from the
beginning of the list.  When set, Limit the results to those
projects that match the specified SUBSTRING.  When set, Limit the
results to those branches that match the specified REGEX.
Boundary matchers '^' and '$' are implicit.  For example: the
regex 't*' will match any branches that start with 'test' and
regex '*t' will match any branches that end with 'test'.  As
result a list of BranchInfo entries is returned."
  (let (query)
    (when limit (push (cons 'n limit) query))
    (when skip (push (cons 's skip) query))
    (when substring (push (cons 's substring) query))
    (when regex (push (cons 's regex) query))
    (gee--rest-request
     (gee--gerrit-rest-build-url
      (format "/a/projects/%s/branches/"
              (url-hexify-string project-name))
      query))))

;;;###autoload
(defun gee-gerrit-rest-project-get-branch (project-name branch)
  "Retrieves a BranchInfo object for the project PROJECT-NAME BRANCH."
  (gee--rest-request
   (gee--gerrit-rest-build-url (format "/a/projects/%s/branches/%s"
                                       (url-hexify-string project-name)
                                       (url-hexify-string branch)))))

;; TODO add create-branch function here

;;;###autoload
(defun gee-gerrit-rest-project-delete-branch (project-name branch-id)
  "Delete a branch from project PROJECT-NAME named BRANCH-ID."
  (with-current-buffer
	  (let ((url-request-method "DELETE"))
		(url-retrieve-synchronously
		 (gee--gerrit-rest-build-url (format "/a/projects/%s/branches/%s"
											 (url-hexify-string project-name)
											 (url-hexify-string branch-id)))))
	(goto-char url-http-end-of-headers)
	(search-forward ")]}'\n")
	(gee-json-read)))

;; TODO add delete-branches function here

;;;###autoload
(defun gee-gerrit-rest-project-branch-get-content (project-name branch-id file-id)
 "Gets the content of a file from the HEAD revision of a certain branch.
PROJECT-NAME is the project, BRANCH-ID the branch, and FILE-ID
the file."
 (base64-decode-string
  (gee--rest-request
   (gee--gerrit-rest-build-url (format "/a/projects/%s/branches/%s/files/%s/content"
                                       (url-hexify-string project-name)
                                       (url-hexify-string branch-id)
                                       (url-hexify-string file-id))))))

;; project child-project endpoints
;; project tag endpoints
;; project commit endpoints

;;  * Get Commit

(defun gee-gerrit-rest-project-commit-get-content (project-name commit-id file-id)
  "Gets the content of a file from a certain commit.
PROJECT-NAME identifies the project, COMMIT-ID the commit and FILE-ID the file."
  (let ((text (gee--rest-request
               (gee--gerrit-rest-build-url
                (format "/a/projects/%s/commits/%s/files/%s/content"
                        (url-hexify-string project-name) commit-id
                        (url-hexify-string (if (symbolp file-id)
                                               (symbol-name file-id)
                                             file-id)))) :body-if-200)))
    (if (string= text "Not Found\n")
        ""
      (base64-decode-string text))))


;; project dashboard endpoints

;;;###autoload
(defun gee-gerrit-rest-project-list-dashboards (project-name)
  "List custom dashboards for project PROJECT-NAME.
As result a list of DashboardInfo entries is returned."
  (gee--rest-request
   (gee--gerrit-rest-build-url (format "/a/projects/%s/dashboards/"
                                       (url-hexify-string project-name)))))

;;;###autoload
(defun gee-gerrit-rest-project-get-dashboard (project-name &optional dashboard)
  "Retrieves PROJECT-NAME DASHBOARD.
The DASHBOARD can be defined on that project or be inherited from
a parent project.  As response a DashboardInfo entity is returned
that describes the DASHBOARD.  Retrieve the default DASHBOARD if nil."
  (setq dashboard (or dashboard "default"))
  (let ((dashboard-info
		 (gee--rest-request
		  (gee--gerrit-rest-build-url (format "/a/projects/%s/dashboards/%s"
											  (url-hexify-string project-name)
											  (url-hexify-string dashboard))))))
	(if (and (integerp dashboard-info) (= 404 dashboard-info))
		nil
	  dashboard-info)))

(provide 'gee-gerrit-rest)
;;; gee-gerrit-rest.el ends here
