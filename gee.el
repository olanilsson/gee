;;; gee.el --- GEE - The Gerrit on Emacs Experience    -*- lexical-binding: t; -*-

;; Copyright (C) 2015, 2016 Ola Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Maintainer: Ola Nilsson <ola.nilsson@gmail.com>
;; URL: https://bitbucket.org/olanilsson/gee
;; Keywords: gerrit
;; Version: 0.4.1
;; Package-Requires: ((emacs "26.3") (let-alist "1.0.4") (cl-lib "0.5") (anaphora "1.0.0") (seq "2.16"))


;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Gee - The Gerrit Emacs Experience

;; **Work in progress!** Gee is very much a work in progress and any part
;;   may change without warning.

;; Gee aims to be a complete Gerrit UI in Emacs.

;; Each Gerrit view should be provided, but replace some parts with more
;; appropriate Emacs constructs.

;; Gee lets you customize the UI.

;; The Gerrit Web UI provides very few customization options.  Gee wants
;; you to add and remove parts of the UI, add new actions, replace entire
;; views and so on.

;; Gee lets you create your own workflows.

;; By providing customization points in as many places as possible, gee
;; lets you hook actions together to create new, more efficient ways of
;; working for.

;;

;;; Code:

(require 'tabulated-list)
(require 'gee-faces)
(require 'gee-util)
(require 'gee-gerrit-rest)
(eval-when-compile (require 'cl-lib))
(require 'cl-lib)
(require 'let-alist)
(require 'anaphora)

(declare-function gee-view-change "gee-review" (change-id))

(defgroup gee-changelist nil "Gee changelist settings" :group 'gee)

(defcustom gee-changelist-use-header-line nil
  "Whether changelists should use the header line for column headings.
Any tabulated view may become wider than the window, and then the
column headings won't follow the cursor when scrolling left."
  :group 'gee-changelist
  :type 'boolean)

(defcustom gee-changelist-columns
  '("ID" "Subject" "Status" "OwnerFullname" "Project" "Branch" "Updated" "Size" "CodeReview" "Verified")
  "Columns displayed by `gee-changelist-mode' for change queries and dashboards.
Shall be a list of keys matching keys in
`gee-changelist-column-definitions'.  If a key is not found in
`gee-changelist-column-definitions' it is used as if it was
present as a string."
  :group 'gee-changelist
  :type '(repeat string)
  )

(defvar-local gee-changeinfo []
  "A vector of (or possibly vector of vector of) ChangeInfo objects.
`gee-changelist-mode' will display these objects.")
(put 'gee-changeinfo 'permanent-local t)

(defvar-local gee-changeinfo-refresh-function nil
  "A function that will fetch the list content.
Set by user-facing changelist query functions to be used to
refresh the list.")
(put 'gee-changeinfo-refresh-function 'permanent-local t)

(defun gee--change-query-action (button)
  "Action function for change query buttons.
Reads the `gee-query-args' property at BUTTON and uses its value
as arguments to `gee-changes-query'."
  (let ((queries (get-text-property button 'gee-query-args)))
	(if queries
		(apply #'gee-changes-query (get-text-property button 'gee-query-args))
	  (message "No query attaced to button at %s" button))))

;; button type for change queries
(define-button-type 'gee-change-query-button
  'action #'gee--change-query-action
  :supertype 'gee-button)

(defun gee-text-query-button (label &rest queries)
  "Return LABEL as a `gee-change-query-button' for QUERIES.
The returned button is a text button."
  (make-text-button label 0 :type 'gee-change-query-button 'gee-query-args queries))

(defun gee-change-owner-button (button)
  "Button action function that opens the default dashboard for owner BUTTON."
  (let* ((change-info (get-text-property button 'change-info))
		 (account-info (gee-json-property 'owner change-info))
		 (user-email (gee-account-property 'email account-info)))
	(gee-changes-query (format "owner:%s+status:open" user-email))))

(defun gee-change-project-button (button)
  "Button action function that opens the default dashboard for project BUTTON."
  (let* ((project (gee-json-property 'project (get-text-property button 'change-info)))
		 (dashboard-info (gee-gerrit-rest-project-get-dashboard project)))
	(if dashboard-info
		(gee-show-dashboard dashboard-info)
	  (gee-changes-query (format "is:open+project:%s" project)))))

(defun gee-change-branch-button (button)
  "Button action function that opens a branch-and-topic query for BUTTON.
Opens the query for open changes with the same branch, and topic
if the change at BUTTON has a topic."
  (let* ((change-info (get-text-property button 'change-info))
		 (project (gee-json-property 'project change-info))
		 (branch (gee-json-property 'branch change-info))
		 (topic (gee-json-property 'topic change-info)))
	(gee-changes-query (format "is:open+project:%s+branch:%s%s"
		   project branch (if topic (concat "+topic:" topic) "")))))

(defun gee-change-size (change-info)
  "Return the total number of changed lines in CHANGE-INFO as a string."
  (let-alist change-info
    (number-to-string (+ .insertions .deletions))))

(defun gee-change-short-time (git-time)
  "Format GIT-TIME in a short format.
GIT-TIME is in YYYY-MM-DD hh:mm:ss.mmmuuunnn format.  Short
format is hh:mm when GIT-TIME is the same date as today, MM-DD if
the same year, and YYYY-MM-DD otherwise."
  (let* ((now (current-time))
		 (time (gee-git-date-to-time git-time))
		 (formatted-time (format-time-string "%F" time))
		 (formatted-now (format-time-string "%F" now)))
	(cond ((string= formatted-now formatted-time)
		   (format-time-string "%R" time))
		  ((time-less-p (time-subtract now time) '(481 13184))
		   (substring formatted-time 5))
		  (t formatted-time))))

(defun gee-changelist-status (change-info)
  "Return the changelist status of CHANGE-INFO.
Draft, Merge conflict, Merged, Abandoned, or the empty string."
  (let-alist change-info
    (if (and (eq :json-false .mergeable) (not (string= "DRAFT" .status)))
        "Merge conflict"
      (if (string= .status "NEW")
          ""
        (capitalize .status)))))

(defcustom gee-changelist-column-definitions
  '(("ID" :width 7 :getter "_number" :formatter number-to-string :right-align t :button (action gee-visit-change-at-pos))
    ("Subject" :width 40 :button (action gee-visit-change-at-pos))
    ("Status" :width 9 :getter gee-changelist-status)
	("OwnerFullname" :label "Owner" :getter gee-owner-fullname :button (action gee-change-owner-button))
	("OwnerUsername" :label "Owner" :getter gee-owner-username :button (action gee-change-owner-button))
	("Project" :button (action gee-change-project-button))
	("Branch" :button (action gee-change-branch-button))
	("Topic" :getter gee-changeinfo-topic :button (action gee-change-branch-button))
	("Updated" :formatter gee-change-short-time :width 10)
	("UpdatedRelative" :label "Updated" :formatter gee-relative-git-time :width 14)
	("Size" :getter gee-change-size :width 5 :right-align t)
	("CodeReview" :label "CR" :getter gee-labels-code-review :width 2)
	("Verified" :label "V" :getter gee-labels-verified :width 2))
  "List of changelist column descriptors.

Each entry in the list specifies a *possible* column to include
in `gee-changelist-columns'.

If the descriptor is a string, that string is used both as key
and column label, and the value is the ChangeInfo attribute of
the same name (only lower case)."
  :group 'gee-changelist
  :type '(repeat (choice (string :tag "Key used as label and attribute")
						 (cons :tag "Key and properties"
							   (string :tag "Key")
							   (plist :tag "Properties"
									  :options ((:label (string :tag "Label"))
												(:getter (choice (string :tag "Attribute")
																 function))
												(:formatter (function :tag "Formatter"))
												(:width (integer :tag "Width"))
												(:right-align boolean)
												(:button (plist :tag "Button properties"
																:options ((action function)))
														 ;;TODO add options here
														 )
												))))))

(defun gee--sort-ci-on-number (ci-vector)
  "Sort CI-VECTOR ascending by `_number'."
  (cl-sort ci-vector #'< :key (lambda (ci) (cdr (assoc '_number ci)))))

(defun gee--load-queries (&rest queries)
  "Return the results of QUERIES."
  (let ((results (gee-gerrit-rest-changes-list queries nil nil "LABELS")))
    (if (vectorp (aref results 0))
        (cl-loop for r across-ref results
                 do (gee--sort-ci-on-number r))
      (gee--sort-ci-on-number results))
    results))

(defun gee-changes-query (query &rest queries)
  "Open changelist view for one QUERY and optionally more QUERIES."
  (interactive "squery: ")
  (let ((buf (get-buffer-create "*Gerrit*")))
	(with-current-buffer buf
      ;; keep data fetching function for refresh operations
      (setq gee-changeinfo-refresh-function
            (apply-partially #'gee--load-queries (cons query queries)))
	  (gee-changelist-mode))
	(switch-to-buffer buf)))

(defun gee--load-dashboard (dashboard-info)
  "Return the results of the queries in DASHBOARD-INFO.
The return value is a vector of (TITLE . [CHANGE-INFO]), one for
each `name' in DASHBOARD-INFO."
  (let-alist dashboard-info
    (let (queries names results)
	  ;; Create two lists, one of dashboard section names and one of
	  ;; corresponding queries.
	  (cl-loop for dashboard-section-info across .sections
			   collect (cdr (assoc 'name dashboard-section-info)) into ns
			   collect (cdr (assoc 'query dashboard-section-info)) into qs
			   finally (setq names ns queries qs))
	  ;; Send all the queries to the server in one request
	  (setq results (gee-gerrit-rest-changes-list queries nil nil "LABELS"))
      (cl-loop for r across-ref results
               do (gee--sort-ci-on-number r))
	  ;; combine each dashboard section title with the matching
	  ;; vector of ChangeInfo objects into a vector of cons-es of
	  ;; title and vector of ChangeInfo.
	  ;; [(TITLE . [CHANGE-INFO ...]) ...]
	  (cl-map 'vector 'cons names results))))

(defun gee-show-dashboard (dashboard-info)
  "Display the Gerrit dashboard defined by DASHBOARD-INFO."
  (let-alist dashboard-info
    (let (buf)
	  (setq buf (gee-buffer-create "*Gerrit:%s*" (or .title "dashboard")))
	  (with-current-buffer buf
        ;; keep data fetching function for refresh operations
        (setq gee-changeinfo-refresh-function
              (apply-partially #'gee--load-dashboard dashboard-info))
	    (gee-changelist-mode))
	  (switch-to-buffer buf))))

(define-obsolete-function-alias 'gee-default-dashboard 'gee-gerrit-dashboard-self "2016-01-23")

(defun gee-gerrit-dashboard-self ()
  "Open the personal dashboard dashboard/self."
  (interactive)
  (gee-show-dashboard `((id . "dashboard:self")
						(description . "My reviews")
						(url . "/dashboard/self")
						(title . "My reviews")
						(sections . [((query . "is:open+owner:self")
									  (name . "Outgoing reviews"))
									 ((query . "is:open+reviewer:self+-owner:self")
									  (name . "Incoming reviews"))
									 ((query . "is:closed+(owner:self+OR+reviewer:self)+limit:10")
									  (name . "Recently closed"))]))))

(defvar gee-gerrit-accounts nil "Cache of Gerrit user account information.")

(defun gee-account-property (property account-info)
  "Get PROPERTY from (possibly incomplete) ACCOUNT-INFO.
If ACCOUNT-INFO does not have any valuye for PROPERTY a full
AccountInfo object is fetched from the Gerrit server, unless
cached in `gee-gerrit-accounts'."
  (cl-macrolet ((json-get (key json) `(cdr (assoc ,key ,json))))
	(let ((account-id (json-get '_account_id account-info))
		  (value (json-get property account-info)))
	  (unless value
		(unless (integerp account-id)
		  (error "Expected '_account_id in ACCOUNT-INFO"))
		(setq account-info nil))
	  (unless value
		(setq account-info (json-get account-id gee-gerrit-accounts))
		(setq value (json-get property account-info)))
	  (unless value
		(setq account-info (gee-gerrit-rest-account account-id))
		(when account-info
		  (add-to-list 'gee-gerrit-accounts (cons account-id account-info))
		  (setq value (json-get property account-info))))
	  value)))

(defun gee--tablist-header-entry (title)
  "Create a tabulated-list entry for a section header TITLE.
The entry is a list (ID [TITLE \"\" \"\" ...] where the vector
should have as many entries as `tabulated-list-format'.  The ID
is \"section:TITLE\" in order to be recognized by
`gee-tablist-printer'."
  (let ((strings (make-vector (length gee-changelist-columns) "")))
	(aset strings 0 title)
	(list (format "section:%s" title) strings)))

(defun gee-changeinfo-entries (changeinfo-vector &optional col-widths)
  "Create a list of change entries for a Tabulated List buffer.
Reads CHANGEINFO-VECTOR and returns a list of elements (ID [DESC1
... DESCN]) suitable for `tabulated-list-entries', which see.

COL-WIDTHS may be a vector that will collect the max width of
each column.

The ID is always the full Gerrit `change-indfo' id:
`<project>~<branch>~<Change-Id>', and each DESCx is converted
according to the column specification in
`gee-changelist-column-definitions'.

If CHANGEINFO-VECTOR is a vector of vectors, it is flattened into a
list of elements as above.

If CHANGEINFO-VECTOR is a single vector of `change-info' object, it is
converted to a list of elements as above.

If CHANGEINFO-VECTOR is a vector of (NAME . VECTOR), each NAME will
be converted to a element (\"section:NAME\" [\"NAME\" ...])
representing section headers and each element in the vector will
be converted to an element as described above.  The resulting
list will be flattened."
  (cond ((vectorp (aref changeinfo-vector 0)) ; subvector
         (cl-loop for subvector across changeinfo-vector
                  append (gee--changeinfo-to-tablistentry subvector col-widths)))
        ;; section header (title . [changes])
        ((and (consp (aref changeinfo-vector 0))
              (stringp (car (aref changeinfo-vector 0)))
              (vectorp (cdr (aref changeinfo-vector 0))))
         (cl-loop for (title . changes) across changeinfo-vector
                  collect (gee--tablist-header-entry title)
                  append (gee--changeinfo-to-tablistentry changes col-widths)))
        (t (gee--changeinfo-to-tablistentry changeinfo-vector col-widths))))

(defcustom gee-changelist-starred-properties
  '(face gee-starred-face)
  "Text properties added to any starred change in the change table.
The properites are added to the entire line."
  :group 'gee-changelist
  :type 'plist )

(defun gee--col-label (col-desc)
  "Return the label/key of COL-DESC.
COL-DESC is a `tabulated-list-mode' column-descriptor described
in `tabulated-list-entries'."
  (if (stringp col-desc) col-desc (car col-desc)))

(defun gee--vector-collect-max (place columns)
  "Collect label maxlens from COLUMNS into PLACE.
PLACE is a vector of integers, COLUMNS is a vector of column
descriptors as described in `tabulated-list-entries'.
Return COLUMNS."
  (cl-loop for maxval across-ref place
           for otherval across columns
           do (cl-callf max maxval (length (gee--col-label otherval))))
  columns)

(defun gee--changeinfo-to-tablistentry (changeinfo &optional col-widths)
  "Convert CHANGEINFO vector to a list of `tabulated-list-mode' entries.
COL-WIDTHS may be a vector that will collect the max width of
each column."
  (unless col-widths
    (setq col-widths (make-vector (length gee-changelist-columns) 0)))
  (let ((col-funcs (cl-loop for col in gee-changelist-columns
                            collect (gee--get-colvalue-function col gee-changelist-column-definitions))))
    (cl-macrolet ((json-get (key list) `(cdr (assoc ,key ,list))))
	  (cl-loop
       for ci across changeinfo
	   collect (list
                (json-get 'id ci)
                (gee--vector-collect-max
                 col-widths
                 (vconcat (cl-loop for fn in col-funcs
                                   collect (funcall fn ci)))))))))

(defun gee--section-id-p (id)
  "Return non-nil if ID is a section line id."
  (and (stringp id) (string-prefix-p "section:" id)))

(defun gee-tablist-section-header-printer (inner-printer id cols)
  "`tabulated-list-printer' specialization that add section headers.
Meant to be used as with `add-function' :around, it passes ID and
COLS on to INNER-PRINTER.  If ID is a string with a `section:'
prefix, temporarily bind `tabulated-list-format' to format the
line as a single column section header line.  The section header
line is marked with the `gee-dashbord-section' face.

For normal lines, text properties are added to the entire line.
The associated ChangeInfo object is always added as the
`change-info' property, and the properties of
`gee-changelist-starred-properties' if the change is starred."
  (if (gee--section-id-p id)
	  ;; this is a section header
	  (let* ((tabulated-list-padding 0) ; do not pad section headings
			 (ncols (length tabulated-list-format))
			 ;; total width of normal columns, could be cached, but
			 ;; there is normally few sections in a screen.
			 (totalwidth (cl-loop for c across tabulated-list-format
								  sum (nth 1 c)))
			 ;; special format, equal number of columns, all but
			 ;; the first are width 1
			 (tabulated-list-format
			  (vconcat `[("Section" ,(- totalwidth ncols -1) nil)]
					   (make-vector (1- ncols) '("" 1 nil))))
			 (beg (point)))
		(funcall inner-printer id cols)
		;; add the section header face
		(put-text-property beg (point) 'face 'gee-dashboard-section))
	;; write a review line
	(funcall inner-printer id cols)
	))

(defun gee-tablist-add-star-printer (inner-printer id cols)
  "Apply `gee-changelist-starred-properties' to the tabline.
Passed ID and COLS to INNER-PRINTER to do the actual printing,
then applies `gee-changelist-starred-properties' to the entire
line if the first column has a `change-info' property with the
`starred' attribute set."
  (let ((beg (point)))
	(funcall inner-printer id cols)
	(when (aand (gee-change-info-from-col-entry (aref cols 0))
				(let-alist it .starred))
	  (add-text-properties beg (point) gee-changelist-starred-properties))))

(defun gee-tablist-add-changeinfo-printer (inner-printer id cols)
  "Apply `change-info' property of column 0 to the entire printed line.
Pass ID and COLS to INNER-PRINTER to print the line, then add
text property `change-info' of the first column to the entire
printed line.  No property is added if the property is not set on
the first column,"
  (let ((beg (point)))
	(funcall inner-printer id cols)
	(awhen (gee-change-info-from-col-entry (aref cols 0))
	  (add-text-properties beg (point) (list 'change-info it)))))

(defun gee-owner-fullname (change-info)
  "Return the CHANGE-INFO owners full name."
  (gee-account-property 'name (gee-json-property 'owner change-info)))

(defun gee-owner-username (change-info)
  "Return the CHANGE-INFO owners username."
  (gee-account-property 'username (gee-json-property 'owner change-info)))

(defun gee-changeinfo-topic (change-info)
  "Return the CHANGE-INFO topic, or the empty string if there is no topic."
  (let-alist change-info
    (or .topic "")))

(defun gee--format-label (label change-info)
  "Format and propertize the value of LABEL in CHANGE-INFO."
  (let* ((label-info (gee-json-property `(labels ,label) change-info))
		 (label-value (gee-json-property `value label-info))
		 (approved (gee-json-property 'approved label-info))
		 (rejected (gee-json-property 'rejected label-info)))
	(if (and (or (null label-value) (zerop label-value))
			 (null approved) (null rejected))
		""
	  (propertize (cond (label-value (format "%+d" label-value))
						(approved "GO")
						(rejected "NO")
						(t ""))
				  'face (if (or approved (< 0 (or label-value -1)))
							'gee-positive-label 'gee-negative-label)))))

(defun gee-labels-code-review (change-info)
  "Return the value of the `Code-Review' label of CHANGE-INFO."
  (gee--format-label 'Code-Review change-info))

(defun gee-labels-verified (change-info)
  "Return the value of the `Verified' label of CHANGE-INFO."
  (gee--format-label 'Verified change-info))

(defun gee--make-colvalue-function (key column-definitions)
  "Create a function that create a column entry for a `change-info' object.
KEY is a string that identifies an element in COLUMN-DEFINITIONS.
Each element of COLUMN is either a string or a list (NAME
. PLIST).  Uses the :getter, :formatter, and :button properties
of the PLIST."
  (let ((desc (car (cl-member key column-definitions
                              :test 'string=
                              :key (lambda (item)
                                     (if (stringp item) item
                                       (car item))))))
        getter formatter button-props form)
    ;; getter defaults to the key
    (setq getter (downcase key))
    ;; Extract relevant props from plist if present
    (anaphoric-when (and (consp desc) (cdr desc))
      (setq getter (or (plist-get it :getter) getter))
      (setq formatter (plist-get it :formatter))
      (setq button-props (plist-get it :button)))
    ;; the getter
    (setq form (cond ((stringp getter)
                      `(cdr (assoc-string ,getter change-info)))
                     ((symbolp getter)
                      (list getter 'change-info))
                     (t `(funcall ,getter change-info))))
    (when formatter    ;; maybe format
      (setq form (if (symbolp formatter)
                     (list formatter form)
                   (list 'funcall formatter form))))
    ;; always add change-info prop
    (setq form (list 'propertize form (quote (quote change-info)) 'change-info))
    (when button-props     ;; maybe button
      (setq form `(cons ,form ',button-props)))
    (setq form `(lambda (change-info)
                  ,(format "Create a `%s' column entry for `change-info'." key)
                  ,form))
    (let ((lexical-binding t))
      (byte-compile form))))

(defvar gee--colvalue-function-hash (make-hash-table :test 'equal)
  "Cache of colvalue functions.")

(eval-when-compile
  (dolist (definition gee-changelist-column-definitions)
    (let ((key (gee--col-label definition)))
      (puthash definition
               (gee--make-colvalue-function key gee-changelist-column-definitions)
               gee--colvalue-function-hash))))

(defun gee--get-colvalue-function (key column-definitions)
  "Return a function that create a column entry for a `change-info' object.
KEY is a string that identifies an element in COLUMN-DEFINITIONS.
Each element of COLUMN is either a string or a list (NAME
. PLIST).  Uses the :getter, :formatter, and :button properties
of the PLIST.  This function looks for an already created
function in `gee--colvalue-function-hash', and calls
`gee--make-colvalue-function' otherwise."
  (let ((definition (car (cl-member key column-definitions
                                    :test 'string=
                                    :key #'gee--col-label))))
    (anaphoric-if (gethash definition gee--colvalue-function-hash)
        it
      (puthash definition
               (gee--make-colvalue-function key (list definition))
               gee--colvalue-function-hash))))

(defun gee--changelist-format (column-list &optional column-max-widths)
  "Return a tabulated list format vector for the columns in COLUMN-LIST.
COLUMN-MAX-WIDTHS should be a sequence of column widths.  The
elements in the returned vectos should be lists (NAME WIDTH SORT
. PROPS) as specified in `tabulated-list-format'."
  (cl-macrolet ((plist-get+ (plist prop default) `(or (plist-get ,plist ,prop) ,default)))
    (vconcat (cl-loop for col in column-list
                      for cw across (or column-max-widths
                                        (make-string (length column-list) 0))
                      for spec = (let ((spec (assoc-string col gee-changelist-column-definitions)))
                                   (unless (stringp spec) (cdr spec)))
                      for name = (plist-get+ spec :label col)
                      collect (list name
                                    (max (plist-get+ spec :width 1) (length name) cw)
                                    nil ;sort
                                    :right-align (plist-get spec :right-align))))))

(defun gee-changelist-refresh ()
  "Refresh the content of the current changelist buffer."
  (when gee-changeinfo-refresh-function
    (setq gee-changeinfo (funcall gee-changeinfo-refresh-function)))
  (let ((colwidths (make-vector (length gee-changelist-columns) 0)))
    (setq tabulated-list-entries (gee-changeinfo-entries gee-changeinfo colwidths))
    (setq tabulated-list-format (gee--changelist-format gee-changelist-columns
                                                        colwidths))))

(defun gee--changelist-set-fake-header-face (&optional header-face &rest _)
  "Set HEADER-FACE as the face property of `tabulated-list--header-overlay'.
If HEADER-FACE is nil, use `gee-changelist-header-line'."
  ;; This private variable is the only way to find the overlay...
  (when (and (derived-mode-p 'gee-changelist-mode) tabulated-list--header-overlay)
	(overlay-put tabulated-list--header-overlay
                 'face (or header-face 'gee-changelist-header-line))))

(define-derived-mode gee-changelist-mode tabulated-list-mode "Gerrit changes"
  "The Gerrit changelist mode.
Used for query results and dashboards."
  (setq tabulated-list-use-header-line gee-changelist-use-header-line)
  (setq tabulated-list-padding 1)
  (gee-changelist-refresh)
  (unless (> (length gee-changeinfo) 0)
	(error "No changes found"))
  (add-hook 'tabulated-list-revert-hook #'gee-changelist-refresh nil t)
  (add-function :around (local 'tabulated-list-printer) #'gee-tablist-add-star-printer)
  (add-function :around (local 'tabulated-list-printer) #'gee-tablist-add-changeinfo-printer)
  (add-function :around (local 'tabulated-list-printer) #'gee-tablist-section-header-printer)
  (tabulated-list-init-header)
  (tabulated-list-print)
  (gee--changelist-set-fake-header-face)
  (add-function :after (local 'revert-buffer-function)
                #'gee--changelist-set-fake-header-face))
(put 'gee-changelist-mode 'mode-class 'special)

(defun gee-change-info-from-col-entry (entry)
    "Return the ChangeInfo object of the tabulated-list column ENTRY.
ENTRY should be an element from the column vector of one element
of `tabulated-list-entries'.  ENTRY may be a string or a (string
. plist) cons."
  (if (consp entry) ; entry is a cons of the string and some button properties
	  (setq entry (car entry)))
  (get-text-property 0 'change-info entry))

(defun gee-change-info-at-pos (&optional pos)
  "Return the ChangeInfo object of the change at POS in OBJECT.
If POS is not set, use `point'."
  (get-text-property (or pos (point)) 'change-info))

(defun gee-change-at-pos (&optional pos)
  "Return the `id' of the change at POS.
If POS is not set, use `point'."
  (gee-json-property 'id (gee-change-info-at-pos (or pos (point)))))

(defun gee-change-toggle-star (change-info)
  "Toggle the `starred' attribute of the change CHANGE-INFO."
  (let ((starred (gee-json-property 'starred change-info))
		(id (gee-json-property 'id change-info)))
	(if starred
		(gee-gerrit-rest-account-unstar id)
	  (gee-gerrit-rest-account-star id))))

(defun gee-change-toggle-star-at-pos (&optional pos)
  "Toggle the `starred' attribute of the change at POS."
  (interactive)
  (gee-change-toggle-star (gee-change-info-at-pos (or pos (point)))))

(defun gee-visit-change-at-pos (&optional pos)
  "Visit the change at POS (or `point' if POS is nil)."
  (interactive)
  (let ((id (gee-change-at-pos (or pos (point)))))
    (when id (gee-view-change id))))

(defun gee--changelist-get-mark (&optional pos)
  "Return the tabulated-list mark of the line at POS.
Use `point' if POS is nil.  Return the empty string for lines
that have no changeinfo."
  (save-excursion
    (when pos (goto-char pos))
    (if (not (gee-change-info-at-pos))
        ""
      (beginning-of-line)
      (make-string 1 (char-after)))))

(defun gee-changelist-mark (&optional _num)
  "Mark a change and move to the next line."
  (interactive "p")
  (if (gee-change-info-at-pos)
      (tabulated-list-put-tag "*" t)
    (forward-line)))

(defun gee-changelist-unmark (&optional _num)
  "Clear any mark on a change and move to the next line."
  (interactive "p")
  (tabulated-list-put-tag " " t))

(defun gee-changelist-marked-changes ()
  "Return a list of change-ids for all marked lines."
  (save-excursion
    (goto-char (point-min))
    (cl-loop while (= 0 (forward-line))
             if (string= "*" (gee--changelist-get-mark (1- (point))))
             collect (gee-change-at-pos (1- (point))))))

(defun gee-changelist-add-reviewer (reviewer)
  "Add a REVIEWER to any marked change or the change at point."
  ;; TODO Add completion
  (interactive "MAdd reviewer: ")
  (let ((marked (gee-changelist-marked-changes)))
    (unless marked
      (setq marked (gee-change-at-pos)))
    (unless marked
      (user-error "No change at point"))
    (dolist (change-id marked)
      (gee-gerrit-rest-change-add-reviewer change-id reviewer))))

(defmacro define-query (name &rest queries)
  "Define a function NAME that call `gee-changes-queries' with QUERIES as arguments."
  `(defun ,name ()
	 ,(format "Visit the gee change table for \"%s\"" (mapconcat 'identity queries "+"))
	 (interactive)
	 (gee-changes-query ,@queries)))

(define-query gee-goto-abandoned "is:abandoned")
(define-query gee-goto-merged "is:merged")
(define-query gee-goto-open "is:open")
(define-query gee-goto-drafts "has:draft")
(define-query gee-goto-watched "is:watched+is:open")
(define-query gee-goto-starred "is:starred")
(define-query gee-goto-draft-comments "has:draft")

;; gerrit changelist standard keys
(define-key gee-changelist-mode-map "j" #'next-line)
(define-key gee-changelist-mode-map "k" #'previous-line)
(define-key gee-changelist-mode-map (kbd "RET") #'gee-visit-change-at-pos)
(define-key gee-changelist-mode-map "R" #'revert-buffer)
(define-key gee-changelist-mode-map "o" #'gee-visit-change-at-pos)
(define-key gee-changelist-mode-map "s" #'gee-change-toggle-star-at-pos)
(define-prefix-command 'gee-goto-prefix-map nil "Go to ...")
(define-key gee-changelist-mode-map "g" 'gee-goto-prefix-map)
(define-key 'gee-goto-prefix-map "a" 'gee-goto-abandoned)
(define-key 'gee-goto-prefix-map "m" 'gee-goto-merged)
(define-key 'gee-goto-prefix-map "o" 'gee-goto-open)
(define-key 'gee-goto-prefix-map "c" 'gee-goto-draft-comments)
(define-key 'gee-goto-prefix-map "d" 'gee-goto-drafts)
(define-key 'gee-goto-prefix-map "i" 'gee-gerrit-dashboard-self)
(define-key 'gee-goto-prefix-map "s" 'gee-goto-starred)
(define-key 'gee-goto-prefix-map "w" 'gee-goto-watched)
;; gee changelist extra keys
(define-key gee-changelist-mode-map "m" #'gee-changelist-mark)
(define-key gee-changelist-mode-map "u" #'gee-changelist-unmark)
(define-key gee-changelist-mode-map "+" #'gee-changelist-add-reviewer)

;; Gerrit menu

(defun gee--docs (doc)
  "Return an unnamed function calling `gee-browse-gerrit-documentation' with DOC as argument."
  (lambda ()
	(interactive)
	(apply 'gee-browse-gerrit-documentation doc)))

(easy-menu-define gee-gerrit-menu gee-changelist-mode-map "Gerrit menu"
  `("Gerrit"
	("All"
	 ["Open"      gee-goto-open]
	 ["Merged"    gee-goto-merged]
	 ["Abandoned" gee-goto-abandoned])
	("My"
	 ;; all of these are actually defined in the user settings as URLs, but are enabled as defaults
	 ["Changes" gee-gerrit-dashboard-self]
	 ["Drafts" gee-goto-drafts]
	 ["Draft comments" gee-goto-draft-comments]
	 ["Watched changes" gee-goto-watched]
	 ["Starred changes" gee-goto-starred]
	 ["Groups" ignore :active nil :help "Not implemented"])
	("Projects"
	 ["List"				ignore :active nil :help "Not implemented"]
	 ["Create new project"	ignore :active nil :help "Not implemented"]
	 ["Import project"		ignore :active nil :help "Not implemented"]
	 ["List imports"		ignore :active nil :help "Not implemented"])
	("People"
	 ["List groups"			ignore :active nil :help "Not implemented"]
	 ["Create new group"	ignore :active nil :help "Not implemented"]
	 ["Import group"		ignore :active nil :help "Not implemented"])
	("Documentation"
	 ["Table of contents" gee-browse-gerrit-documentation]
	 ["Searching" ,(gee--docs "user-search.html")]
	 ["Uploading" ,(gee--docs "user-upload.html")]
	 ["Access controls" ,(gee--docs "access-control.html")]
	 ["REST API" ,(gee--docs "rest-api.html")]
	 ["Project owner guide" ,(gee--docs "intro-project-owner.html")])
	))


(provide 'gee)
;;; gee.el ends here

;; Local Variables:
;; tab-width: 4
;; indent-tabs-mode: nil
;; End:
