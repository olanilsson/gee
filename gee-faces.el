;;; gee-faces.el --- Faces used by gee               -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Ola Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords: faces

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Faces used by gee.

;;; Code:

(defgroup gee-faces nil "Faces used by gee - The Gerrit Emacs Experience."
  :prefix "gee"
  :group 'gee)

(defface gee-comment-button
  '((((class color)) :weight bold :foreground "white" :background "RoyalBlue2" :box "black")
	(((class grayscale)) :weight bold :foreground "white" :background "dim gray")
	(t :inverse-video t :weight bold))
  "Face for comment action buttons, typically `Reply', `Done', and `Fix'.")

(defface gee-comment-comment '((((class color)) :background "yellow")
                              (((class grayscale)) :background "gray")
                              (t :inverse-video t weight bold))
  "Face for comment text." :group 'gee-faces)

(defface gee-comment-discard-button
  '((((class color)) :weight bold :foreground "white" :background "red" :box "black")
	(((class grayscale)) :weight bold :foreground "white" :background "dim gray")
	(t :inverse-video t :weight bold))
  "Face for draft comment discard buttons.")

(defface gee-comment-draft
  '((((class color))  :foreground "white" :background "gray67")
	(((class grayscale))  :foreground "white" :background "gray67")
	(t :inverse-video t ))
  "Face for comment DRAFT tag.")

(defface gee-comment-range '((((class color)) :background "gold")
                              (((class grayscale)) :background "gray")
                              (t :inverse-video t weight bold))
  "Face for comment ranges." :group 'gee-faces)

(defface gee-review-section '((((class color)) :foreground "black" :background "gray" :weight bold)
                              (((class grayscale)) :foreground "black" :background "gray" :weight bold)
                              (t :inverse-video t :weight bold))
  "Face for review section headers." :group 'gee-faces)

(defface gee-review-link-face '((t :inherit link))
  "Face for buttons in gee review buffers."
  :group 'gee-faces)

(defface gee-reviewer '((((class color)) :foreground "black" :background "gray")
						(((class grayscale)) :foreground "black" :background "gray")
						(t :inverse-video t))
  "Face used for reviewers in `gee-review-mode'." :group 'gee-faces)

(defface gee-starred-face '((((class color)) :background "LightGoldenRod")
							(((class grayscale)) :background "gray80")
							(t :box t))
  "Face for starred reviews in dashboards." :group 'gee-faces)

(defface gee-dashboard-section '((((class color)) :foreground "black" :background "gray" :weight bold)
								 (((class grayscale)) :foreground "black" :background "gray" :weight bold)
								 (t :inverse-video t :weight bold))
  "Face for dashboard section headers." :group 'gee-faces)

(defface gee-changelist-header-line
  '((t :inherit gee-dashboard-section :slant italic))
  "Face for the changelist header line if `gee-changelist-use-header-line' is nil."
  :group 'gee-faces
  :group 'gee-changelist)

(defface gee-positive-label
  '((((class color)) :foreground "green")
	(((class grayscale)) :weight bold)
	(t :inverse-video t :weight bold))
  "Face for poitive label values." :group 'gee-faces)

(defface gee-negative-label
  '((((class color)) :foreground "red")
	(((class grayscale)) :slant italic)
	(t :inverse-video t :slant italic))
  "Face for negative label values." :group 'gee-faces)

(defface gee-review-bordered-button-face
  '((t :inherit custom-button))
  "Face for normal bordered buttons."
  :group 'gee-faces)

(defface gee-review-bordered-button-mouse-face
  '((t :inherit custom-button-mouse))
  "Mouse face for normal bordered buttons."
  :group 'gee-faces)

(provide 'gee-faces)
;;; gee-faces.el ends here
