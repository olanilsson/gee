;;; gee-comment-mode.el --- Show and edit gerrit comments  -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Ola Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords: gerrit

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'cl-lib)

(require 'seq)

(require 'gee-faces)
(require 'gee-gerrit-rest)
(require 'gee-util)

(defvar-local gee-comment-change-id nil)
(defvar-local gee-comment-revision-id "current")
(defvar-local gee-comment-path nil)
(defvar-local gee-comment-comments nil)
(defvar-local gee-comment-drafts nil)

(define-minor-mode gee-comment-mode
  "Minor mode to view and manage Gerrit code review comments for a file.
FIX: Requires `gee-change-info' and `gee-revision' variables to be set.

If `gee-comment-mode' is called interactively, ARG is an
optional (prefix) argument.  No prefix argument toggles the mode.
A prefix argument enables the mode if the argument is positive,
and disables it otherwise.

When called from Lisp, `gee-comment-mode' toggles the mode if ARG
is `toggle', disables the mode if ARG is a non-positive integer,
and enables the mode otherwise (including if ARG is omitted or
nil or a positive integer)."
  :init-value nil :lighter "" :keymap nil
  (if gee-comment-mode
      (unless (and gee-comment-change-id
                   gee-comment-revision-id)
        (gee-comment-mode -1)
        (user-error "Enable `gee-comment-mode' by calling `gee-show-comments'"))
    (gee-comment-remove-overlays)))

(defun gee-comment-line-bounds (line)
  "Return the beginning and end positions of LINE as a list `(beg end)'."
  (save-excursion
    (save-restriction
      (goto-char (point-min))
      (unless (and (zerop (forward-line (1- line)))
                   (bolp))
        (error "LINE out of bounds"))
      (list (point) (line-end-position)))))

(defun gee-comment-range-bounds (comment-range)
  "Return the beginning and end positions of COMMENT-RANGE as a list `(beg end)'."
  (save-excursion
    (save-restriction
      (goto-char (point-min))
      (let (beg end)
        (let-alist comment-range
          (unless (= 0 (forward-line (1- .start_line)))
            (error "COMMENT-RANGE start line out of bounds"))
          (setq beg (+ (point) (1- .start_character)))
          (unless (<= beg (line-end-position))
            (error "COMMENT-RANGE start character past end of line"))
          (unless (= 0 (forward-line (- .end_line .start_line)))
            (error "COMMENT-RANGE end line out of bounds"))
          (setq end (+ (point) (1- .end_character)))
          (unless (<= end (line-end-position))
            (error "COMMENT-RANGE end character past end of line"))
          (when (< end beg)
            (error "COMMENT-RANGE end before start")))
        (list beg end)))))

(defun gee-comment-make-range (begin end)
  "Create a CommentRange of the regiion (BEGIN END)."
  (save-excursion
    (save-restriction
      (widen)
      (cl-labels ((column-number-at-pos (pos)
                                        ;; assume save-excursion
                                        (goto-char pos)
                                        (1+ (current-column))))
        `((start_line . ,(line-number-at-pos begin))
          (start_character . ,(column-number-at-pos begin))
          (end_line . ,(line-number-at-pos end))
          (end_character . ,(column-number-at-pos end)))))))

(defun gee-comment-info-bounds (comment-info)
  "Return a list of point `(beg end)' for COMMENT-INFO."
  (let-alist comment-info
    (if .range
        (gee-comment-range-bounds .range)
      (gee-comment-line-bounds .line))))

(defun gee-comment-remove-overlays ()
  "Remove all `gee-comment' overlays from the current buffer."
  (save-restriction
    (cl-loop for ov being the overlays
             if (cl-member (overlay-get ov 'category)
                           '(gee-comment-bounds gee-comment-display gee-comment-range))
             do (delete-overlay ov))))

(defun gee-comment-make-range-overlay (comment-info)
  "Return a new overlay for the range specified in COMMENT-INFO."
  (when (assoc 'range comment-info)
    (let ((ov (apply #'make-overlay (gee-comment-info-bounds comment-info))))
      (overlay-put ov 'comment-info comment-info)
      (overlay-put ov 'category 'gee-comment-range)
      (overlay-put ov 'face 'gee-comment-range)
      ov)))

(define-button-type 'gee-comment-button
  :supertype 'gee-button
  'face 'gee-comment-button
  'mouse-face 'gee-comment-button)

(defun gee-make-comment-button (label comment-info action)
  "Make a button LABEL for COMMENT-INFO comment, with specified ACTION."
  (propertize
   (gee-make-button label 'gee-comment-button
                    'comment-info comment-info
                    'action action)
   'face 'gee-comment-button
   ;; 'keymap (let ((map (make-sparse-keymap)))
   ;;           (define-key map [mouse-2] #'gee-comment-button-action))
))

(defun gee-comment-create-reply (change-id comment-info message &optional revision-id)
  "Create a draft comment reply for the specified CHANGE-ID.
The created drafte comment will be a reply to the comment
specified by COMMENT-INFO, and use MESSAGE as text.  The comment
will be creteted on the specified REVISION-ID.  REVISION-ID nil
is interpreted as \"current\"."
  (gee-gerrit-rest-create-draft
   change-id
   (list
    (assoc-string 'path comment-info)
    (assoc-string 'line comment-info)
    (or (assoc-string 'range comment-info) '(range))
    `(in_reply_to . ,(cdr (assoc-string 'id comment-info)))
    `(message . ,message))
     revision-id))

(defun gee-comment-create-comment (&optional begin end)
  (interactive "r")
  (let ((message (read-string "Comment: ")))
    (gee-gerrit-rest-create-draft
     gee-comment-change-id
     `((path . ,gee-comment-path)
       ,(if (use-region-p)
            (cons 'range (gee-comment-make-range begin end))
          (cons 'line (line-number-at-pos (point))))
       (message . ,message)) gee-comment-revision-id)
  ))

(defun gee-comment-button-reply (button)
  "Reply to the comment associated with BUTTON."
  (interactive)
  (gee-comment-create-reply gee-comment-change-id
                            (button-get button 'comment-info)
                            (read-string "Message: ")
                            gee-comment-revision-id))

(defun gee-comment-button-done (button)
  "Reply \"Done\" to the comment tied to BUTTON."
  (interactive)
  (gee-comment-create-reply gee-comment-change-id
                            (button-get button 'comment-info)
                            "Done" gee-comment-revision-id))

(defun gee-comment-button-fix (button)
  "Start a Fix for the comment tied to BUTTON."
  (interactive)
  (ignore button)
  (user-error "Fix is not implemented"))

(defun gee-comment-button-edit (button)
  "Edit the draft comment tied to BUTTON."
  (interactive)
  (let* ((comment-info (button-get button 'comment-info))
         (comment-input (cl-remove-if-not
                         (lambda (key) (member key
                                               '(id path line range in_reply_to)))
                         comment-info :key #'car)))
    (let-alist comment-info
      (push (cons 'message (read-string "Update message: " .message)) comment-input)
      (gee-gerrit-rest-update-draft gee-comment-change-id
                                    .id
                                    comment-input
                                    gee-comment-revision-id))))

(defun gee-comment-button-discard (button)
  "Discard the draft comment tied to BUTTON."
  (interactive)
  (gee-gerrit-rest-delete-draft
   gee-comment-change-id
   (cdr (assoc-string 'id (button-get button 'comment-info)))
   gee-comment-revision-id))

(defun gee-comment-format-full-message (comment-info &optional win-width)
  "Format the expanded text for COMMENT-INFO.
If WIN-WIDTH is a number, assume the window displaying the
overlay is that many characters wide, otherwise call
`window-body-width' for the current window."
  (setq win-width (or win-width (window-body-width)))
  (let-alist comment-info
    (let ((time-str (gee-timestamp-to-display-time .updated)))
      (concat
       (if .draft
           (propertize "DRAFT" 'face 'gee-comment-draft)
         (propertize .author.name 'face 'bold))
       (gee-aligned-space (- win-width (length time-str)))
       time-str
       "\n" .message "\n"
       (if .draft
           (concat
            (gee-make-comment-button "Edit" comment-info #'gee-comment-button-edit) " "
            ;;(propertize "Cancel" 'face 'gee-comment-button)
            (gee-aligned-space 30)
            (propertize (gee-make-comment-button "Discard" comment-info #'gee-comment-button-discard)
                        'face 'gee-comment-discard-button)
            )
         (concat
          (gee-make-comment-button "Reply" comment-info #'gee-comment-button-reply) " "
          (gee-make-comment-button "Done" comment-info #'gee-comment-button-done) " "
          (gee-make-comment-button "Fix" comment-info #'gee-comment-button-fix)))
       (gee-aligned-space win-width)
       ))))

(defun gee-comment-format-collapsed-message (comment-info &optional win-width)
  "Format the collapsed text for COMMENT-INFO.
WIN-WIDTH can optionally be set to the window width in character
the formatted message should be displayed in."
  (setq win-width (or win-width (window-body-width)))
  (let-alist comment-info
    (let* ((time-str (gee-timestamp-to-display-time .updated))
           (time-len (length time-str))
           (message (replace-regexp-in-string "\n" " " .message))
           (message-len (length message))
           (header (if .draft
                       (propertize "DRAFT" 'face 'gee-comment-draft)
                     .author.name))
           (max-message-len (- win-width time-len 25 2)))
      (propertize
       (concat header
               (gee-aligned-space 25)
               (if (< max-message-len message-len)
                   (substring message 0 max-message-len)
                 message)
               (gee-aligned-space (- win-width time-len))
               time-str)
       'help-echo .message)
       )))

(defun gee-comment-format (comment-overlay &optional win-width format-function)
  "Format the text for COMMENT-OVERLAY's `after-string' property.
WIN-WIDTH is passed to the formatting function.
The formatting is done by the function specified by the
`format-function' property of COMMENT-OVERLAY.  If
FORMAT-FUNCTION is set, it is used instead of the
`format-function' property."
  (let* ((comment-info (overlay-get comment-overlay 'comment-info))
         (text (funcall (or format-function
                            (car-safe (overlay-get comment-overlay 'format-function-ring))
                            ;;(lambda (&rest ignore) "format function broken")
                            #'ignore)
                        comment-info win-width))
         (map (make-sparse-keymap))
         (toggle (lambda () (interactive) (gee-comment-toggle comment-overlay))))
    (when text
      ;; trigger toggle on mouse-2
      (define-key map [mouse-2] toggle)
      ;; Use the mouse-1-click-follows-link function to convert mouse-1
      ;; to mouse-2
      (define-key map [follow-link] [mouse-2])
      (add-face-text-property 0 (length text)
                              'gee-comment-comment t text)
      (let-alist comment-info
        (add-text-properties 0 (or (cl-position ?\n text) (length text))
                             `(pointer hand keymap ,map)
                             text)
        (concat (if .line "\n" "") text)))))

(defun gee-comment-refresh-overlays (buffer window)
  "Refresh `gee-comment-display' overlays for BUFFER in WINDOW."
  (unless (member window (get-buffer-window-list buffer nil t))
    (error "%s not displayed in %s" buffer window))
  (unless (window-live-p window)
    (error "%s is not a live window" window))
  (cl-loop for ov being the overlays of buffer
           with win-width = (window-body-width window)
           if (eq (overlay-get ov 'category)
                  'gee-comment-display)
           do (overlay-put ov
                           (if (overlay-get ov 'after-string)
                               'after-string 'before-string)
                           (gee-comment-format ov win-width))))

(defun gee-comment-size-changed (frame)
  "Update `gee-comment-display' overlays in the specified FRAME.
This function is meant to be added to
`window-size-change-functions'."
  (dolist (window (window-list frame))
    (when (window-live-p window)
      (gee-comment-refresh-overlays (window-buffer window) window))))

(add-hook 'window-size-change-functions #'gee-comment-size-changed)

(defun gee-comment-winchange ()
  "Update `gee-comment-display' overlays for the current buffer and window.
Suitable for `window-configuration-change-hook'."
  (gee-comment-refresh-overlays (current-buffer) (selected-window)))

(defun gee-comment-toggle (comment-overlay)
  "Toggle the `after-string' property of COMMENT-OVERLAY.
The switch is done by rotating the `format-function-ring'
property of COMMENT-OVERLAY, and then setting `after-string' or
`before-string' to the text formatted by the new function."
  (overlay-put comment-overlay 'format-function-ring
               (cdr-safe (overlay-get comment-overlay 'format-function-ring)))
  (overlay-put comment-overlay 'before-string nil)
  (overlay-put comment-overlay 'after-string nil)
  (let-alist (overlay-get comment-overlay 'comment-info)
    (overlay-put comment-overlay (if .line 'after-string 'before-string)
                 (gee-comment-format comment-overlay))))

(defun gee-make-ring (&rest elements)
  "Create a circular list of ELEMENTS."
  (when elements
    (let ((r (cl-copy-list elements)))
      (setcdr (last r) r)
      r)))

(defun gee-comment-make-comment-overlay (comment-info &rest prop-list)
  "Create a `gee-comment-display' overlay for COMMENT-INFO.
Any property pairs in PROP-LIST are added to the overlay.  If
COMMENT-INFO has a `range' property, an extra overlay with
category `gee-comment-range' will be created."
  (let-alist comment-info
    (let* ((line-bounds (gee-comment-line-bounds (or .line 1)))
           (ov (apply #'make-overlay line-bounds)))
      (overlay-put ov 'comment-info comment-info)
      (overlay-put ov 'category 'gee-comment-display)
      (overlay-put ov 'range-overlay (gee-comment-make-range-overlay comment-info))
      (overlay-put ov 'format-function-ring
                   (gee-make-ring #'gee-comment-format-collapsed-message
                                  #'gee-comment-format-full-message))
      (overlay-put ov (if .line 'after-string 'before-string) (gee-comment-format ov))
      (let (prop val)
        (while (and prop-list (cdr prop-list)
                    (symbolp (car prop-list)))
          (setq prop (pop prop-list))
          (setq val (pop prop-list))
          (overlay-put ov prop val))))))

(defun gee-comment-info-before-p (before after)
  "Return t if the `updated' member of BEFORE is earlier than AFTER.
Also return t if the time strings are equal."
  (let ((before-time (cdr (assoc 'updated before)))
        (after-time (cdr (assoc 'updated after))))
    (or (string< before-time after-time)
        (string= before-time after-time))))

(defun gee-comment-process-comments (comment-list draft-list)
  "Add coment overlays for comments in COMMENT-LIST and drafts in DRAFT-LIST.
Both COMMENT-LIST and DRAFT-LIST should be lists of `CommentInfo' objects."
  (let* (;; create a map of comment ids to a list of messages that are
         ;; replies to that comment
         (reply-to-map
          (seq-group-by (lambda (e) (cdr (assoc 'in_reply_to e)))
                        (append
                         ;; mark comment-infos in draft-list with a 'draft property set to t
                         (mapc (lambda (draft-info)
                                        (setcdr draft-info (cons (cons 'draft t) (cdr draft-info))))
                                      draft-list)
                         comment-list)))
         ;; Setting the priority property of the overlay decreasingly
         ;; ensures that the overlays are displayed in the correct
         ;; order.
         (prio (* 2 (length reply-to-map)))
         stack)
    ;; intialize the stack by pushing all non-reply comments
    ;; (identified by having no in_reply_to property) to the stack
    ;; with the oldest on top.
    (setq stack (sort (cdr (assoc nil reply-to-map))
                      #'gee-comment-info-before-p))
    (while stack
      ;; pop the first comment from the stack
      (let* ((current-comment (pop stack))
             ;; identify the replies to 'current-comment
             (replies (sort (cdr (assoc (cdr (assoc 'id current-comment)) reply-to-map))
                            #'gee-comment-info-before-p)))
        ;; create an overlay for 'current-comment
        (let-alist current-comment
          (unless (string= .side "PARENT")
            (gee-comment-make-comment-overlay current-comment 'priority (cl-decf prio))))
        ;; push the replies to the stack with the oldest on top
        (setq stack (append replies stack))))
    ))

(defsubst gee-comment--prepare-comments (path-to-comment-info-alist path)
  "From PATH-TO-COMMENT-INFO-ALIST, exract and modify comments for PATH.
PATH-TO-COMMENT-INFO shall be an alist mapping paths to vectors
of comments.  Pick the comment-vector for PATH, convert it to an
alist and add the PATH as an alist property of each CommentInfo
object."
  (mapcar (lambda (comment-info) (cons `(path . ,path) comment-info))
          (seq-into (cdr (assoc-string path path-to-comment-info-alist)) 'list)))

(defun gee-show-comments (path change-id revision-id)
  "Fetch and show comments for PATH in CHANGE-ID and REVISION-ID.
Assumes the content of the current buffer matches that of the
specified PATH, CHANGE-ID, and REVISION-ID."
  (setq-local gee-comment-change-id change-id)
  (setq-local gee-comment-revision-id revision-id)
  (setq-local gee-comment-path path)
  (setq-local gee-comment-comments
              (gee-comment--prepare-comments
               (gee-gerrit-rest-list-comments change-id revision-id) path))
  (setq-local gee-comment-drafts
              (gee-comment--prepare-comments
               (gee-gerrit-rest-list-drafts change-id revision-id) path))
  (gee-comment-remove-overlays)
  (gee-comment-process-comments gee-comment-comments gee-comment-drafts)
  (add-hook 'window-configuration-change-hook #'gee-comment-winchange nil t)
  (gee-comment-mode))

(provide 'gee-comment-mode)
;;; gee-comment-mode.el ends here
