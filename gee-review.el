;;; gee-review.el ---                                -*- lexical-binding: t; coding: utf-8;-*-

;; Copyright (C) 2016  Ola Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Maintainer: Ola Nilsson <ola.nilsson@gmail.com>
;; URL: https://bitbucket.org/olanilsson/gee
;; Keywords: gerrit

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'let-alist)
(require 'anaphora)

(require 'gee)
(require 'gee-ediff)
(require 'gee-faces)
(require 'gee-gerrit-rest)
(require 'gee-util)

(declare-function gee-reply-form "gee-reply" (change-info &optional revision-id))

(defvar-local gee-review-change-info nil
  "The ChangeInfo object for the shown change.")
(put 'gee-review-change-info 'permanent-local t)

(defvar-local gee-review-active-revision nil
  "The curently displayed review.
Should always be a SHA1 that is listed in the `revisions'
dictionary of `gee-review-change-info'.")
(put 'gee-review-active-revision 'permanent-local t)

(defvar-local gee-review-base-revision 0
  "The revision to diff against.  0 means the base revision.")
(put 'gee-review-base-revision 'permanent-local t)

(defun gee-change-header-state (change-info &optional revision-id)
  "Return the readable state string for the header for CHANGE-INFO.
REVISION-ID may be set to specify some other review than current."
  (setq revision-id (gee-canonical-revision-id change-info revision-id))
  (let-alist change-info
    (ignore .approved .optional .rejected) ; used by inner let-alist
    (cond ((not (string= revision-id .current_revision)) "Not Current")
          ((string= .status "DRAFT") "Draft")
          ((string= .status "MERGED") "Merged")
          ((string= .status "ABANDONED") "Abandoned")
          ((string= .status "SUBMITTED") "Submitted, merge pending")
          ((cl-every (lambda (lli)
                       (let-alist (cdr lli)
                         (and (not .rejected) (or .optional .approved)))) .labels) "Ready to submit")
          ((cl-loop for (label . label-info) in .labels
                    unless (cdr (assoc 'optional label-info))
                    if (assoc 'rejected label-info) return (format "Not %s" (symbol-name label))
                    else if (not (assoc 'approved label-info)) return (format "Needs %s" (symbol-name label))))
          (t ""))))

(defun gee-review-button (button)
  "Button action that call `gee-view-change' using BUTTON properties.
The `change-id', `change-review', and `change-diff-base'
properties of BUTTON are used as arguments to `gee-view-change'."
  (gee-view-change (get-text-property button 'change-id)
                   (get-text-property button 'change-review)
                   (get-text-property button 'change-diff-base)))

;; button type for change views
(define-button-type 'gee-review-link
  :supertype 'gee-button
  'action #'gee-review-button
  'face 'gee-review-link-face
  'change-id nil
  'change-review nil
  )

(define-button-type 'gee-review-button
  :supertype 'gee-bordered-button
  'action #'gee-review-button
  'change-id nil
  'change-review nil
  )

(defun gee-make-review-button (label change-id &optional revision-id diff-base)
  "Make a button LABEL that will call `gee-view-change' for CHANGE-ID.
If REVISION-ID is set it is passed to `gee-view-change' as well,
and DIFF-BASE will pass the file diff revision."
  (gee-make-button label 'gee-review-button
                   'change-id change-id
                   'change-review revision-id
                   'change-diff-base (or diff-base 0)))

(defun gee-make-review-link (label change-id &optional revision-id)
  "Make a link LABEL that will call `gee-view-change' for CHANGE-ID.
If REVISION-ID is set it is passed to `gee-view-change' as well."
  (gee-make-button label'gee-review-link
                   'change-id change-id
                   'change-review revision-id))

(defun gee-review-form (button)
  "Call `gee-reply-form' with arguments taken from BUTTONs buffer."
  (with-current-buffer (marker-buffer button)
    (gee-reply-form gee-review-change-info gee-review-active-revision)))

(define-button-type 'gee-review-reply-button
  :supertype 'gee-bordered-button
  'action 'gee-review-form)

(defun gee-make-review-reply-button ()
  "Make a `Reply' button."
  (gee-make-button "Reply..." 'gee-review-reply-button))

(defun gee-review-git-person (git-person-info)
  "Return a string \"fullname <email>\" from GIT-PERSON-INFO object."
  (let ((email (gee-json-property 'email git-person-info)))
	(gee-text-query-button (concat (gee-json-property 'name git-person-info)
								   " <" email ">")
						   (format "owner:%s+status:open" email))))

(defun gee-review-git-date (git-person-info)
  "Return a formatted date string from GIT-PERSON-INFO."
  (let* ((date (gee-json-property 'date git-person-info))
		 (tz (gee-json-property 'tz git-person-info))
		 (time (gee-git-date-to-time date tz)))
	(format-time-string "%Y-%m-%d %H:%M" time)))

(defun gee-reviewer-tag (account-info)
  "Return a propertized name string from ACCOUNT-INFO.
The full user name is propertized with the face `gee-reviewer'."
  (let ((name (gee-account-property 'name account-info)))
	(propertize name 'face 'gee-reviewer)))

(defun gee--mergeable (change-info &optional revision-id)
  "Get the mergeable status for CHANGE-INFO.
Return information for the `current_review' in CHANGE-INFO unless
another REVISION-ID is specified.  CHANGE-INFO does not hold the
information, so a separate request to the changes/mergeable API
is needed.  The returned string is suitable for the `Strategy:'
field of the change view."
  ;; This function is not as good as it should be. It will not create
  ;; the correct string in edge cases and probably cause errors in
  ;; some.
  (setq revision-id (gee-canonical-revision-id change-info revision-id))
  (let* ((change-id (cdr (assoc 'id change-info)))
         (mergeable-info (gee-gerrit-rest-change-revision-get-mergeable
                          change-id revision-id)))
    (and mergeable-info (listp mergeable-info)
         (concat (capitalize
                  (replace-regexp-in-string "_" " "
                                            (or (gee-json-property 'submit_type
                                                                   mergeable-info) "")))
                 (propertize (if (gee-json-property 'mergeable mergeable-info)
                                 "" " Cannot merge")
                             'face '(:foreground "red"))))))

(defun gee-review-insert-header (&rest arguments)
  "Insert the ARGUMENTS at point using `insert'.
The inserted text will have the `gee-review-section' face
appended to the face text property.  Any buttons inserted will
have any `face' property from their `category' symbol added to
their pure `face' property as well."
  (when arguments
    (let ((begin (point)) end)
      (apply #'insert arguments)
      (setq end (point))
      (let ((button (next-button begin t)))
        (while (and button (< button end))
          (let ((face (button-get button 'face))
                (bend (min end (button-end button))))
            (add-face-text-property (button-start button) bend face)
            (setq button (when (< bend end)
                           (next-button bend t))))))
      (add-face-text-property begin end 'gee-review-section t))))

(defun gee-review-select-patchset (button)
  "Ask the user for a patchset number and display it.
BUTTON has to have properties `change-id' set to a value
accepted by `gee-view-change', and `maxrev' set to the highest
available patchset number."
  (let ((change-id (get-text-property button 'change-id))
        (maxrev (get-text-property button 'maxrev)))
  (gee-view-change
   change-id
   (string-to-number
    (completing-read (format "View patchset (1..%d): " maxrev)
                     (mapcar #'number-to-string (number-sequence 1 maxrev)) nil t nil nil
                     (number-to-string maxrev))))))

(defun gee-review-patchset-buttons-string (change-info revision-id)
  "Return a string with patchset selection buttons.
The buttons returned will select a patchset from CHANGE-INFO.
The string is formatted \"< Patch set a/b >\" where the `<' and
`>' are separate buttons that select the patchsets preceeding and
following REVISION-ID, respectively.  The rest of the text is one
button which when pressed will ask the user for a patchset number
to switch to."
  (let ((revnum (gee-patchset-number change-info revision-id))
        (maxrev (gee-max-patchset-number change-info)))
    (let-alist change-info
      (concat
       (if (> revnum 1) (gee-make-review-button "<" .id (1- revnum)) " ") " "
       (gee-make-button (format "Patch set %d/%d" revnum maxrev)
                        'gee-bordered-button 'change-id .id 'maxrev maxrev
                        'action #'gee-review-select-patchset) " "
       (if (< revnum maxrev) (gee-make-review-button ">" .id (1+ revnum)) " ")))))

(defun gee-review-select-diff-base (button)
  "BUTTON."
  (let ((change-id (get-text-property button 'change-id))
        (maxrev (get-text-property button 'maxrev))
        (revision-id (get-text-property button 'revision-id))
        (diff-base (get-text-property button 'diff-base)))
  (gee-view-change
   change-id revision-id
   (string-to-number
    (completing-read (format "Diff against: (0..%d) [%d]: " (1- maxrev) diff-base)
                     (mapcar #'number-to-string (number-sequence 0 (1- maxrev))) nil t nil nil
                     (number-to-string diff-base))))))

(defun gee-review-diffbase-buttons-string (change-info revision-id diff-base)
  "CHANGE-INFO REVISION-ID DIFF-BASE."
  (let ((revnum (gee-patchset-number change-info revision-id))
        (maxrev (gee-max-patchset-number change-info)))
    (let-alist change-info
      (concat
       (if (> diff-base 0) (gee-make-review-button "<" .id revnum (1- diff-base)) " ") " "
       (gee-make-button (if (zerop diff-base) "Base"
                          (format "%d: %s" diff-base
                                  (substring (gee-canonical-revision-id change-info diff-base) 0 7)))
                        'gee-bordered-button 'change-id .id 'maxrev revnum
                        'revision-id revision-id
                        'diff-base diff-base
                        'action #'gee-review-select-diff-base) " "
       (if (< diff-base (1- maxrev)) (gee-make-review-button ">" .id revnum (1+ diff-base)) " ")))))

(defun gee-review-filelist-printer (id cols)
  "Perform final formatting of file-info lines.
ID is a Lisp object identifying the entry to print, and COLS is a
vector of column descriptors.  Call `tabulated-list-print-entry'
with another vector with reformatted values.  Also apply bold
face to the header and summary lines."
  (let ((beg (point))
        (newcols (copy-sequence cols))
        (titles [nil nil "Drafts:" "Comments:"])
        (comment-widths `[nil nil ,(cadr (aref tabulated-list-format 2))
                              ,(cadr (aref tabulated-list-format 3))])
        (path (aref cols 1))) ; may be string or (string . button-props)
    (when (string= "/COMMIT_MSG" (if (consp path) (car path) path))
      (aset newcols 1 (if (consp path) (cons "Commit Message" (cdr path)) "Commit Message"))
      (aset newcols 4 "")) ; empty size col for commit message
    (when (numberp (aref cols 2))
      (dolist (i '(2 3))
        (let ((val (aref cols i))
              (title (aref titles i))
              (width (aref comment-widths i)))
          (aset newcols i (if (zerop val) ""
                            (setq val (number-to-string val))
                            (concat title
                                    (make-string (- width (length title) (length val)) ?\s)
                                    val))))))
    (tabulated-list-print-entry id newcols)
    (add-text-properties beg (point) `(file-info id))
    (when (symbolp id)
      (add-face-text-property beg (point) '(:weight bold)))))

(defun gee-review-file-info-diff (change-id base-id revision-id path &optional binary)
  "Return a FileInfo alist representing a intra-revision diff for a file.
CHANGE-ID identifies the change.  BASE-ID is the id of the
revision to `Diff against'.  REVISION-ID is the id of the target
revision.  PATH is the filename as returned by the `List Files'
REST API.  If BINARY is non-nil, set the `binary' flag in the
returned FileInfo alist."
  (let ((diff-info (gee-gerrit-rest-get-diff change-id revision-id path base-id))
        file-info)
    (let-alist diff-info
      (unless (string= "MODIFIED" .change_type)
        (let ((status (substring .change_type 0 1)))
          (when (and (string= status "R") (string= .change_type "REWRITTEN"))
            (setq status "W"))
          (push `(status . ,status) file-info)))
      (when binary
        (push '(binary . t) file-info))
      (when (or (string= "RENAMED" .change_type) (string= "COPIED" .change_type))
        (push (cons 'old_path .meta_a.name) file-info))
      (cl-loop for diff-content across .content
               if (assq 'a diff-content) sum (length (cdr it)) into deleted-lines
               if (assq 'b diff-content) sum (length (cdr it)) into inserted-lines
               finally
               (unless (zerop inserted-lines)
                 (push `(lines_inserted . ,inserted-lines) file-info))
               (unless (zerop deleted-lines)
                 (push `(lines_deleted . ,deleted-lines) file-info)))
    (nreverse file-info)
  )))

(defun gee-review-list-files (change-info revision-id &optional diff-base)
  "Return a path=>FileInfo alist for CHANGE-INFO revision REVISION-ID.
DIFF-BASE can be an earlier revision-id of the change."
  (setq revision-id (gee-canonical-revision-id change-info revision-id))
  (let-alist change-info
    (if (or (zerop diff-base) (null diff-base))
        ;; diff against base is easy, just request the files for the
        ;; revision. The returned alist needs to be sorted on the path
        ;; (car) and make a proper cons for the COMMIT_MSG entry.
        (cl-substitute '(/COMMIT_MSG . nil) '/COMMIT_MSG
                       (cl-sort (gee-gerrit-rest-list-files .id revision-id) #'string< :key #'car)
                       :key #'car)
      ;; calculate the diff
      (setq diff-base (gee-canonical-revision-id change-info diff-base))
      ;; request the filesets for the two revisions
      (let* ((base-files (gee-gerrit-rest-list-files .id diff-base))
             (rev-files  (gee-gerrit-rest-list-files .id revision-id))
             (base-paths (mapcar 'car base-files))
             (rev-paths  (mapcar 'car rev-files))
             file-list) ;collect results here
        ;; in base, ignore all files already listed in rev
        (setq base-paths (cl-set-difference base-paths rev-paths))
        (dolist (path rev-paths) ; first check all files listed in the later rev
          (if (equal path '/COMMIT_MSG)
              (push (cons path nil) file-list)
            (let ((file-info (gee-review-file-info-diff .id diff-base revision-id path
                                                        (assq 'binary (cdr (assq path rev-files))))))
              (when (or (assq 'status file-info) ; false for MODIFIED files
                        (assq 'lines_inserted file-info)
                        (assq 'lines_deleted file-info))
                ;; push all except MODIFIED files with no line changes
                (push (cons path file-info) file-list))
              ;; we do not want to handle the file again, so remove it
              ;; from the list of files in the earlier revision.
              (if (equal (cdr (assq 'status file-info)) "R")
                  ;; ... except if it was renamed, then we have to remove the old name from the earlier revision.
                  (setq base-paths (cl-remove (cdr (assq 'old_path file-info)) base-paths :test #'string=))
                (setq base-paths (cl-remove path base-paths))))))
        (dolist (path base-paths) ;; handle files present only in the earlier revision, deleted files
          (unless (equal (cdr (assq 'status (cdr (assq path base-files)))) "D")
            ;; except if they have deleted state in the earlier revisison,
            (let ((file-info (gee-review-file-info-diff .id diff-base revision-id path
                                                        (assq 'binary (cdr (assq path base-files))))))
              (push (cons path file-info) file-list))))
        (cl-sort file-list #'string< :key #'car))))) ;; sort the resulting list on path

(defcustom gee-file-diff-function #'gee-ediff
  "The function used to view file diffs.
Should take four arguments; file-path, change-id, revision-id,
and base-revision."
  :type 'function
  :group 'gee)

(defun gee-review-diff-file-at-pos (&optional pos)
  "Visit the change at POS (or `point' if POS is nil)."
  (interactive)
  (let ((file-info (get-text-property pos 'tabulated-list-id)))
    (funcall gee-file-diff-function gee-review-change-info
             (cdr (assq 'path file-info))
             gee-review-active-revision gee-review-base-revision)))

(defun gee-review-insert-file-section (change-info &optional revision-id)
  "Insert file information for CHANGE-INFO for REVISION-ID.
REVISION-ID is resolved using `gee-canonical-revision-id', and
nil means the current revision."
  (setq revision-id (gee-canonical-revision-id change-info revision-id))
  (gee-review-insert-header
   "Files        | Diff against: "
   (gee-review-diffbase-buttons-string change-info revision-id gee-review-base-revision)
   "\n")
  (let* ((revisions (gee-json-property 'revisions change-info))
         (revision-info (gee-json-property revision-id revisions))
         (drafts (and (cdr (assoc 'has_draft_comments revision-info))
                      (gee-gerrit-rest-list-drafts (cdr (assoc 'id change-info))
                                                   revision-id)))
         (comments (gee-gerrit-rest-list-comments (cdr (assoc 'id change-info))
                                                  revision-id))
         (files (gee-review-list-files change-info revision-id gee-review-base-revision)))
    (cl-loop for (path . file-info) in files
             ;; values used more than once deserve their own variables
             for status = (let-alist file-info (or .status " "))
             for deleted = (let-alist file-info (or .lines_deleted 0))
             for inserted = (let-alist file-info (or .lines_inserted 0))
             for path-name = (symbol-name path)
             for n-drafts = (length (cdr (assoc path drafts)))
             for n-comments = (length (cdr (assoc path comments)))
             ;; summarize the total number of added and deleted lines,
             ;; used in the summary line
             sum deleted into total-deleted
             sum inserted into total-inserted
             ;; Collect some maximum values for proper table alignment
             maximize (length path-name) into max-file-name-length
             maximize n-drafts into most-drafts
             maximize n-comments into most-comments
             ;; collect the table lines
             collect `(,(append file-info (list (cons 'path path)))
                       [,status ,(cons path-name (list 'action 'gee-review-diff-file-at-pos)) ,n-drafts ,n-comments
                                ,(number-to-string (+ deleted inserted))]) into lines
             ;; add header and summary line
             finally (setq lines
                           (append `((header ["" "File Path"
                                              ,(if (zerop most-drafts) "" "Comments")
                                              ,(if (zerop most-drafts) "Comments" "")
                                              "Size"]))
                                   lines
                                   `((summary ["" "" 0 0 ,(format "+%d,-%d" total-inserted total-deleted)]))))
             (let ((tabulated-list-format `[("Status" 1)
                                            ("File Path" ,(max 14 ; length of "Commit Message"
                                                               max-file-name-length) nil :pad-right 4)
                                            ("Drafts" ,(if (zerop most-drafts) 0
                                                         (length (format "Drafts: %d" most-drafts))))
                                            ("Comments" ,(if (zerop most-comments)
                                                             (if (zerop most-drafts) (length "Comments") 0)
                                                           (length (format "Comments: %d" most-comments))))
                                            ("Size" ,(length (format "+%d/-%d" total-inserted total-deleted)) nil :right-align t)]))
               (mapc (apply-partially #'apply #'gee-review-filelist-printer) lines)))))

(defun gee-review-insert-commit-info (change-info &optional revision-id)
  "Insert commit information for CHANGE-INFO, REVISION-ID."
  (setq revision-id (gee-canonical-revision-id change-info revision-id))
  (let* ((revisions (gee-json-property 'revisions change-info))
         (revision (gee-json-property revision-id revisions))
         (commit (gee-json-property 'commit revision))
         (change-id (gee-json-property 'change_id change-info)))
    (let-alist commit
      (insert .message "\n"
              "Author:    " (gee-review-git-person .author) " " (gee-review-git-date .author) "\n"
              "Committer: " (gee-review-git-person .committer) " " (gee-review-git-date .committer) "\n"
              "Commit:    " revision-id "\n"
              "Parent(s): " (mapconcat (apply-partially #'gee-json-property 'commit)
                                       .parents "\n           ")
              "\n"
              "Change-Id: " (gee-text-query-button change-id (format "change:%s" change-id)) "\n"
              "\n"))))

(defun gee-review-reviewer-tags (reviewers)
  "REVIEWERS should be an alist ((TYPE . [ACCOUNT-INFO]) ...)
where type is `REVIEWER', `CC', or `REMOVED' as specified in the
gerrit ChangeInfo documentation."
  (let-alist reviewers
    (let ((reviewers (cl-mapcar #'gee-reviewer-tag .REVIEWER))
          ;; (removed (seq-map #'gee-reviewer-tag .REMOVED))
          (cc (cl-mapcar #'gee-reviewer-tag .CC)))
      (append reviewers cc))))

(defun gee-review-insert-change-info-block (change-info &optional revision-id)
  "Insert the change info block for CHANGE-INFO with REVISION-ID."
  (setq revision-id (gee-canonical-revision-id change-info revision-id))
  (let-alist change-info
    (insert "Owner:     " (gee-text-query-button .owner.name change-info) "\n"
            "Reviewers: " (mapconcat #'identity (gee-review-reviewer-tags .reviewers) " ") "\n"
            "Project:   " .project "\n"
            "Branch:    " .branch "\n"
            "Topic:     " (or .topic "") "\n")
    (when (string= revision-id (gee-canonical-revision-id change-info))
      (let ((strategy (gee--mergeable change-info revision-id)))
        (when (stringp strategy)
          (insert "Strategy:  " strategy "\n"))))
    (insert "Updated:   " (gee-relative-git-time .updated) "\n")
    (dolist (lbl .labels)
      (insert (format "%s: " (car lbl)))
      (dolist (type (cdr lbl))
        (cl-case (car type)
          ('rejected (insert (format "NO %s " (gee-json-property 'name (cdr type)))))
          ('approved (insert (format "GO %s " (gee-json-property 'name (cdr type)))))
          ('all (cl-loop for reviewer being the elements of (cdr type)
                         for value = (gee-json-property 'value reviewer)
                         unless (zerop (or value 0)) ; no vote
                         do (insert (format "%+d %s " value (gee-json-property 'name reviewer)))))))
      (insert "\n"))
      (insert "ref:       " (gee-json-property (list revision-id 'ref) .revisions) "\n")))

(define-derived-mode gee-review-mode special-mode "Change view"
  "The Gerrit review mode."
  (unless gee-review-active-revision
    (setq gee-review-active-revision (gee-json-property 'current_revision gee-review-change-info)))
  (setq header-line-format `("Change "
                             ,(number-to-string (gee-json-property '_number gee-review-change-info))))
  (let ((inhibit-read-only t))
	(erase-buffer)
    (gee-review-insert-header
     "Change "
     (let-alist gee-review-change-info
       (gee-make-review-link (number-to-string ._number) .id))
     " - " (format "%-20s" (gee-change-header-state gee-review-change-info gee-review-active-revision))
     "   " (gee-make-review-reply-button)
     "   " (gee-review-patchset-buttons-string gee-review-change-info gee-review-active-revision)
     "\n")
    ;; commit information
    (gee-review-insert-commit-info gee-review-change-info gee-review-active-revision)
    ;; revision info
    (gee-review-insert-change-info-block gee-review-change-info gee-review-active-revision)
    ;; Files
    (gee-review-insert-file-section gee-review-change-info gee-review-active-revision)
    ;; Message history
    (gee-review-insert-header "History\n")
    (let ((messages (gee-json-property 'messages gee-review-change-info)))
      (cl-loop for message across messages
               do (let-alist message
                    (insert (propertize (if .author
                                            (gee-account-property 'name .author)
                                          "Gerrit Code Review")
                                        'face '(:weight bold)) "\n"
                            .message "\n"))))
    ))

(defun gee-max-patchset-number (change-info)
  "Return the highest patchset number in CHANGE-INFO."
  (cl-loop
   for (_revision . revision-info) in (cdr (assoc 'revisions change-info))
   maximize (cdr (assoc '_number revision-info))))

(defun gee-changeinfo-current-revision-number (change-info)
  "Return the patch set number of CHANGE-INFO's current revision."
  (let-alist change-info
    (cdr (assoc '_number (cdr (assoc (intern-soft .current_revision) .revisions))))))

(defun gee-patchset-number (change-info &optional revision-id)
  "Return the patchset number in CHANGE-INFO of revision REVISION-ID.
REVISION-ID is resolved using `gee-canonical-revision-id'."
  (let-alist change-info
    (cond ((numberp revision-id)
           revision-id) ;; no need to look up the number if revision-id is already a number
          ((and (stringp revision-id)
                (cond ((string= revision-id "current") (gee-changeinfo-current-revision-number change-info))
                      ((string= revision-id "base") 0))))
          (t (setq revision-id (gee-canonical-revision-id change-info revision-id))
             (let ((revision-info (cl-find revision-id .revisions
                                           :key (lambda (e) (symbol-name (car e)))
                                           :test #'string-prefix-p)))
               (cdr (assoc '_number revision-info)))))))

;;;###autoload
(define-obsolete-function-alias 'gee--canonical-revision-id 'gee-canonical-revision-id "2016-02-25")
;;;###autoload
(defun gee-canonical-revision-id (change-info &optional revision-id)
  "Get the SHA1 of CHANGE-INFO REVISION-ID.
REVISION-ID can be several ways of identifying a revision; nil or
\"current\", the legacy Gerrit revision number, or part (at least
4 characters) of a SHA1.  Signal an error if no or multiple
matching revision(s) is found in the CHANGE-INFO json object.

As a special case, REVISION-ID 0 or \"base\" will return the SHA1
of the parent commit of revision number 1.  This is a valid git
SHA1, but may not be valid in the `/change/' endpoints for
CHANGE-INFO."
  ;; The most common use-case will be to call this function with a
  ;; full SHA1 string matching current_revision, so make that the most
  ;; efficient path.
  ;; The second most common case will be a full SHA1, and as git and
  ;; gerrit can be trusted not to send a revision list with duplicate
  ;; SHA1's the check for multiple matches can be skipped in this case.
  (let-alist change-info
    (ignore .commit.parents)
    (or
     (when (stringp revision-id)
       (cond ((string= revision-id .current_revision) revision-id)
             ((string-match "^[0-9a-f]\\{40\\}$" revision-id)
              (symbol-name (car (cl-find revision-id .revisions
                                         :key (lambda (e) (symbol-name (car e)))
                                         :test #'string-prefix-p))))
             ((string= revision-id "current") .current_revision)
             ((string-match "^[0-9a-f]\\{4,40\\}$" revision-id)
              (let ((rev (cl-member revision-id .revisions
                                    :key (lambda (e) (symbol-name (car e)))
                                    :test #'string-prefix-p)))
                (when rev
                  (if (cl-member revision-id (cdr rev)
                                 :key (lambda (e) (symbol-name (car e)))
                                 :test #'string-prefix-p)
                      (error "'%s' is not a unique revision-id for change %s" revision-id .id))
                  (symbol-name (caar rev)))))
             ((string= revision-id "base") (and (setq revision-id 0) nil))))
     (and (null revision-id) .current_revision)
     (when (numberp revision-id)
       (cond ((= 0 revision-id)
              (let-alist (cdr (gee-find-revision-number .revisions 1))
                ;;TODO: What to do when .commit.parents have more than one element?
                (cdr (assoc 'commit (aref .commit.parents 0)))))
             ((awhen (car (gee-find-revision-number .revisions revision-id))
                (symbol-name it)))))
     (error "'%s' is not a valid revision-id for change %s" revision-id .id))))

(defun gee-find-revision-number (revisions-alist number)
  "Search REVISIONS-ALIST for revision-info object NUMBER."
  (cl-loop for (revision . revision-info) in revisions-alist
           if (= (cdr (assoc '_number revision-info)) number)
           return (cons revision revision-info)))

;;;###autoload
(defun gee-view-change (change-id &optional revision-id diff-base)
  "Open the Gerrit change view for CHANGE-ID.
Show the latest revision unless REVISION-ID is set.
Show file diffs agains DIFF-BASE."
  (interactive "schange-id: ")
  (let* ((change-info (gee-gerrit-rest-get-change-detail change-id
                                                         "ALL_REVISIONS"
                                                         "ALL_COMMITS"
                                                         "ALL_FILES"
                                                         "DRAFT_COMMENTS"))
         (buf (get-buffer-create (format "*Gerrit change %s*"
                                         (cdr (assoc '_number change-info))))))
    (unless change-info
      (error "Change %s not found" change-id))
	(with-current-buffer buf
	  (setq gee-review-change-info change-info
            gee-review-active-revision (gee-canonical-revision-id change-info
                                                                  revision-id)
            gee-review-base-revision (or diff-base 0))
	  (gee-review-mode))
	(switch-to-buffer buf)
    (goto-char (point-min))))

(provide 'gee-review)
;;; gee-review.el ends here

;; Local Variables:
;; indent-tabs-mode: nil
;; tab-width: 4
;; End:
