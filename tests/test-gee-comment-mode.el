;;; test-gee-comment-mode.el ---                     -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Ola Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(require 'gee-comment-mode)
(require 'buttercup)

(defmacro gc--with-temp-content (text &rest body)
  `(with-temp-buffer (insert text) ,@body))

(describe "gee-comment-line-bounds"
  (let ((text "foo\nfooo\n\nfo"))
    (it "return correct bounds for first line"
      (gc--with-temp-content text
        (expect '(1 4) :to-equal (gee-comment-line-bounds 1))))
    (it "return correct bounds for normal lines"
      (gc--with-temp-content text
        (expect '(5 9) :to-equal (gee-comment-line-bounds 2))))
    (it "return correct bounds for empty lines"
      (gc--with-temp-content text
        (expect '(10 10) :to-equal (gee-comment-line-bounds 3))))
    (it "return correct bounds for last line"
      (gc--with-temp-content text
        (expect (gee-comment-line-bounds 4) :to-equal '(11 13))))
    (it "errors when LINE is out of bounds"
      (gc--with-temp-content text
        (expect (gee-comment-line-bounds 0) :to-throw)
        (expect (gee-comment-line-bounds 5) :to-throw)
        (expect (gee-comment-line-bounds -1) :to-throw)))
      ))

(defmacro gc--comment-range (line-start character-start line-end character-end)
  `(list (cons 'start_line ,line-start) (cons 'start_character ,character-start)
    (cons 'end_line ,line-end) (cons 'end_character ,character-end)))

(describe "gee-comment-range-bounds"
  (let ((text "foo\nfooo\n\nfo"))
    (it "Handles part of line"
      (gc--with-temp-content
       text
       (expect (gee-comment-range-bounds (gc--comment-range 2 1 2 3)) :to-equal '(5 7))))
    (it "Handles two lines"
        (gc--with-temp-content
         text
         (expect (gee-comment-range-bounds (gc--comment-range 2 2 4 2)) :to-equal '(6 12))))
    (it "handles the start of the buffer"
        (gc--with-temp-content
         text
         (expect (gee-comment-range-bounds (gc--comment-range 1 1 4 2)) :to-equal '(1 12))))
    (it "handles the end of the buffer"
        (gc--with-temp-content
         text
         (expect (gee-comment-range-bounds (gc--comment-range 2 2 4 3)) :to-equal '(6 13))))
    (it "errors when out of bounds"
      (gc--with-temp-content
       text
       (expect (gee-comment-range-bounds (gc--comment-range 4 1 5 2)) :to-throw)
       (expect (gee-comment-range-bounds (gc--comment-range 1 5 2 3)) :to-throw)
       (expect (gee-comment-range-bounds (gc--comment-range 1 1 5 3)) :to-throw)
       (expect (gee-comment-range-bounds (gc--comment-range 1 1 3 4)) :to-throw)))
    ))

(describe "gee-comment-info-before-p"
  (let ((a '((updated . "2016-04-03 21:12:47.891000000")))
        (b '((updated . "2016-04-03 21:12:47.892000001"))))
    (it "Sorts correct on last time digit"
      (expect (gee-comment-info-before-p a b) :to-be-truthy)
      (expect (gee-comment-info-before-p b a) :not :to-be-truthy))
    (it "Should compare equal strings as equal"
      (expect (gee-comment-info-before-p a a) :to-be-truthy)
      (expect (gee-comment-info-before-p b b) :to-be-truthy))))

(describe "gee-comment-process-comments"
  (before-each
    (spy-on 'gee-comment-make-comment-overlay
            :and-call-fake (lambda (comment-info &rest prop-list) (ignore comment-info prop-list))))
  (it "Should never create overlays for empty lists"
    (expect (gee-comment-process-comments nil nil) :not :to-throw)
    (expect 'gee-comment-make-comment-overlay :not :to-have-been-called))
  (let ((comments '(((id . "foo") (updated . "2016-04-03 21:12:47.891000001"))
                    ((id . "bar") (updated . "2016-04-03 21:12:47.891000000"))
                    ((id . "baz") (updated . "2016-04-03 22:12:47.891000000"))
                    ((id . "qux") (updated . "2016-04-02 21:12:47.891000000"))))
        (drafts '(((id . "food") (updated . "2016-04-03 21:12:48.891000001"))
                  ((id . "bard") (updated . "2016-04-03 21:12:48.891000000"))
                  ((id . "bazd") (updated . "2016-04-03 22:12:48.891000000"))
                  ((id . "quxd") (updated . "2016-04-02 21:12:48.891000000")))))
    (it "Should call gee-comment-make-comment-overlay once per comment"
      (expect (gee-comment-process-comments comments drafts) :not :to-throw)
      (expect 'gee-comment-make-comment-overlay :to-have-been-called)
      (expect (spy-calls-count 'gee-comment-make-comment-overlay) :to-equal (+ (length comments) (length drafts))))
    (it "Should call gee-comment-make-comment-overlay in timestamp order"
      (expect (gee-comment-process-comments comments drafts) :not :to-throw)
      (let ((args (spy-calls-all-args 'gee-comment-make-comment-overlay)))
        (expect (length args) :to-equal  (+ (length comments) (length drafts)))
        (expect args :to-equal (cl-sort (copy-sequence args) #'gee-comment-info-before-p :key #'car)))))
  (it "Should call gee-comment-make-comment-overlay in nesting order"
    (expect (gee-comment-process-comments
             '(((id . "5") (in_reply_to . "4"))
               ((id . "1"))
               ((id . "2") (in_reply_to . "1")))
             '(((id . "6") (in_reply_to . "5"))
               ((id . "3") (in_reply_to . "2"))
               ((id . "4") (in_reply_to . "3")))) :not :to-throw)
    (let ((args (spy-calls-all-args 'gee-comment-make-comment-overlay)))
      (expect (mapcar (lambda (e)
                        (cdr (assoc 'id (car e)))) args)
              :to-equal '("1" "2" "3" "4" "5" "6")))))

(provide 'test-gee-comment-mode)
;;; test-gee-comment-mode.el ends here
