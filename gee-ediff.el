;;; gee-ediff.el ---                                 -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Ola Nilsson

;; The gee-ediff-quit-hook and gee-ediff-restore-previous-winconf and
;; associated code was copied from magit-ediff.el version
;; 20160606.1254 and are Copyright (C) 2010-2015 The Magit Project
;; Contributors

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(require 'ediff-util)

(require 'gee-util)
(require 'gee-gerrit-rest)
(autoload 'gee-patchset-number "gee-review")
(autoload 'gee-canonical-revision-id "gee-review")

(defgroup gee-ediff nil "Gee Ediff customizations." :group 'gee)

(defun gee-find-revision (change-info file-path revision)
  "For CHANGE-INFO, return a buffer visiting FILE-PATH version REVISION.
The file will be named `gee-tmp-dir'/`project'/FILE-PATH.~change:patchset~."
  (let-alist change-info
    (let* ((filename (format "%s/%s/%s.~%d:%d~" gee-tmp-dir .project
                            file-path ._number
                            (gee-patchset-number change-info revision)))
           (filebuf (or (get-file-buffer filename)
                        (progn (make-directory (file-name-directory filename) t)
                               (find-file-noselect filename))
                        (get-buffer-create (file-name-nondirectory filename))))
           (commit-id (gee-canonical-revision-id change-info revision)))
      (unless (file-exists-p filename)
        (message "Checking out %s..." filename)
        (with-current-buffer filebuf
          (let ((inhibit-read-only t))
            (insert (gee-gerrit-rest-project-commit-get-content .project commit-id file-path))
            (write-file filename))))
      filebuf)))

(defvar gee-ediff-active nil "Indicate to `gee-ediff-mode-setup' that gee is active.")


(defcustom gee-ediff-quit-hook
  '(ediff-cleanup-mess
    gee-ediff-restore-previous-winconf)
  "Hooks to run after finishing Ediff, when that was invoked using Gee.
The hooks are run in the Ediff control buffer.  This is similar
to `ediff-quit-hook' but takes the needs of Gee into account.
The `ediff-quit-hook' is ignored by Ediff sessions which were
invoked using Gee."
  :package-version '(gee . "0.1")
  :group 'gee-ediff
  :type 'hook
  :options '(ediff-cleanup-mess
             gee-ediff-restore-previous-winconf))

(defvar gee-ediff-previous-winconf nil
  "Temporarily store the windows configuration before `gee-ediff'.")

(defun gee-ediff-restore-previous-winconf ()
  "Restore the window configuration from `gee-ediff-previous-winconf'."
  (set-window-configuration gee-ediff-previous-winconf))

(defun gee-ediff (change-info file-path revision-id base-revision)
  "CHANGE-INFO FILE-PATH REVISION-ID BASE-REVISION."
  (let-alist change-info
    (let ((bufa (gee-find-revision change-info file-path base-revision))
          (bufb (gee-find-revision change-info file-path revision-id))
          (winconf (current-window-configuration))
          (ediff-mode-hook (append ediff-mode-hook
                                   (list 'gee-ediff-mode-setup)))
          (gee-ediff-active t)
          (ediff-prepare-buffer-hook
           (append ediff-prepare-buffer-hook
                   (list (lambda ()
                           (setq-local ediff-toggle-read-only-function
                                       #'ignore))))))
      (ediff-setup bufa (buffer-file-name bufa)
                   bufb (buffer-file-name bufb)
                   nil nil
                   `((lambda ()
                       (setq-local
                        ediff-quit-hook
                        (lambda ()
                          ,@(unless bufa
                              '((ediff-kill-buffer-carefully ediff-buffer-A)))
                          ,@(unless bufb
                              '((ediff-kill-buffer-carefully ediff-buffer-B)))
                          ;; ,@(unless bufC
                          ;;     '((ediff-kill-buffer-carefully ediff-buffer-C)))
                          (let ((gee-ediff-previous-winconf ,winconf))
                            (run-hooks 'gee-ediff-quit-hook))))))
                   (list (cons 'ediff-job-name 'ediff-buffers))))))

(defun gee-ediff-mode-setup ()
  "Set up ediff for gee."
  (when gee-ediff-active
    (setq-local ediff-make-buffers-readonly-at-startup t)
    (setq-local ediff-toggle-read-only-function #'ignore)
    (setq-local gee-ediff-active t)))

(provide 'gee-ediff)
;;; gee-ediff.el ends here
