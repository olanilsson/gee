;;; test-gee-gerrit-rest.el --- tests for gee-gerrit-rest.el  -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Ola Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'gee-gerrit-rest)
(require 'buttercup)
(add-to-list 'load-path (file-name-directory load-file-name))
(require 'utils-gee-tests)

(defvar url-http-end-of-headers)
(make-variable-buffer-local 'url-http-end-of-headers)

(describe "gee--gerrit-rest-request"
  (after-each (spy-calls-reset 'url-retrieve-synchronously))
  (it "Returns vector"
    (with-url-spy :json-string "[1, 2, 3]"
                   (expect (gee--rest-request "dummy-url") :to-equal [1 2 3])))
  (it "Returns alist"
    (with-url-spy :json-string "{\"a\": 1}"
                  (expect (gee--rest-request "dummy-url") :to-equal '((a . 1)))))
  (it "Returns string"
    (with-url-spy :json-string "Not JSON"
                  (expect (gee--rest-request "dummy-url") :to-equal "Not JSON")))
  (it "Returns number as string"
    (with-url-spy :json-string "407"
                  (expect (gee--rest-request "dummy-url") :to-equal "407")))
  (it "Returns status code when no \")]}'\""
    (with-url-spy :raw "error message" :http-status 409
                  (expect (gee--rest-request "dummy-url") :to-equal 409)))
  )

(describe "gee--gerrit-rest-build-url"
  (it "uses gee-gerrit-rest-base as base"
    (let ((gee-gerrit-rest-base "BASE"))
      (expect (gee--gerrit-rest-build-url "/foobar/")
              :to-equal "BASE/foobar/")))
  (it "builds query strings"
    (expect (gee--gerrit-rest-build-url "/endpoint/"
                                        '((foo "foo")
                                          (bar "bar")))
            :to-match "?foo=foo&bar=bar$")))

(describe "Check basic REST properties"
  ;;url-retrieve-synchronously called with correct URI, including
  ;; gee-gerrit-rest-base.
  ;;json-read called with correct settings
  :var (gerritbase gee-gerrit-rest-base)
  (before-all (setq gee-gerrit-rest-base "GERRITBASE"))
  (after-all  (setq gee-gerrit-rest-base gerritbase))
  (after-each (spy-calls-reset 'url-retrieve-synchronously))
  (it "for gee-gerrit-rest-account"
	(with-bad-json-settings
	 (with-url-spy :json-file "account-424242.json"
				   (expect (gee-gerrit-rest-account 424242)
						   :to-alist-equal  '((_account_id . 424242)
											  (name . "Joe Random User")
											  (email . "joe@gerrit.test")))
				   (expect 'url-retrieve-synchronously
						   :to-have-been-called-with "GERRITBASE/a/accounts/424242"))))
  (it "for gee-gerrit-rest-changes-list"
	(with-bad-json-settings
	 (with-url-spy :json-file "changes-2-open.json"
				   (expect (gee-gerrit-rest-changes-list "status:open") :to-be-correct-json)
				   (expect 'url-retrieve-synchronously
						   :to-have-been-called-with "GERRITBASE/a/changes/?q=status:open"))))
  (it "for gee-gerrit-rest-get-change"
	(with-bad-json-settings
	 (with-url-spy :json-file "simple-change-12345.json"
				   (expect (gee-gerrit-rest-get-change 12345) :to-be-correct-json)
				   (expect 'url-retrieve-synchronously
						   :to-have-been-called-with "GERRITBASE/a/changes/12345"))))
  (it "for gee-gerrit-rest-get-change-revision-get-mergeable"
	(with-bad-json-settings
	 (with-url-spy :json-file "mergeable.json"
				   (expect (gee-gerrit-rest-change-revision-get-mergeable 12345)
						   :to-alist-equal '((submit_type . "MERGE_IF_NECESSARY") (mergeable . t)))
				   (expect (url-retrieve-synchronously
							:to-have-been-called-with "GERRITBASE/a/changes/12345/revisions/current/mergeable")))))
  (it "for gee-gerrit-rest-get-change-revision-get-submit-type"
	(with-bad-json-settings
	 (with-url-spy :json-string "MERGE_IF_NECESSARY"
				   (expect (gee-gerrit-change-revision-get-submit-type 12345)
						   :to-equal "MERGE_IF_NECESSARY")
				   (expect (url-retrieve-synchronously
							:to-have-been-called-with "GERRITBASE/a/changes/12345/revisions/current/submit_type")))))
  )

(provide 'test-gee-gerrit-rest)
;;; test-gee-gerrit-rest.el ends here
