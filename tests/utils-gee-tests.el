;;; utils-gee-tests.el --- Utilities for gee buttercup tests  -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Ola Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(eval-and-compile (require 'buttercup))

(defun alist-equalish (a b)
  "Compare to possibly nested alists A and B for set equality.
The alists are considered equal even if the elements are not in
the same order.  Nested lists are compared recursively.  Alist
cells (KEY . VALUE) are compared with `equal'"
  (cond ((and (null a) (null b)))
        ((or (null a) (null b)) nil)
        ((or (not (listp a)) (not (listp b))) (equal a b))
        ;; both a and b are lists and neither is nil
        ((and (cl-notevery #'consp a) (cl-notevery #'consp b)) (equal a b))
        ((and (= (length a) (length b))
              (cl-every (lambda (ae)
                          (let ((be (assoc (car ae) b)))
                            (or (and (listp (cdr ae)) (listp (cdr be))
                                     (alist-equalish (cdr ae) (cdr be)))
                                (equal ae be))))
                        a)))
        (t nil)))

(buttercup-define-matcher :to-alist-equal (a b)
  (if (alist-equalish (funcall a) (funcall b))
      (cons t (format "Expected %S not to be equalish to %S" a b))
    (cons nil (format "Expected %S to be equalish to %S" a b))))

(describe "Verify the alist matcher"
  (it "is ok with two empty lists"
    (expect nil :to-alist-equal nil))
  (it "is not ok with one empty list"
    (expect nil :not :to-alist-equal '(2)))
  (it "will match non-list values with equal"
    (expect 2 :to-alist-equal 2)
    (expect 2 :not :to-alist-equal '((foo . 2)))
    (expect '((foo . 2)) :not :to-alist-equal 5))
  (it "handles non-alists"
    (expect '(1 2 3) :to-alist-equal '(1 2 3))
    (expect '(2 3 1) :not :to-alist-equal '(1 2 3)))
  (it "will match equal-order alists"
    (expect '((foo . 2) (bar . "bar") (baz . (1 2 3)))
            :to-alist-equal '((foo . 2) (bar . "bar") (baz . (1 2 3)))))
  (it "will match not-equal-order alists"
    (expect '((foo . 2) (bar . "bar") (baz . (1 2 3)))
            :to-alist-equal '((foo . 2) (bar . "bar") (baz . (1 2 3)))))
  (it "will handle nested alists"
    (expect '((foo . ((bar . 1) (baz . 2))) (quux . 3))
            :to-alist-equal '((quux . 3) (foo . ((baz . 2) (bar . 1)))))))

(defun to-be-or-contain (item-or-sequence val)
  "Check if ITEM-OR-SEQUENCE is `eq` to or contain VAL.
If ITEM-OR-SEQUENCE is a sequence, check if VAL is contained in
that sequence.  If ITEM-OR-SEQUENCE is not a list, check if it is
`eq' to VAL."
  (if (sequencep item-or-sequence)
	  (member val item-or-sequence)
	(eq item-or-sequence val)))

(buttercup-define-matcher-for-binary-function
	:to-be-or-contain to-be-or-contain
  :expect-match-phrase "Expected `%A' to be or contain `%b', but `%a' was/did not."
  :expect-mismatch-phrase "Expected `%A' not to be or contain `%b', but `%a' was/did.")

(describe "Verify the :to-be-or-contain matcher"
  (it "works as :to-be"
    (expect 'test-symbol :to-be-or-contain 'test-symbol)
    (expect 'test-symbol :not :to-be-or-contain nil)
    (expect nil :not :to-be-or-contain 'test-symbol)
    (expect 'other-symbol :not :to-be-or-contain 'test-symbol))
  (it "works as :to-contain"
    (expect (list 1 2 3) :to-be-or-contain 3)
    (expect (list 1 2 3) :not :to-be-or-contain 4)))

(buttercup-define-matcher-for-binary-function
	:to-be-a-key-in assoc
  :expect-match-phrase "Expected `%A' to be a key in `%b', but it wasn't."
  :expect-mismatch-phrase "Expected `%A' not to be a key in `%b', but it was.")

(describe "The :to-be-a-key-in matcher"
  (it "should find symbols that are keys in alists"
  	(expect 'a :to-be-a-key-in '((b . 1) (a . 2) (c . 4)))
  	(expect 'd :not :to-be-a-key-in '((b . 1) (a . 2) (c . 4))))
  (it "should find integers that are keys in alists"
	(expect 1 :to-be-a-key-in '((1 . b) (2 . a) (4 . c)))
	(expect 7 :not :to-be-a-key-in '((1 . b) (2 . a) (4 . c))))
  (it "should find strings that are keys in alists"
  	(expect "a" :to-be-a-key-in '(("b" . 1) ("a" . 2) ("c" . 4)))
  	(expect "d" :not :to-be-a-key-in '(("b" . 1) ("a" . 2) ("c" . 4)))))

(defun gee-json-p (object)
  "Check if OBJECT is a default-coded json object."
  (cond ((listp object)
         (cl-every (lambda (elt) (and (consp elt)
                                      (symbolp (car elt))
                                      (if (or (listp (cdr elt))
                                              (vectorp (cdr elt)))
                                          (gee-json-p (cdr elt))
                                        t)))
				   object))
        ((vectorp object)
         (cl-every #'gee-json-p object))))

(buttercup-define-matcher-for-unary-function :to-be-correct-json gee-json-p
  :expect-match-phrase "Expected `%A' to be a valid JSON object, but instead it was `%a'."
  :expect-mismatch-phrase "Expected `%A' not to be a valid JSON object, but it was `%a'.")

(describe "Verify the :to-be-correct-json matcher"
  (it "2 not to be correct json"
    (expect 2 :not :to-be-correct-json))
  (it "the empty list to be ok"
    (expect '() :to-be-correct-json))
  (it "the empty vector to be ok"
    (expect '[] :to-be-correct-json))
  (it "an alist with symbol keys to be ok"
    (expect '((foo . 2) (bar . "string")) :to-be-correct-json))
  (it "an alist with non-symbol keys to fail"
    (expect '((2 . foo) (bar . "string")) :not :to-be-correct-json))
  (it "an non-empty not alist to fail"
    (expect '((bar . "string") 2 foo) :not :to-be-correct-json))
  (it "a vector of alists to be correct"
    (expect '[((foo . 1)) ((bar . 2))] :to-be-correct-json))
  (it "nested lists to be ok"
    (expect '[(;(id . "aaa") (v . []) (f . :json-false) (i . 22)
               (r . ((n . 22))))]
    :to-be-correct-json)
  ))

(defvar url-http-end-of-headers)
(defvar url-http-response-status)
(make-variable-buffer-local 'url-http-end-of-headers)
(make-variable-buffer-local 'url-http-response-status)

(defvar gee-testfile-path
  (let ((gee-topdir (locate-dominating-file default-directory "Cask")))
    (list gee-topdir (concat gee-topdir "tests/")))
  "Paths to search for json testfiles in.")

(defvar gee-http-status-texts
  '((100 . "Continue")
    (101 . "Switching protocols")
    (102 . "Processing") ; webdav rfc 2518
    (200 . "OK")
    (201 . "Created")
    (202 . "Accepted")
    (203 . "Non-Authorative Information")
    (204 . "No content")
    (205 . "Reset Content")
    (206 . "Partial Content") ; rfc 7233
    (207 . "Multi-Status") ; webdav rfc 4918
    (208 . "Already Reported") ; webdav rfc 5842
    (226 . "IM Used") ;RFC 3229
    (300 . "Multiple Choices")
    (301 . "Moved Permanently")
    (302 . "Found")
    (303 . "See Other")
    (304 . "Not Modified") ; rfc 7232
    (305 . "Use Proxy")
    (306 . "Switch Proxy")
    (307 . "Temporary Redirect")
    (308 . "Permanent Redirect")
    (400 . "Bad Request")
    (401 . "Unauthorized")
    (402 . "Payment Required")
    (403 . "Forbidden")
    (404 . "Not Found")
    (405 . "Method Not Allowed")
    (406 . "Not Acceptable")
    (407 . "Proxy Authentication Required")
    (408 . "Request Timeout")
    (409 . "Conflict")
    (410 . "Gone")
    (411 . "Length Required")
    (412 . "Precondition Failed")
    (413 . "Payload Too Large")
    (414 . "URI Too Long")
    (415 . "Unsupported Media Type")
    (416 . "Range Not Satisfiable")
    (417 . "Expectation Failed")
    (418 . "I'm A Teapot")
    (421 . "Misdirected Request")
    (422 . "Unprocessable Entity")
    (423 . "Locked")
    (424 . "Failed Dependency")
    (426 . "Upgrade Required")
    (428 . "Precondition Required")
    (429 . "Too Many Requests")
    (431 . "Request Header Fields Too Large")
    (451 . "Unavailable For Legal Reasons")
    (500 . "Internal Server Error")
    (501 . "Not Implemented")
    (502 . "Bad Gateway")
    (503 . "Service Unavailable")
    (504 . "Gateway Timeout")
    (505 . "HTTP Version Not Supported")
    (506 . "Variant Also Negotiates")
    (507 . "Insufficient Storage")
    (508 . "Loop Detected")
    (510 . "Not Extended")
    (511 . "Network Authnetication Required"))
  "Human readable strings for HTTP status codes.")

(cl-defun test-gee-url-buffer-setup (&key json-string
                                          json-file
                                          raw
                                          (buffer-name " *gee testbuf*")
                                          (http-status 200)
                                     )
  "Create and return a buffer as that made by `url-retrieve-synchronously'.
The buffer contains a HTTP response as delivered by a Gerrit
server, and the required local variables set up.  The HTTP status
value is set from HTTP-STATUS.  If RAW is set, that string is
inserted as reply content.  Otherwise use JSON-STRING or the
content of JSON-FILE.  The gerrit response prefix line \")]}\" is
added when any of he JSON arguments is used.  The buffer name can
be specified in BUFFER-NAME."
  (let ((buf (generate-new-buffer buffer-name))
        content-length)
    (with-current-buffer buf
      (if raw
          (insert raw)
        (insert ")]}'\n")
        (cond ((and json-string json-file) (error "Both json-string and json-file set"))
              (json-string (insert json-string "\n"))
              (json-file (insert-file-contents (locate-file json-file gee-testfile-path)))))
      (setq content-length (point-max))
      (goto-char (point-min))
      (insert "HTTP/1.1 " (number-to-string http-status) " "
              (or (cdr (assoc http-status gee-http-status-texts)) "Unknown Status") "
Date: " (format-time-string "%a, %e %b %Y %T %Z" nil t)
"
Server: Apache/2.4.7 (Ubuntu)
Content-Disposition: attachment
X-Content-Type-Options: nosniff
Cache-Control: no-cache, no-store, max-age=0, must-revalidate
Pragma: no-cache
Expires: Mon, 01 Jan 1990 00:00:00 GMT
Content-Type: application/json; charset=UTF-8
Content-Length: " (number-to-string content-length)
"
Keep-Alive: timeout=5, max=100
Connection: Keep-Alive
")
      (setq-local url-http-end-of-headers (point))
      (setq-local url-http-response-status http-status)
      (insert "\n"))
    buf))

(cl-defun spy-on-url-retrieve (&rest other-keys)
  "Set up a fake `url-retrieve-synchronously' function for testing.
The fake function is defined using buttercups `spy-on' and will
return a buffer created by `test-gee-url-buffer-setup'.
All arguments are passed to `test-gee-url-buffer-setup'.
\n(fn [KEYWORD VALUE ...])"
  (let ((buf (apply #'test-gee-url-buffer-setup other-keys)))
    (spy-on 'url-retrieve-synchronously :and-return-value buf)))

(cl-defmacro with-url-spy (&rest other-keys-and-body)
  "Sets up a buttercup spy environment for `url-retrieve-synchronously'.
Creates a buffer with contents as delivered by a Gerrit server
and the required local variables set.  That buffer is returned by
the `url-retrieve-synchronously' replacement function.

Keywords not used by `with-url-spy' are passed on to
`test-gee-url-buffer-setup'.

\n(fn [KEYWORD VALUE ...] BODY)"
  (let ((buffer-varname (cl-gensym "buf"))
        other-keys)
    (while (and (cddr other-keys-and-body) ; at least two elements
                (or (symbolp (car other-keys-and-body))
                    (and (listp (car other-keys-and-body))
                         (eq 'quote (caar other-keys-and-body)))))
      (push (pop other-keys-and-body) other-keys)
      (push (pop other-keys-and-body) other-keys))
    (setq other-keys (nreverse other-keys))
    `(let ((,buffer-varname (test-gee-url-buffer-setup ,@other-keys)))
       (spy-on 'url-retrieve-synchronously :and-return-value ,buffer-varname)
       ,@other-keys-and-body)))

(defmacro with-bad-json-settings (&rest body)
  "Execute BODY with incorrect `json-read' settings."
  `(let ((json-object-type 'plist)
         (json-array-type 'list)
         (json-false :my-json-false)
         (json-null :my-json-null)
         (json-key-type 'string))
     ,@body))


(provide 'utils-gee-tests)
;;; utils-gee-tests.el ends here
