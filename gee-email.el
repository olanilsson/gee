;;; gee-email.el --- Actions on Gerrit notification emails -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Ola Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords: vc

;;; Commentary:

;; Commands that can trigger gerrit interactions from buffers
;; containing Gerrit notification emails.  These emails have a footer
;; of key-value pairs (details in
;; https://gerrit-review.googlesource.com/Documentation/user-notify.html)
;; that can be read with `gee-gerrit-email-data'.

;;; Code:

(require 'gee-gerrit-rest)

(defvar gee-vote-hist nil
  "History of Gerrit vote values used by `gee-read-vote-from-minibuffer'.")

(defun gee-read-vote-from-minibuffer (label min max default)
  "Read a Gerrit vote value from the minibuffer.
LABEL will be included in the prompt.  MIN and MAX are the
minimum and maximum accepted values.  DEFAULT will be used if the
user enters an empty string."
  (let (vote)
    (while
        (let ((str (read-from-minibuffer
                    (format "%s value (%d..%d) [%d]: " label min max default)
                    nil nil nil 'coan-mu4e-gerrit-vote-hist (number-to-string default))))
          (condition-case nil
              (setq vote (cond
                          ((zerop (length str)) default)
                          ((stringp str) (read str))))
            (error nil))
          (unless (and (integerp vote)
                       (<= vote max)
                       (>= vote min))
            (message "Please enter a %s vote in the interval %d to %d." label min max)
            (sit-for 1)
            t)))
    vote))

(defun gee-gerrit-email-data (&optional buffer)
  "Read the fields of the footer of a Gerrit notification email in BUFFER.
The read data is returned in an alist where each footer
`Gerrit-Foo: Bar' is represented by a cons `(\"Foo\" , \"Bar\")'.
Some footers may be present more than once, each one will be
added to the alist.  For further information about the footers
see URL
`https://gerrit-review.googlesource.com/Documentation/user-notify.html#footers'."
  (let (data)
    (with-current-buffer (or buffer (current-buffer))
      (save-excursion
        (save-match-data
          (goto-char (point-min))
          (while (re-search-forward "^Gerrit-\\(.*\\): \\(.*\\) *$" nil t)
            (push (cons (match-string 1) (match-string 2)) data)))))
    (unless data (user-error "No Gerrit data footer found in buffer"))
    (nreverse data)))

;;;###autoload
(defun gee-gerrit-email-restore ()
  "Restore the gerrit change mentioned in a Gerrit abandon email."
  (interactive)
  (let* ((data (gee-gerrit-email-data))
         (change-nbr (cdr (assoc "Change-Number" data))))
    (unless change-nbr
      (user-error "Gerrit-Change-Id and/or Gerrit-Change-Number footer missing"))
    (message "Restoring %s" change-nbr)
    (if (gee-gerrit-rest-restore-change change-nbr)
		(message "%s restored" change-nbr)
	  (message "Failed to restore %s" change-nbr))))

;;;###autoload
(defun gee-gerrit-email-code-review (&optional value)
  "Vote for Code-Review with VALUE (default +1)."
  (interactive "P")
  (let* ((data (gee-gerrit-email-data))
         (change-nbr (cdr (assoc "Change-Number" data)))
         (change-ps (cdr (assoc "PatchSet" data))))
    (unless (and change-nbr change-ps)
      (user-error "Gerrit-Change-Number and/or Gerrit-PatchSet footer missing"))
    (if (null value)
        (setq value 1)
      (unless (and (integerp value)
                   (<= value 2)
                   (>= value -2))
		;; TODO: Proper max and min from project settings
        (setq value (gee-read-vote-from-minibuffer "Code-Review" -2 2 1))))
    (gee-gerrit-rest-change-revision-set-review
     change-nbr change-ps nil `(("Code-Review" . ,value)) nil nil :not-strict-labels)
    ))

;;;###autoload
(defvar gee-email-map
  (let ((m (make-sparse-keymap)))
	(define-key m "r" 'gee-gerrit-email-restore)
	(define-key m "c" 'gee-gerrit-email-code-review)
	m)
  "Keymap of gerrit email commands.")

(provide 'gee-email)
;;; gee-email.el ends here
