;;; test-review.el --- tests for gee-review.el  -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Ola Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(or (require 'gee-review nil t)
	(load "./gee-review")
	(error "Failed to load gee-review"))
(or (require 'buttercup)
	(and
	(load (expand-file-name "~/emacs-packages/emacs-buttercup/buttercup-compat") nil t)
	(load (expand-file-name "~/emacs-packages/emacs-buttercup/buttercup") nil t))
	(error "Failed to load buttercup"))
(add-to-list 'load-path (file-name-directory load-file-name))
										;(require 'utils-gee-tests)

(describe "gee-change-header-state"
  :var ((change-info
         '((current_revision . "12345678901234567890")
           (revisions . ((\12345678901234567890 . ((_number . 2)))
                         (\00000000000000000001 . ((_number . 1))))))))
  (it "Not Current"
	(expect (gee-change-header-state change-info 1) :to-equal "Not Current"))
  (it "Draft"
	(expect (gee-change-header-state (cons '(status . "DRAFT") change-info))
			:to-equal "Draft"))
  (it "Merged"
	(expect (gee-change-header-state (cons '(status . "MERGED") change-info))
			:to-equal "Merged"))
  (it "Abandoned"
	(expect (gee-change-header-state (cons '(status . "ABANDONED") change-info))
			:to-equal "Abandoned"))
  (it "Submitted"
	(expect (gee-change-header-state (cons '(status . "SUBMITTED") change-info))
			:to-equal "Submitted, merge pending"))
  (let* ((approved '(A . ((approved . ((_account_id . 22))))))
		 (empty    '(E . ()))
		 (optional '(O . ((optional . t))))
		 (rejected '(R . ((rejected . ((_account_id . 22))))))
		 (l-approved `(labels . ,(list approved)))
		 (l-approved-opt `(labels . ,(list approved optional)))
		 (l-needs `(labels . ,(list empty)))
		 (l-needs-opt `(labels . ,(list optional empty)))
		 (l-needs-opt-rej `(labels . ,(list optional empty rejected)))
		 (l-not `(labels . ,(list rejected empty)))
		 (l-not-opt `(labels . ,(list optional rejected)))
		 (l-not-opt-approved `(labels . ,(list optional rejected approved))))
    (it "Submittable"
      (expect (gee-change-header-state (cons l-approved
                                             change-info))
              :to-equal "Ready to submit")
      (expect (gee-change-header-state (cons l-approved-opt
                                             change-info))
              :to-equal "Ready to submit"))
    (it "Needs label"
      (expect (gee-change-header-state (cons l-needs
                                             change-info))
              :to-equal "Needs E")
      (expect (gee-change-header-state (cons l-needs-opt
                                             change-info))
              :to-equal "Needs E")
      (expect (gee-change-header-state (cons l-needs-opt-rej
                                             change-info))
              :to-equal "Needs E"))
    (it "Blocking label"
      (expect (gee-change-header-state (cons l-not
                                             change-info))
              :to-equal "Not R")
      (expect (gee-change-header-state (cons l-not-opt
                                             change-info))
              :to-equal "Not R")
      (expect (gee-change-header-state (cons l-not-opt-approved
                                             change-info))
              :to-equal "Not R"))))

(describe "gee-review-insert-header"
  (cl-labels ((insert-to-string (&rest args) (with-temp-buffer
                                               (apply #'gee-review-insert-header args)
                                               (buffer-substring (point-min) (point-max)))))
    (it "does nothing with no arguments"
      (expect (insert-to-string) :to-equal "")
      (expect 'next-button :not :to-have-been-called))
    (it "finds adjacent buttons"
      (let ((str (insert-to-string (make-text-button "1" 0)
                                   (make-text-button "1" 0)
                                   (make-text-button "1" 0)
                                   (make-text-button "1" 0)
                                   (make-text-button "1" 0))))
        (expect str :to-equal "11111")
        (dotimes (i (length str))
          (expect (get-text-property i 'face str) :to-equal '(button gee-review-section))
          (expect (get-text-property i 'button str) :to-equal '(t)))
        ))
    (it "finds separated buttons"
      (let ((str (insert-to-string (make-text-button "1" 0)
                                   "-"
                                   (make-text-button "2" 0))))
        (expect str :to-equal "1-2")
        (dotimes (i (length str))
          (expect (get-text-property i 'face str) :to-be-or-contain 'gee-review-section)
          (if (cl-evenp i)
              (expect (get-text-property i 'face str) :to-contain 'button)
            (expect (get-text-property i 'face str) :not :to-be-or-contain 'button)))))
    ))

(describe "gee-review-file-info-diff"
  (it "Set correct FileInfo status"
    (let ((val-map '(("A" . "ADDED") ("D" . "DELETED") ("R" . "RENAMED")
                     ("C" . "COPIED") ("W" . "REWRITTEN") (nil . "MODIFIED"))))
      (dolist (pair val-map)
        (spy-on 'gee-gerrit-rest-get-diff :and-return-value `((change_type . ,(cdr pair))))
        (let-alist (gee-review-file-info-diff 252525 2 7 'filename)
          (expect .status :to-equal (car pair))
          (expect .lines_inserted :to-be nil)
          (expect .lines_deleted :to-be nil))
        (spy-calls-reset 'gee-gerrit-rest-get-diff))))
  (it "propagates the binary flag"
    (spy-on 'gee-gerrit-rest-get-diff :and-return-value '((change_type . "MODIFIED")))
    (let-alist (gee-review-file-info-diff 1 2 3 'name :binary)
      (expect .binary :to-be t)
      (expect .lines_inserted :to-be nil)
      (expect .lines_deleted :to-be nil))
    (let-alist (gee-review-file-info-diff 1 2 3 'name nil)
      (expect .binary :to-be nil)
      (expect .lines_inserted :to-be nil)
      (expect .lines_deleted :to-be nil))
    (spy-calls-reset 'gee-gerrit-rest-get-diff))
  (it "sets old path properly"
    (dolist (change-type '("RENAMED" "COPIED"))
      (spy-on 'gee-gerrit-rest-get-diff :and-return-value `((change_type . ,change-type)
                                                            (meta_a . ((name . "correct-name")))
                                                            (meta_b . ((name . "wrong-name")))))
      (let-alist (gee-review-file-info-diff 1 2 3 'name)
        (expect .old_path :to-equal "correct-name")
        (expect .lines_inserted :to-be nil)
        (expect .lines_deleted :to-be nil))
      (spy-calls-reset 'gee-gerrit-rest-get-diff))
    (spy-on 'gee-gerrit-rest-get-diff :and-return-value `((change_type . "MODIFIED")
                                                          (meta_a . ((name . "wrong-name-a")))
                                                          (meta_b . ((name . "wrong-name-b")))))
    (let-alist (gee-review-file-info-diff 1 2 3 'name)
      (expect .old_path :to-be nil)
      (expect .lines_inserted :to-be nil)
      (expect .lines_deleted :to-be nil))
    (spy-calls-reset 'gee-gerrit-rest-get-diff))
  (it "computes lines inserted and deleted"
    (spy-on 'gee-gerrit-rest-get-diff :and-return-value `((change_type . "MODIFIED")
                                                          (content . [((a . [1 2 3])) ((b . [4 5 6 7]))])
                                                          (meta_a . ((name . "wrong-name-a")))
                                                          (meta_b . ((name . "wrong-name-b")))))
    (let-alist (gee-review-file-info-diff 1 2 3 'name)
      (expect .lines_inserted :to-equal 4)
      (expect .lines_deleted :to-equal 3))
    (spy-calls-reset 'gee-gerrit-rest-get-diff))
  )

(describe "gee-review-list-files")

(describe "gee-patchset-number"
  :var ((change-info '((id . "project~target~Icfbd91722e453e75cfda11b5250759f5b1393fef")
                       (change_id . "Icfbd91722e453e75cfda11b5250759f5b1393fef")
                       (current_revision . "deadbeef1234567890abcdef1234567890abcdef")
                       (revisions . ((cafebabe1234567890abcdef1234567890abcdef
                                      . ((_number . 1)
                                         (commit
                                          . ((parents
                                              . [((commit . "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"))])))))
                                     (f000cafe1234567890abcdef1234567890abcdef . ((_number . 2)))
                                     (c001babe1234567890abcdef1234567890abcdef . ((_number . 3)))
                                     (deadf0011234567890abcdef1234567890abcdef . ((_number . 4)))
                                     (badc0ffe1234567890abcdef1234567890abcdef . ((_number . 5)))
                                     (deadbeef1234567890abcdef1234567890abcdef . ((_number . 6)))
                                     )))))
  (it "returns any number given"
	(dotimes (n 10)
	  (expect (gee-patchset-number change-info n) :to-equal n))
	(expect (gee-patchset-number change-info 0) :to-equal 0))
  (it "returns 0 for base"
	(expect (gee-patchset-number change-info "base") :to-equal 0))
  (it "returns the last revision number for `current'"
	(expect (gee-patchset-number change-info "current") :to-equal 6))
  (it "returns the number of matched full SHA1"
	(expect (gee-patchset-number change-info "cafebabe1234567890abcdef1234567890abcdef") :to-equal 1)
	(expect (gee-patchset-number change-info "badc0ffe1234567890abcdef1234567890abcdef") :to-equal 5))
  (it "returns the number of partial SHA1"
	(expect (gee-patchset-number change-info "c001babe") :to-equal 3)
	(expect (gee-patchset-number change-info "deadbeef") :to-equal 6))
  (it "returns nil for non-matching SHA1"
	(expect (gee-patchset-number change-info "0000000") :to-throw))
  )

(describe "gee-canonical-revision-id"
  :var ((change-info '((id . "project~target~Icfbd91722e453e75cfda11b5250759f5b1393fef")
                       (change_id . "Icfbd91722e453e75cfda11b5250759f5b1393fef")
                       (current_revision . "deadbeef1234567890abcdef1234567890abcdef")
                       (revisions . ((cafebabe1234567890abcdef1234567890abcdef
                                      . ((_number . 1)
                                         (commit
                                          . ((parents
                                              . [((commit . "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"))])))))
                                     (f000cafe1234567890abcdef1234567890abcdef . ((_number . 2)))
                                     (c001babe1234567890abcdef1234567890abcdef . ((_number . 3)))
                                     (deadf0011234567890abcdef1234567890abcdef . ((_number . 4)))
                                     (badc0ffe1234567890abcdef1234567890abcdef . ((_number . 5)))
                                     (deadbeef1234567890abcdef1234567890abcdef . ((_number . 6)))
                                     )))))
  (it "current returns current_revision"
	(expect (gee-canonical-revision-id change-info "current")
			:to-equal "deadbeef1234567890abcdef1234567890abcdef"))
  (it "number finds correct revision"
	(expect (gee-canonical-revision-id change-info 4)
			:to-equal "deadf0011234567890abcdef1234567890abcdef"))
  (it "partial SHA1 finds correct revision"
	(expect (gee-canonical-revision-id change-info "badc0ffe")
			:to-equal "badc0ffe1234567890abcdef1234567890abcdef"))
  (it "full SHA1 finds correct revision"
	(expect (gee-canonical-revision-id change-info "badc0ffe1234567890abcdef1234567890abcdef")
			:to-equal "badc0ffe1234567890abcdef1234567890abcdef"))
  (it "error on list"
	(expect (gee-canonical-revision-id change-info '(1)) :to-throw))
  (it "error when number not found"
	(expect (gee-canonical-revision-id change-info 42) :to-throw))
  (it "error when SHA1 not found"
	(expect (gee-canonical-revision-id change-info "1234567") :to-throw))
  (it "error when multiple SHA1 matches"
	(expect (gee-canonical-revision-id change-info "dead") :to-throw))
  (it "error when string is not a SHA1"
	(expect (gee-canonical-revision-id change-info "not a SHA1") :to-throw))
  (it "error when string is too short"
	(expect (gee-canonical-revision-id change-info "123") :to-throw))
  (it "error when string is too long"
	(expect (gee-canonical-revision-id change-info (make-string 41 ?1)) :to-throw))
  (it "returns the parent commit SHA1 when called with number 0"
	(expect (gee-canonical-revision-id change-info 0) :to-equal "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
	;; TODO: What wil happen if revision 1 is a merge
	(expect (gee-canonical-revision-id change-info "base") :to-equal "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"))
  )

(describe "gee-find-revision-number"
  :var ((revisions '((cafebabe1234567890abcdef1234567890abcdef
					  . ((_number . 1)
						 (commit
						  . ((parents
							  . [((commit . "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"))])))))
					 (f000cafe1234567890abcdef1234567890abcdef . ((_number . 2)))
					 (c001babe1234567890abcdef1234567890abcdef . ((_number . 3)))
					 (deadf0011234567890abcdef1234567890abcdef . ((_number . 4)))
					 (badc0ffe1234567890abcdef1234567890abcdef . ((_number . 5)))
					 (deadbeef1234567890abcdef1234567890abcdef . ((_number . 6)))
					 )))
  (it "Returns nil outside range"
	(expect (gee-find-revision-number revisions 0) :to-equal nil)
	(expect (gee-find-revision-number revisions 7) :to-equal nil)
	(expect (gee-find-revision-number revisions 4) :not :to-equal nil))
  (it "Returns the entire RevisionInfo object"
	(expect (gee-find-revision-number revisions 1) :to-equal (nth 0 revisions))
	(expect (gee-find-revision-number revisions 3) :to-equal (nth 2 revisions))))

(provide 'test-gee-review)
;;; test-gee-review.el ends here
