;;; gee-util.el --- Common utilities for gee   -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Ola Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Maintainer: Ola Nilsson <ola.nilsson@gmail.com>
;; URL: https://bitbucket.org/olanilsson/gee
;; Keywords: gerrit

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file contains functions that are used across the various gee views.
;; * basic customization
;; * functions to manage buttons
;; * helper functions to manage text properties
;; * json helper functions
;; * Time conversion functions

;;; Code:

(require 'cus-edit) ; we use faces defined here
(require 'let-alist)

(defgroup gee nil "Customization points for gee - The Gerrit Emacs Experience."
  :prefix "gee"
  :group 'vc
  :link '(url-link :tag "Gerrit documentation" "https://gerrit-review.googlesource.com/Documentation/")
  :link '(url-link :tag "gee - The Gerrit Emacs Experience" "https://bitbucket.org/olanilsson/gee")
  )

(defcustom gee-tmp-dir "/tmp/gee/"
  "Base directory for gee temporary files."
  :group 'gee
  :type 'directory)

;; buffers

(defmacro gee-buffer-create (fmt &rest args)
  "Create a buffer with name (format FMT ARGS)."
  `(get-buffer-create (format ,fmt ,@args)))

;; buttons

(define-button-type 'gee-button) ; common button supertype for all gee buttons

(define-button-type 'gee-bordered-button
  :supertype 'gee-button
  'face 'gee-review-bordered-button-face
  'mouse-face 'gee-review-bordered-button-mouse-face)

(defun gee-make-button (label type &rest properties)
  "Return string button with text LABEL and type TYPE.
PROPERTIES should be a property list (but the keys may also be
plain symbols) that will be applied to the button."
  (apply #'make-text-button label 0 :type type properties))

;; text properties

(defmacro let-text-props (pos &rest body)
  "Let-bind dotted symbols to their text properties at POS and execute BODY.
Dotted symbol is any symbol starting with a `.'.  Only those present
in BODY are let-bound and this search is done at compile time.

For instance, the following code

  (let-text-props pos
    (if (and .title .body)
        .body
      .site))

essentially expands to

  (let ((.title (get-text-property pos 'title)))
        (.body  (get-text-property pos 'body)))
        (.site  (get-text-property pos 'site))))
    (if (and .title .body)
        .body
      .site))

If you nest `let-text-props' invocations, the inner one can't access
the variables of the outer one."
  (declare (indent 1) (debug t))
  (let ((var (make-symbol "this-button")))
    `(let ((,var ,pos))
       (let ,(mapcar (lambda (x) `(,(car x) ,(let-text-props--access-sexp (car x) var)))
               (delete-dups (let-alist--deep-dot-search body)))
         ,@body))))

(defun let-text-props--access-sexp (symbol variable)
  "Return a sexp used to access SYMBOL inside VARIABLE."
  (let* ((clean (let-alist--remove-dot symbol))
         (name (symbol-name clean)))
    (if (string-match "\\`\\." name)
        clean
      (list 'get-text-property variable (list 'quote clean)))))

(defun gee-aligned-space (hpos &optional char)
  "Create a wide space string with :align-to HPOS.
The string is \" \" unless CHAR specifies another character."
  (propertize (make-string 1 (or char ?\s)) 'display `(space . (:align-to ,hpos))))

;; json functions

(defun gee-json-property (property json-object)
  "Return the value of PROPERTY of JSON-OBJECT.
PROPERTY may be a string or a symbol, and JSON-OBJECT may use
strings or symbols as attribute keys.  Any combination works.
PROPERTY may be a list, where (gee-json-property '(foo bar) obj)
is equivalent to (gee-json-property 'bar (gee-json-property 'foo
obj))."
  (if (listp property)
	  (let ((next-obj (gee-json-property (car property) json-object))
			(next-prop (cdr property)))
		(if next-prop
			(gee-json-property (cdr property) next-obj)
		  next-obj))
	(let ((val (cdr (or (assoc property json-object)
                        (and (symbolp property) (assoc (symbol-name property) json-object))
                        (and (stringp property) (assoc (intern property) json-object))))))
      (unless (eq :json-false val) val))))

(defun gee-plist-p (list)
  "Non-null if and only if LIST is a plist.
Based on `json-plist-p' but allows all symbols in the property
name slots while `json-plist-p' only allows keywords."
  (while (consp list)
    (setq list (if (and (keywordp (car list))
                        (consp (cdr list)))
                   (cddr list)
                 'not-plist)))
  (null list))

;; Time functions

(defun gee-git-date-to-time (git-date &optional git-tz)
  "Parse GIT-DATE and return a time value.
GIT-DATE should be formatted `YYYY-MM-DD hh:mm:ss.ns' where `ns'
are nine digits.  An optional timezone argument as minutes offset
from GMT can be provided in GIT-TZ."
  (let ((time (date-to-time git-date)))
	(when (and (< (length time) 3)
			   (string-match "\\.\\([0-9]\\{1,6\\}\\)\\([0-9]\\{3\\}\\)"
							 git-date))
	  (setq time (append time (list (string-to-number (match-string 1 git-date))
									(string-to-number (match-string 2 git-date)))))
	  (when git-tz
		(setq time (time-add time (seconds-to-time (* git-tz 60)))))
	  time)))

(defun gee-format-time-relative (time)
  "Format TIME relative to now as `git log --relative-date' does.
TIME is specified as (HIGH LOW USEC PSEC), as returned by
`current-time' or `file-attributes'.  The format is `X <unit>
ago' where unit is minutes, seconds, hours, days, weeks, months,
or years."
  (let* ((now (current-time))
		 (elapsed (time-subtract now time))
		 (seconds (time-to-seconds elapsed)))
	(cond ((time-less-p now time) "in the future")
		  ((time-less-p elapsed '(0 90))   (format-seconds "%s seconds ago" seconds))
		  ((time-less-p elapsed '(0 5400)) (format-seconds "%m minutes ago" seconds))
		  ((time-less-p elapsed '(1 64064)) (format-seconds "%h hours ago" seconds))
		  ((time-less-p elapsed '(18 29952)) (format-seconds "%d days ago" seconds))
		  ((time-less-p elapsed '(92 18688))
		   (format "%d weeks ago" (/ (string-to-number (format-seconds "%d" seconds)) 7)))
		  ((time-less-p elapsed '(481 13184))
		   (format "%d months ago" (/ (string-to-number (format-seconds "%d" seconds)) 30)))
		  ((time-less-p elapsed '(2406 384))
		   (let* ((days (string-to-number (format-seconds "%d" seconds)))
				  (years (/ days 365))
				  (months (/ (% days 365) 30)))
			 (format "%d years, %d months ago" years months)))
		  (t (format-seconds "%y years ago" seconds)))))

(defun gee-relative-git-time (git-time)
  "Format GIT-TIME as a time relative to now."
  (gee-format-time-relative (gee-git-date-to-time git-time)))

(defun gee-timestamp-to-display-time (timestamp)
  "Format a Gerrit TIMESTAMP to a displayable string.
The Gerrit timestamp is a string with the format `yyyy-mm-dd
hh:mm:ss.fffffffff' where `fffffffff' represents nanoseconds.  It
is always given in UTC.  The displayable string is increasingly
inaccurate the further into the past.  The returned string is
formatted `yyyy-mm-dd' for timestamps older than a year, `mm-dd
hh:mm' for timestamps older than 24 hours and `hh:mm' for
timestamps newer than 24 hours."
  (let* ((now (current-time))
         (stamp (gee-git-date-to-time timestamp))
         (elapsed (time-subtract now stamp)))
    (cond ((time-less-p now stamp) "in the future")
		  ((time-less-p elapsed '(1 20864)) (format-time-string "%H:%M" stamp))
		  ((time-less-p elapsed '(481 13184)) (format-time-string "%m-%d %H:%M" stamp))
		  (t (format-time-string "%Y-%m-%d" stamp)))))

(provide 'gee-util)
;;; gee-util.el ends here
